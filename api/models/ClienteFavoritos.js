"use strict";

const { Modelo } = require("./Modelo");


class ClienteFavoritos extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "clientes_favoritos";
    }
}
module.exports = ClienteFavoritos;
