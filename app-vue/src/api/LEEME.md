# Funciones y rutinas para la API

Como regla general, las funciones que realizan llamadas al API regresarán una promesa.
Las funciónes cuyos nombres comienzan con `recuperar` realizan llamadas al API.
Las funciones cuyos nombres comienzan con `obtener` son locales, y devuelven datos.


## Módulo: auth
`api/auth.js`
Contiene funciones para la autenticación.

**autenticar(_correo_, _contraseña_)** -> Promise { _datos_ }
Acepta dos cadenas de texto. En caso de éxito devuelve una promesa con los datos recibidos del API, en caso de error la promesa contiene los datos sobre el error.

**salir([_callback_])** -> Promise { true }
Elimina todos los datos locales del usuario. Adicionalmente, acepta una función callback.

**obtenerDatos()** -> object || null
Devuelve los datos del usuario actual _sin los datos de autenticación_.

**obtenerToken()** -> string || null
Devuelve el token de la sesión si existe.

**obtenerTipoUsuario()** -> string || null
Devuelve el tipo de usuario logeado.


## Modulo: general
`api/modulos/general.js`

**recuperarData(_ruta_)** -> Promise { _datos_ }
Realiza una petición GET simple al API.

**recuperarDataPorId(_ruta_, _id_)** -> Promise { _datos_ }
Igual que `recuperarData`, pero anexa la id a la peticion.

**post(_ruta_, _datos_)** -> Promise { _datos_ }
Realiza una petición POST a la ruta dada, devuelve una promesa con los datos recibidos del API.

**put(_ruta_, _datos_)** -> Promise { _datos_ }
Realiza una petición POST a la ruta dada, devuelve una promesa con los datos recibidos del API.

