//llamar al modelo
const Tipo_Vehiculo = require("../models/Tipo_Vehiculo");

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const moment = require("moment");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../config");
var uuid = require("uuid");
var sesion = require("./sesion");
var sesi = new sesion();
var archivos = require("../Archivos");
var fs = require("fs-extra");

//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

//Manejador de Errores
const { AuthError, ValidationError } = require("../Errores");

var router = express.Router();

//registrar 
router.post("/add", upload.none(), sesi.validar_admin, async (req, res, next) => {
    data = req.body;

    let errr = false;

    // inicio validaciones
    if (!data.nombre_veh) errr = "Ingresa el tipo de vehículo";
    if (!data.icono_veh) errr = "Selecciona el icono del vehículo";
    //fin validaciones
    if(errr) return AR.enviarDatos({r: false, msj: errr}, res);

    await Tipo_Vehiculo.query()
    .insert({
        nombre_veh: data.nombre_veh,
        icono_veh: data.icono_veh
    })
    .then(async resp => {
      resp.msj = "Tipo de vehículo registrado";
      resp.r = true;
      AR.enviarDatos(resp, res);
    })
    .catch(err => {
      console.log(err)
      err.r = false;
      err.msj = 'Error al registrar Tipo de vehículo';
      res.send(err);
    });

});

//editar 
router.put("/edit", upload.none(), sesi.validar_admin, async (req, res, next) => {
  data = req.body;
  let errr = false;

  // inicio validaciones
  if (!data.nombre_veh) errr = "Ingresa el tipo de vehículo";
  if (!data.icono_veh) errr = "Selecciona el icono del vehículo";
  if (!data.iden) errr = "Ingresa la id";
  //fin validaciones
  if(errr) return AR.enviarDatos({r: false, msj: errr}, res);

  var datos = {
    nombre_veh: data.nombre_veh,
    icono_veh: data.icono_veh
  }

  await Tipo_Vehiculo.query().updateAndFetchById(data.iden, datos)
  .then(async resp => {
    resp.msj = "Datos actualizados con éxito";
    resp.r = true;
    AR.enviarDatos(resp, res);
  })
  .catch(err => {
    console.log(err)
    err.r = false;
    err.msj = 'Error al actualizar datos';
    res.send(err);
  });

});
//listar 
router.get("/", sesi.validar_general, async(req, res) => {
  await Tipo_Vehiculo.query()
      .then(ret => {
      AR.enviarDatos(ret, res);
  });
});

//ver 
router.get("/:id", sesi.validar_general, async(req, res) => {
  await Tipo_Vehiculo.query().where("id",req.params.id).then(ret => {
      AR.enviarDatos(ret[0], res);
  });

});

//eliminar 
router.delete("/delete/:id", upload.none(), sesi.validar_admin, async(req, res) => {
  data = req.query;
  const eliminado = await Tipo_Vehiculo.query().delete().where({ id: req.params.id })
    .catch(err => {
      console.log(err);
      res.send(err);
    });

    if (eliminado){
      AR.enviarDatos({ msj: "Registro eliminado exitosamente" }, res);
    }
    else{ AR.enviarDatos({ msj: "No se puede eliminar el tipo de vehículo" }, res);}
});


module.exports = router;
