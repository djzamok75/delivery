
exports.up = function(knex, Promise) {
    return knex.schema.createTable("cuentas", table => {
        table.increments("id").primary();
        table.string("nombre");
        table.string("numero");
        table.string("tipo");
        table.text("detalles");
        table.integer("id_restaurante").unsigned().references('id').inTable('restaurantes');
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTableIfExists("cuentas");
};
