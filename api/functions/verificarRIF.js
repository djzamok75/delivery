/*
Formato de RIF: X-00000000-N || X00000000N
La expresion regular:

^             : Principio de texto.
[JGVEPjgvep]  : La letra de la nacionalidad (X), mayuscula o minuscula.
[-]?          : Puede o no haber un guion.
[0-9]{7-9}    : Numeros, de 7 a 9 digitos.
[0-9]{1}      : El digito de chequeo (N).
$             : Fin de texto.
*/

module.exports = function verificarRIF(rif){
	return /^[JGVEPjgvep][-]?[0-9]{7,9}[-]?[0-9]{1}$/.test(rif);
}
