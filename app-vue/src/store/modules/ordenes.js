import Vue from 'vue'
import estatus_ordenes from '../estatus-ordenes'

export default
{
  namespaced: true,

  state: () => ({
    ordenes: {},
  }),


  getters: {
    all: state => Object.values(state.ordenes),
    porDoomialiado: (state) => (id) => state.ordenes[id],
  },


  mutations:
  {
    SET_ORDEN: (state, payload) => Vue.set(state.ordenes, payload[0], payload[1]),
  },


  actions:
  {
    checkout ({commit, dispatch, getters, rootGetters}, datos) {
      return new Promise(function(resolve, reject){
        const orden = {
          // idCuenta: datos.cuenta || datos.idCuenta,
          // idMetodo: datos.metodo || datos.idMetodo,
          // referencia: datos.referencia,
          ordenes: rootGetters['carrito/all'],
          idDoomialiado: rootGetters['carrito/idDoomialiadoActual'],
          estatus: estatus_ordenes.ENVIADO,
        };

        const ordenDoomi = getters['porDoomialiado'](orden.idDoomialiado);
        if(false)
          return reject('Ya tienes una orden en este doomialiado');

        commit('SET_ORDEN', [orden.idDoomialiado, orden]);
        dispatch('carrito/vaciar', null, {root:true});

        resolve('Orden enviada');

      });
    },
  },



};
