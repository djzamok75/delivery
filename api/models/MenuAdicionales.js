"use strict";

const { Modelo } = require("./Modelo");


class MenuAdicionales extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "menus_adicionales";
    }
}
module.exports = MenuAdicionales;
