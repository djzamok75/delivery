"use strict";
const Paises = require("../models/Pais");
const Estado = require("../models/Estado");
const Ciudad = require("../models/Ciudad");
const Usuario = require("../models/Usuario");
const Horarios = require("../models/Horarios");
const Categoria = require("../models/Categoria");
const Subcategoria = require("../models/Subcategoria")
const CategoriaMenu = require("../models/CategoriaMenu");
const RestauranteSubcategorias = require("../models/RestauranteSubcategorias");

const { Modelo } = require("./Modelo");


class Restaurante extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "restaurantes";
    }

    $afterGet(queryContext) {
        return this.ag();
    }

    async pais() {
        if(this.id_pais){
            var res = await Paises.query()
                .where("id", this.id_pais);
            if (res[0]) var valor = res;
            else var valor = [];
            this.pais = valor;
        }
    }
    async estado() {
        if(this.id_estado){
            var res = await Estado.query()
                .where("id", this.id_estado);
            if (res[0]) var valor = res;
            else var valor = [];
            this.estado = valor;
        }
    }
    async ciudad() {
        if(this.id_ciudad){
            var res = await Ciudad.query()
                .where("id", this.id_ciudad);
            if (res[0]) var valor = res;
            else var valor = [];
            this.ciudad = valor;
        }
    }
    async usuarios() {
        if(this.id){
            var res = await Usuario.query()
                .where("id_restaurante", this.id)
                .select('cedula','nombre_apellido','id','correo');
            if (res[0]) var valor = res;
            else var valor = [];
            this.usuarios = valor;
        }
    }

    async categoria() {
        if (this.id_categoria) {
            var cat = await Categoria.query().where("id", this.id_categoria);
            this.categoria = cat[0] ? cat[0] : [];
        }
    }

    async subcategorias() {
        let relaciones = await RestauranteSubcategorias.query().where("id_restaurante", this.id);

        let ids_subcategorias = [];
        for (let rel of relaciones){ ids_subcategorias.push(rel.id_subcategoria) }

        let subs = await Subcategoria.query().whereIn("id", ids_subcategorias);

        this.subcategorias = subs;
        this.id_subcategorias = subs.map(s => s.id);
    }

    async categoriasMenu() {
        let cm = await CategoriaMenu.query().where("id_restaurante", this.id);
        this.categorias_menu = cm;
    }

    async horarios(){
        let h = await Horarios.query().where("id_restaurante", this.id);
        this.horarios = h;
    }

    async ag() {
        await this.pais();
        await this.estado();
        await this.ciudad();
        await this.usuarios();
        await this.categoria();
        await this.subcategorias();
        await this.categoriasMenu();
        await this.horarios();
    }
}
module.exports = Restaurante;
