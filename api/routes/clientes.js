// Obtener modelo.
const Cliente = require("../models/Cliente");
const Restaurante = require("../models/Restaurante");
const ClienteFavoritos = require("../models/ClienteFavoritos")
const TokenActivacionCliente = require("../models/TokenActivacionCliente");

// Otras librerias.
const bcrypt = require("bcryptjs");
const crypto = require("crypto");
const fs = require("fs");
const AR = require("../ApiResponser");
const validateMail = require("../functions/validateMail");
const puntoEnRadio = require("../functions/coordUtils/puntoEnRadio");
const Mailer = require("../mails/Mailer");
const mailp = require("../mails/correo");
const config = require("../config");
const archivos = require("../Archivos");


const router = require("express").Router();
const upload = require("multer")();
const sesi = new (require("./sesion"))();

const configuracion = JSON.parse(fs.readFileSync(__dirname+"/../configuracion.json"));



// Ver un cliente
router.get("/:id", upload.none(), sesi.validar_general, async(req, res)=>{
    await Cliente.query().where("id", req.params.id)
        .then(clientes=>{
            AR.enviarDatos(clientes[0], res);
        });
});

// Registrar un cliente
router.post("/add", archivos.fields([{ name: "foto", maxCount: 1 }]), async(req, res)=>{
    data = req.body;

    // Validar datos.
    // Si el correo es invalido generar el error de correo.
    if (data.correo && !validateMail(data.correo))
        data.correo = "";

    //-- Validar los datos.
    errr = false;
    if (!data.identificacion) errr = "Selecciona un tipo de identificación";
    if (!data.cedula) errr = "Ingrese un número de identificación";
    if (!data.nombre) errr = "Ingrese un nombre";
    if (!data.apellido) errr = "Ingrese un apellido";
    if (!data.telefono) errr = "Ingrese un número telefónico";
    if (!data.correo) errr = "Ingrese un correo válido";
    if (!data.pass) errr = "Ingreses una contraseña";
    if (!data.direccion) errr = "Ingrese la dirección";
    // if (!data.usuario) errr = "Ingresa un usuario";
    // if (!data.pais) errr = "Selecciona el pais";
    // if (!data.estado) errr = "Selecciona el estado";
    // if (!data.ciudad) errr = "Selecciona la ciudad";
    // if ((!data.lat || !data.lon) && (!data.latitud || !data.longitud))
    //     errr = "Selecciona la ubicación en el mapa";
    // if (typeof data.sexo != "string" ||
    //     !["M", "F", "O"].includes(data.sexo.toUpperCase()))
    //     errr = "ERROR INTERNO: no se especificó el sexo o tiene un valor incorrecto";

    if (data.whatsapp) {
        const verificar_w = await Cliente.query().where({ whatsapp: data.whatsapp });
        if (verificar_w[0]) errr = "El Whatsapp ya existe";
    }


    if (data.correo && data.cedula){
        const verificar_cor = await Cliente.query().where({ correo: data.correo });
        const verificar_ced = await Cliente.query().where({ cedula: data.cedula });

        if (verificar_ced[0]) errr = "El número de identificación ya existe";
        if (verificar_cor[0]) errr = "El correo ya existe";
    }
    if(errr) {
        //ELIMINAR IMAGENES SI NO PASA LAS VALIDACIONES
        if(req.files["foto"])
            await fs.unlink(__dirname + "/../public/uploads/" + req.files["foto"][0].filename, function (err) { });
        return AR.enviarDatos({r: false, msj: errr}, res);
    }
    // Fin de validacion.

    datos = {identificacion: data.identificacion
            ,nombre: data.nombre
            ,apellido: data.apellido
            ,sexo: data.sexo
            ,cedula: data.cedula
            ,correo: data.correo
            ,usuario: data.usuario
            ,direccion: data.direccion
            ,telefono: data.telefono
            ,whatsapp: data.whatsapp
            ,facebook: data.facebook
            ,instagram: data.instagram
            ,punto_referencia: data.punto_referencia
            ,id_pais: data.pais
            ,id_estado: data.estado
            ,id_ciudad: data.ciudad
            ,lat: data.lat || data.latitud
            ,lon: data.lon || data.longitud
            ,pass: bcrypt.hashSync(data.pass, 8)
        }

    //guardar ruta imagen
    if(req.files["foto"])
        datos.foto = config.rutaArchivo(req.files["foto"][0].filename);


    Cliente.query()
        .insert(datos)
        .then(async (resp)=>{
            // Crear y almacenar el token para la activacion.

            const token = await crypto.randomBytes(64).toString('hex');
            await TokenActivacionCliente.query().insert({token:token, id_cliente:resp.id})

            const dominioApi = "".concat(req.protocol, "://", req.get('host'), "/");
            const dominio = "".concat(req.protocol, "://", req.hostname, "/");

            const rutaActivacion = dominioApi.concat("clientes/activar/", token);
            //const urlActivacion = dominio.concat("delivery/panel/confirmacioncliente.html?ea=", encodeURIComponent(rutaActivacion));

            const urlActivacion = config.dominioWeb+"panel/confirmacioncliente.html?ea="+config.dominio+"/clientes/activar/"+token;
            
            const titulo = "Doomi | Correo de verificación";
            const cuerpo = `
            Hola, ${data.nombre}<br />
            Haz click en el botón para verificar tu correo.<br />
            <div style="text-align:center" >
                <a href="${urlActivacion}" style="background-color:#182C61; border:none; color:white; padding:15px 32px; text-align:center; text-decoration:none; display:inline-block; font-size:16px; margin:50px auto; width:70px; cursor:pointer;">
                    Verificar
                </a>
                </div>
                `  ;


            Mailer.enviarMail({
                direccion: resp.correo,
                titulo: titulo,
                mensaje: mailp({cuerpo:cuerpo, titulo:titulo})
            }, false/*Sin manejar el error*/)
            .then(info=>{
                console.log(info);
                resp.r = true;
                resp.msj = "Se ha enviado un correo de activación";
                AR.enviarDatos(resp, res);
            })
            .catch(async(err)=>{
                console.log(err);
                await Cliente.query().delete().where({id:resp.id});
                await TokenActivacionCliente.query().delete().where({id_cliente: resp.id});
                resp.r = false;
                resp.msj = "No se pudo enviar el correo de activacion, intente de nuevo";
                AR.enviarDatos(resp, res);
            });
        })
        .catch(async(err)=>{
            //ELIMINAR IMAGENES POR SI EXISTE ALGUN ERROR
            if(req.files["foto"])
                await fs.unlink(__dirname + "/../public/uploads/" + req.files["foto"][0].filename, function (err) { });

            await Cliente.query().where("correo", data.correo)
                .then(async(cliente)=>{
                    if (cliente[0])
                        await TokenActivacionCliente.query().delete().where("id_cliente",cliente[0].id);
            });
            console.log(err);
            err.f = false;
            err.msj = "Error al registrar";
            res.send(err);
        });
});

// Editar un cliente
router.put("/edit", archivos.fields([{ name: "foto", maxCount: 1 }]), sesi.validar_cliente, async(req, res)=>{
    data = req.body;

    // Si el correo es invalido generar el error de correo.
    if (data.correo && !validateMail(data.correo))
        data.correo = "";

    //-- Validar los datos.
    errr = false;
    if (!data.identificacion) errr = "Selecciona un tipo de identificación";
    if (!data.cedula) errr = "Ingrese un número de identificación";
    if (!data.nombre) errr = "Ingrese un nombre";
    if (!data.apellido) errr = "Ingrese un apellido";
    if (!data.direccion) errr = "Ingrese la dirección";
    if (!data.telefono) errr = "Ingrese un número telefónico"
    if (!data.correo) errr = "Ingrese un correo válido";
    if (!data.usuario) errr = "Ingresa un usuario";
    if (!data.pais) errr = "Selecciona el pais";
    if (!data.estado) errr = "Selecciona el estado";
    if (!data.ciudad) errr = "Selecciona la ciudad";
    if ((!data.lat || !data.lon) && (!data.latitud || !data.longitud)) errr = "Selecciona la ubicación en el mapa";

    if(!data.iden) errr = "ERROR INTERNO: no se especificó el id del cliente"; 
    if (typeof data.sexo != "string" ||
        !["M", "F", "O"].includes(data.sexo.toUpperCase()))
        errr = "Seleccione el sexo";

    if (data.whatsapp && data.iden) {
        const verificar_w = await Cliente.query().where({ whatsapp: data.whatsapp }).whereNot({id: data.iden});
        if (verificar_w[0]) errr = "El Whatsapp ya existe";
    }

    if (data.correo && data.cedula && data.iden){
        const verificar_cor = await Cliente.query().where({ correo: data.correo }).whereNot({id: data.iden});
        const verificar_ced = await Cliente.query().where({ cedula: data.cedula }).whereNot({id: data.iden});

        if (verificar_ced[0]) errr = "El número de identificación ya existe";
        if (verificar_cor[0]) errr = "El correo ya existe";
    }
    //-- Fin de validacion.

    if(errr) {
        //ELIMINAR IMAGENES SI NO PASA LAS VALIDACIONES
        if(req.files["foto"])
            await fs.unlink(__dirname + "/../public/uploads/" + req.files["foto"][0].filename, function (err) { });
        return AR.enviarDatos({r: false, msj: errr}, res);
    }
    // Fin de validacion.

    datos = {identificacion: data.identificacion
            ,nombre: data.nombre
            ,apellido: data.apellido
            ,sexo: data.sexo
            ,cedula: data.cedula
            ,correo: data.correo
            ,usuario: data.usuario
            ,direccion: data.direccion
            ,telefono: data.telefono
            ,whatsapp: data.whatsapp
            ,facebook: data.facebook
            ,instagram: data.instagram
            ,punto_referencia: data.punto_referencia
            ,id_pais: data.pais
            ,id_estado: data.estado
            ,id_ciudad: data.ciudad
            ,lat: data.lat || data.latitud
            ,lon: data.lon || data.longitud
        }

    if (data.pass && data.pass.length > 0) {
        datos.pass = bcrypt.hashSync(data.pass, 8);
    }

    //guardar ruta imagen
    if(req.files["foto"])
        datos.foto = config.rutaArchivo(req.files["foto"][0].filename);

    Cliente.query().updateAndFetchById(data.iden, datos)
        .then(async (resp)=>{
            resp.r = true;
            resp.msj = "Datos actualizados exitosamente";
            AR.enviarDatos(resp, res);
        })
        .catch(async (err)=>{
            //ELIMINAR IMAGENES POR SI EXISTE ALGUN ERROR
            if(req.files["foto"])
                await fs.unlink(__dirname + "/../public/uploads/" + req.files["foto"][0].filename, function (err) { });
            console.log(err);
            err.f = false;
            err.msj = "Error al actualizar los datos";
            res.send(err);
        });
});

// Eliminar un cliente
router.delete("/delete/:id", upload.none(), sesi.validar_cliente, async(req, res)=>{

    var info = await Cliente.query().where({ id: req.params.id })
    const eliminado = await Cliente.query().delete().where({ id: req.params.id })
        .catch(err => {
            console.log(err);
            res.send(err);
        });

    if (eliminado){
        if(info[0]){
            if(info[0].img_rif)
                await fs.unlink(__dirname + "/../public/uploads/" + info[0].img_rif.split(`\\public\\uploads\\`)[1], function (err) { });
        }
        AR.enviarDatos({ msj: "Registro eliminado exitosamente" }, res);
    }
    else{
        AR.enviarDatos({ msj: "No se puede eliminar el Repartidor" }, res);
    }
});

// Ver clientes
router.get("/", upload.none(), sesi.validar_general, async(req, res)=>{
    Cliente.query().orderBy('fecha_registro', 'desc')
        .then(clientes=>{
            AR.enviarDatos(clientes, res);
        });
});

// Activar cliente con el enlace enviado al correo.
router.get("/activar/:token", async(req, res)=>{
    let TAC = await TokenActivacionCliente.query().where("token", req.params.token);

    if (TAC[0]){
        await Cliente.query().updateAndFetchById(TAC[0].id_cliente, {activado: true});
        await TokenActivacionCliente.query().delete().where({id: TAC[0].id})
        return AR.enviarDatos({r: true, msj: "Activación exitosa"}, res);
    }
    else
        return AR.enviarDatos({r: false, msj: "Usuario no encontrado"}, res);
});

// Listar restaurantes
router.post("/listar_res", upload.none(), sesi.validar_cliente, async(req, res)=>{
    data = req.body;

    await Restaurante.query()
        .then(restaurantes=>{

            let ubicacionCliente = {
                lat: data.lat || data.latitud,
                lon: data.lon || data.longitud,
            };

            let restCercanos = restaurantes.filter(rest=>{
                return puntoEnRadio(
                    {lat:rest.lat, lon:rest.lon},
                    ubicacionCliente,
                    configuracion.km,
                );
            });

            return AR.enviarDatos(restCercanos, res);
        });
});

// Listar restaurantes favoritos.
router.get("/:id/doomis_favoritos", upload.none(), sesi.validar_cliente, async(req, res)=>{
    await ClienteFavoritos.query().where("id_cliente", req.params.id)
        .then(resp => AR.enviarDatos(resp, res))
        .catch(err => {
            console.log(err);
            res.sendStatus(500);
        });
});


// Guardar favorito.
router.put("/:cliente/doomis_favoritos/:id_res", upload.none(), sesi.validar_cliente, async(req, res)=>{
    let data = req.params;

    // Verificar.
    verificar_doomi = await Restaurante.query()
        .where("id", data.id_res).catch(err => console.log(err));

    verificar_fav = await ClienteFavoritos.query()
        .where("id_restaurante", data.id_res).catch(err => console.log(err));

    if (!verificar_doomi[0])
        return AR.enviarError('Doomialiado no encontrado', res, 404);
    if (verificar_fav[0])
        return AR.enviarDatos({r:true}, res);

    await ClienteFavoritos.query()
        .insert({id_cliente: data.cliente, id_restaurante: data.id_res})
        .then(r => AR.enviarDatos({r:true}, res))
        .catch(err => {res.sendStatus(500); console.log(err)});
});

// Eliminar favorito.
router.delete("/:cliente/doomis_favoritos/:id_res", upload.none(), sesi.validar_cliente, async(req, res)=>{
    const x = await ClienteFavoritos.query().delete()
        .where("id_restaurante", req.params.id_res)
        .then(r => AR.enviarDatos({r:true}, res))
        .catch(err => {res.sendStatus(500); console.log(err)});
});


module.exports = router;
