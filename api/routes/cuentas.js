//llamar al modelo
const Cuentas = require("../models/Cuentas");
const CuentaswhitDetails = require("../models/CuentaswhitDetails");
const Metodos_en_cuentas = require("../models/Metodos_en_cuentas");

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../config");
var uuid = require("uuid");
var sesion = require("./sesion");
var sesi = new sesion();


//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

//Manejador de Errores
const { AuthError, ValidationError } = require("../Errores");

var router = express.Router();

//registrar un aula
router.post("/add", upload.none(), sesi.validar_general, async (req, res, next) => {
    data = req.body;

    let errr = false;
    
    // inicio validaciones
    if (!data.nombre) errr = "Ingresa un nombre";
    if (!data.metodos) errr = "debe elegir al menos un método de pago";
    if (!data.id_res && data.tipo_usu!='administrador') errr = "Ingresa id del restaurante";

    //fin validaciones
    if(errr) return AR.enviarDatos({r: false, msj: errr}, res);
    
    var datos = {
      nombre: data.nombre,
      numero: data.numero,
      tipo: data.tipo,
      detalles: data.ext
    };
    if(data.tipo_usu!='administrador')
      datos.id_restaurante = data.id_res;

    await Cuentas.query()
    .insert(datos)
    .then(async resp => {
      
      for (var i = 0; i < data.metodos.length; i++) {
        await Metodos_en_cuentas.query().insert({ id_cuenta: resp.id, id_metodo: data.metodos[i] }).catch(err => { });
      };
      resp.msj = "Cuenta registrada";
      resp.r = true;
      AR.enviarDatos(resp, res);

    })
    .catch(err => {
      console.log(err)
      err.r = false;
      err.msj = 'Error al registrar Cuenta';
      res.send(err);
    });

});
router.put("/edit", upload.none(), sesi.validar_general, async (req, res, next) => {
    data = req.body;

    let errr = false;

    // inicio validaciones
    if (!data.nombre) errr = "Ingresa un nombre";
    if (!data.metodos) errr = "debe elegir al menos un método de pago";
    if (!data.iden) errr = "Ingresa la id";
    //fin validaciones
    if(errr) return AR.enviarDatos({r: false, msj: errr}, res);
    var datos = {
      nombre: data.nombre,
      numero: data.numero,
      tipo: data.tipo,
      detalles: data.ext
    };

   const aula = await Cuentas.query()
    .updateAndFetchById(data.iden, datos)
    .then(async resp => {
      
      await Metodos_en_cuentas.query().delete().where({ id_cuenta: data.iden }).catch(err => { console.log(err); });
      
      for (var i = 0; i < data.metodos.length; i++) {
        await Metodos_en_cuentas.query().insert({ id_cuenta: data.iden, id_metodo: data.metodos[i] }).catch(err => { });
      };
      
      resp.msj = "Cuenta actualizada";
      resp.r = true;
      AR.enviarDatos(resp, res);

    })
    .catch(err => {
      console.log(err)
      err.r = false;
      err.msj = 'Error al editar Cuenta';
      res.send(err);
    });

});

router.delete("/delete/:id", upload.none(), sesi.validar_general, async(req, res) => {
      data = req.query;
      await Metodos_en_cuentas.query().delete().where({ id_cuenta: req.params.id }).catch(err => { console.log(err); });
        const eliminado = await Cuentas.query()
            .delete()
            .where({ id: req.params.id })
            .catch(err => {
                console.log(err);
                res.send(err);
            });
        if (eliminado){AR.enviarDatos({ msj: "Registro eliminado exitosamente" }, res);}
        else{ AR.enviarDatos("0", res);}
});

router.get("/", async(req, res) => {
    data = req.query;
    if(data.tipo_usu=='administrador'){
      await Cuentas.query().where("id_restaurante",null)
          .then(cuentas => {
          AR.enviarDatos(cuentas, res);
      });
    }else{
      await Cuentas.query().where("id_restaurante",data.id_res)
          .then(cuentas => {
          AR.enviarDatos(cuentas, res);
      });
    }
});
router.get("/:id", async(req, res) => {
    await CuentaswhitDetails.query()
        .findById(req.params.id)
        .then(cuentas => {
        AR.enviarDatos(cuentas, res);
    });
});

module.exports = router;