exports.up = knex => {
    return knex.schema.createTable("direcciones", table => {
        table.increments("id").primary();
        //table.integer("id_cliente").unsigned().references("id").inTable("clientes").onDelete("cascade");
        table.integer("id_cliente");
        table.string("nom_dir", 50);
        table.string("dir_dir");
        table.string("lat_dir", 100).defaultTo(null);
        table.string("lon_dir", 100).defaultTo(null);
        table.string("ref_dir");
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("direcciones");
};