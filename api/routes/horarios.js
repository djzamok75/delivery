//llamar al modelo
const Horarios = require("../models/Horarios");

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const moment = require("moment");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../config");
var uuid = require("uuid");
var sesion = require("./sesion");
var sesi = new sesion();
var archivos = require("../Archivos");
var fs = require("fs-extra");

//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

//Manejador de Errores
const { AuthError, ValidationError } = require("../Errores");

var router = express.Router();

//registrar 
router.post("/add", upload.none(), sesi.validar_restaurante, async (req, res, next) => {
    data = req.body;
    let errr = false;
    let falta = false;
    hor_1_i = data.hora_ini_1;
    hor_1_f = data.hora_fin_1;
    sts_1 = data.status_h_1;
    hor_2_i = data.hora_ini_2;
    hor_2_f = data.hora_fin_2;
    sts_2 = data.status_h_2;
    hor_3_i = data.hora_ini_3;
    hor_3_f = data.hora_fin_3;
    sts_3 = data.status_h_3;
    //console.log(data);
    //var horario = ["LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES", "SABADO", "DOMINGO"];
    var horario = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"];
    //VALIDACIONES PARA CAMPOS VACIOS AL SELECCIONAR LA HORA
    for (var i = 0; i < hor_1_i.length; i++) {
      er1 = "horario 1 del dia ("+horario[i]+")";
      er2 = "horario 2 del dia ("+horario[i]+")";
      er3 = "horario 3 del dia ("+horario[i]+")";
      if((hor_1_i[i]!='' && hor_1_f[i]=='') || (hor_1_i[i]=='' && hor_1_f[i]!='')){
        if(falta)
          falta += "<br>"+er1;
        else
          falta = er1;
      }

      if((hor_2_i[i]!='' && hor_2_f[i]=='') || (hor_2_i[i]=='' && hor_2_f[i]!='')){
        if(falta)
          falta += "<br>"+er2;
        else
          falta = er2;
      }
      
      

      if((hor_3_i[i]!='' && hor_3_f[i]=='') || (hor_3_i[i]=='' && hor_3_f[i]!='')){
        if(falta)
          falta += "<br>"+er3;
        else
          falta = er3;
      }
      
    }
    if(falta) return AR.enviarDatos({r: false, msj: "Falta completar las horas de los horarios:<br>"+falta}, res);

    //VALIDACIONES PARA LOS CAMPOS SELECCIONADOS
    if(sts_1){
      for (var i = 0; i < sts_1.length; i++) {
        if(sts_1[i]){
          er = false;
          if(hor_1_i[i]=='' || hor_1_f[i]==''){
            er = "Falta el horario 1 del dia ("+horario[i]+")";
          } 
          if(er){
            if(falta)
              falta += "<br>"+er;
            else
              falta = er;
          }
        }
      }
    }
    if(sts_2){
      for (var i = 0; i < sts_2.length; i++) {
        if(sts_2[i]){
          er = false;
          fv = new Date('2/1/2020 '+hor_2_i[i]);//darle una fecha especifica para las validaciones
          f1 = new Date('2/1/2020 '+hor_1_i[i]);//darle una fecha especifica para las validaciones
          f2 = new Date('2/1/2020 '+hor_1_f[i]);//darle una fecha especifica para las validaciones

          if(f2<f1){ //si la fecha desde es pm y el hasta am, se valida con fechas diferentes
            f1 = new Date('1/1/2020 '+hor_1_i[i]);
            f2 = new Date('2/1/2020 '+hor_1_f[i]);
          }
          
          if(sts_1 && sts_1[i] && hor_1_i[i]!='' && hor_1_f[i]!='' && fv<f2){
            er = "El horario 2 del dia ("+horario[i]+") no debe estar en el rango del horario 1";
          }

          if(er){
            if(falta)
              falta += "<br>"+er;
            else
              falta = er;
          } 
        }
      }
    }
    if(sts_3){
      for (var i = 0; i < sts_3.length; i++) {
        if(sts_3[i]){
          er = false;
          fv = new Date('2/1/2020 '+hor_3_i[i]);//darle una fecha especifica para las validaciones
          f1 = new Date('2/1/2020 '+hor_1_i[i]);//darle una fecha especifica para las validaciones
          f2 = new Date('2/1/2020 '+hor_1_f[i]);//darle una fecha especifica para las validaciones
          f11 = new Date('2/1/2020 '+hor_2_i[i]);//darle una fecha especifica para las validaciones
          f22 = new Date('2/1/2020 '+hor_2_f[i]);//darle una fecha especifica para las validaciones

          if(f2<f1){ //si la fecha desde es pm y el hasta am, se valida con fechas diferentes
            f1 = new Date('1/1/2020 '+hor_1_i[i]);
            f2 = new Date('2/1/2020 '+hor_1_f[i]);
          }
          if(f22<f11){ //si la fecha desde es pm y el hasta am, se valida con fechas diferentes
            f11 = new Date('1/1/2020 '+hor_2_i[i]);
            f22 = new Date('2/1/2020 '+hor_2_f[i]);
          }
          
          if(sts_2 && sts_2[i] && hor_2_i[i]!='' && hor_2_f[i]!='' && fv<f22){
            er = "El horario 3 del dia ("+horario[i]+") no debe estar en el rango del horario 2";
          }else if(sts_1 && sts_1[i] && hor_1_i[i]!='' && hor_1_f[i]!='' && fv<f2){
            er = "El horario 3 del dia ("+horario[i]+") no debe estar en el rango del horario 1";
          }

          if(er){
            if(falta)
              falta += "<br>"+er;
            else
              falta = er;
          } 
        }
      }
    }
    if(falta) return AR.enviarDatos({r: false, msj: "Se presentan los siguienres errores en los horarios a mostrar:<br>"+falta}, res);
    
    await Horarios.query().delete().where({ id_restaurante: data.iden });
    for (var i = 0; i < hor_1_i.length; i++) {
      if(hor_1_i[i]!='' && hor_1_f!=''){
        val_s_1 = 0;
        if(sts_1 && sts_1[i])
          val_s_1 = sts_1[i];
        await Horarios.query()
          .insert({
            id_restaurante: data.iden,
            dia: horario[i],
            inicio_hor: hor_1_i[i],
            fin_hor: hor_1_f[i],
            estatus: val_s_1,
            horario: '1'
          });
      }
      if(hor_2_i[i]!='' && hor_2_f!=''){
        val_s_2 = 0;
        if(sts_2 && sts_2[i])
          val_s_2 = sts_2[i];
        await Horarios.query()
          .insert({
            id_restaurante: data.iden,
            dia: horario[i],
            inicio_hor: hor_2_i[i],
            fin_hor: hor_2_f[i],
            estatus: val_s_2,
            horario: '2'
          });
      }
      if(hor_3_i[i]!='' &&  hor_3_f!=''){
        val_s_3 = 0;
        if(sts_3 && sts_3[i])
          val_s_3 = sts_3[i];
        await Horarios.query()
          .insert({
            id_restaurante: data.iden,
            dia: horario[i],
            inicio_hor: hor_3_i[i],
            fin_hor: hor_3_f[i],
            estatus: val_s_3,
            horario: '3'
          });
      }
    }

    AR.enviarDatos({r:true, msj: "Horario actualziado correctamente!"}, res);
   

});

//listar 
router.get("/", sesi.validar_general, async(req, res) => {
  await Horarios.query()
      .then(ret => {
      AR.enviarDatos(ret, res);
  });
});

//ver 
router.get("/:id", sesi.validar_general, async(req, res) => {
  await Horarios.query().where("id_restaurante",req.params.id).then(ret => {
      AR.enviarDatos(ret, res);
  });

});




module.exports = router;
