exports.up = knex => {
    return knex.schema.createTable("clientes_favoritos", table => {
        table.increments("id").primary();
        table.integer("id_cliente").unsigned().references('id').inTable('clientes');
        table.integer("id_restaurante").unsigned().references('id').inTable('restaurantes').unique();
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("clientes_favoritos");
};
