$(document).ready(function() {
    $("select.multiple_select_serv").multipleSelect({
        filter: true,
        multiple: true,
        placeholder: "Selecciona un servicio"
    });
	$("select.multiple_select").multipleSelect({
            filter: true,
            multiple: true,
            placeholder: "Selecciona un estudiante"
    });
    $("select.multiple_select_pro").multipleSelect({
        filter: true,
        multiple: true,
        placeholder: "Selecciona un profesor"
    });
    $("select.multiple_select_hor").multipleSelect({
            filter: true,
            multiple: true,
            placeholder: "Selecciona un horario"
    });
    $("select.multiple_select_estat").multipleSelect({
        filter: true,
        multiple: true,
        placeholder: "Selecciona un estatus"
    });
});