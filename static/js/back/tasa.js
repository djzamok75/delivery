function tasajs(){
    $(document).on('click', '.abrir-tasa', function(){
        $("#pre_tasa").val(formato.precio(userDOOMI.restaurante[0].pre_tasa));
        $("#md-tasa").modal("open");
    });
    $(".save-tasa").on('click', function(e) {
        var tasa = $('#pre_tasa').val();
        if (tasa=='') {
            Materialize.toast( "Tasa: Ingresa la tasa actual." , 5000);
            $("#pre_tasa").focus()
            return false;
        }
        if (tasa=='0,00') {
            Materialize.toast( "Tasa: Ingresa un monto valido." , 5000);
            $("#pre_tasa").focus()
            return false;
        }
        act_tasa_fac = $('input:radio[name=opta]:checked').val();
        var data = new FormData();
        data.append("iden", userDOOMI.restaurante[0].id);
        data.append("tasa", Quitarcoma(tasa));
        $('.save-tasa').attr('disabled', true);
        $.ajax({
            url: dominio + "restaurantes/edit_tasa/",
            type: "PUT",
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            data: data,
            contentType: false,
            processData: false,
            success: function(data) {
                Materialize.toast( data.msj , 5000);

                if (data.r) {
                    //pre_tasa = parseFloat(Quitarcoma(tasa));
                    $(".tasa_res").html(formato.precio(Quitarcoma(tasa)));
                    userDOOMI.restaurante[0].pre_tasa = Quitarcoma(tasa);
                    localStorage.setItem('userDOOMI',JSON.stringify(userDOOMI));
                    $("#md-tasa").modal("close");
                    if(getop("op") == "menu")
                        menus();
                }
                $('.save-tasa').attr('disabled', false);
            },
            error: function(xhr, status, errorThrown) {
                console.log("Errr : ");
                $('.save-tasa').attr('disabled', false);
            }
        });
    });

    //FUNCIONES DEL LABORANDO
    comprobarsts();
    function comprobarsts(){
        let data = {};
        $.ajax({
            url: dominio + 'restaurantes/'+userDOOMI.restaurante[0].id,
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'GET',
            data: data,
            success: function(data) {
                if(data.laborando==1)
                    $("#sts_laborando").attr('checked', true);
            }, error: function(xhr, status, errorThrown) {
                console.log(errorThrown);
            }
        })
    }

    $(document).on('click', "#sts_laborando", function(){
        v=0;
        if($("#sts_laborando:checked").length>0)
          v = 1;
        var data = new FormData();
        data.append("iden", userDOOMI.restaurante[0].id);
        data.append("sts", v);
        $.ajax({
            method: "PUT",
            url: dominio + 'restaurantes/edit_sts_laborando',
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            data: data,
            contentType: false,
            processData: false,
            success: function(data) {
                Materialize.toast( data.msj , 5000);
            },
            error: function(xhr, status, errorThrown) {
                console.log("Errr : ");
            }
        });
    });
}
