function configuracionjs(){
    listconfi();
   
    function listconfi(){
        $.ajax({
            url : dominio+'configuracion/',
            type: 'GET',
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            data:{},
            success:function(data){
                //console.log(data);
                actualizar_confi(data)
            },
            error: function(xhr, status, errorThrown){
                console.log(xhr);
            }
        });
    }
    function actualizar_confi(data){
        $("#tar_min").val(formato.precio(data.minimo));
        $("#ran_km").val(data.km);
        $("#pre_km").val(formato.precio(data.precio));
    }
    $("#form-config").on('submit', function(e) {
        e.preventDefault();
        var data = new FormData(this);
        $('.save-con').attr('disabled', true);
        $.ajax({
            url: dominio + "configuracion/",
            type: "POST",
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            data: data,
            contentType: false,
            processData: false,
            success: function(data) {
                Materialize.toast( data.msg , 5000);
                if(data.r){
                    $("#md-configuracion").modal('close');
                    $("#tar_min").val(formato.precio($("#tar_min").val()));
                    $("#pre_km").val(formato.precio($("#pre_km").val()));
                }
                $('.save-con').attr('disabled', false);
            },
            error: function(xhr, status, errorThrown) {
                console.log("Errr : ");
                $('.save-con').attr('disabled', false);
            }
        });
    });
}
