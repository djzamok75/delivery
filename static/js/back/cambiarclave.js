function cambiarclavejs(){

    
    //submit al cambiar clave
    $("#cambiar_clave").on('submit', function(e) {
        e.preventDefault();

        var pass = $('#pass').val();
        var pass2 = $('#pass_new').val()
        var pass3 = $('#pass_new_con').val()
        if (pass.length < 4) {
            Materialize.toast( "Contraseña actual: Mínimo 4 caracteres." , 5000);
            $("#pass").focus()
            return false;
        }
        if (pass.length < 4) {
            Materialize.toast( "Contraseña nueva: Mínimo 4 caracteres." , 5000);
            $("#pass_new").focus()
            return false;
        }
        if (pass.length < 4) {
            Materialize.toast( "Confirmar contraseña: Mínimo 4 caracteres." , 5000);
            $("#pass_new_con").focus()
            return false;
        }
        var data = new FormData(this);
        data.append('usuario', userDOOMI.tipo_usu);
        data.append("id", userDOOMI.id);
        $('#bt_pass').attr('disabled', true);
        $.ajax({
            url: dominio + "login/cambiar-clave",
            type: "POST",
            data: data,
            contentType: false,
            processData: false,
            success: function(data) {
                Materialize.toast( data.msg , 5000);
                if (data.msg == "Ha cambiado su contraseña satisfactoriamente.") {
                    $('#cambiar_clave').trigger("reset");
                }
                $('#bt_pass').attr('disabled', false);
            },
            error: function(xhr, status, errorThrown) {
                //Materialize.toast({html:err.data.msj}, 5000);
                console.log("Errr : ");
                $('#bt_pass').attr('disabled', false);
            }
        });

    });
}
