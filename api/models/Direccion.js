"use strict";

const { Modelo } = require("./Modelo");

class direccion extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "direcciones";
    }

}

module.exports = direccion;
