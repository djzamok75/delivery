function listar_menus() {

    var id_categoria_s = false;
    var id_estatus_s = false;
    const route = "menu/";

    $("#cat_men, #est_men")
        .select2({placeholder: "Seleccione", allowClear: false});

    ajaxCategorias();
    ajaxEstatus();

    var imgEdit = false;
    var img64_1 = '';
    
    var output = document.getElementById('imagen');
    var el = document.getElementById('resizer-demo');
    var resize = new Croppie(el, {
        viewport: { width: 180, height: 180 },
        boundary: { width: 210, height: 210 },
        showZoomer: true,
        enableResize: false,
        enableOrientation: true,
        mouseWheelZoom: 'ctrl'
    });


    abrirEditor = function abrirEditor(e,op) {
        if (e.files[0]) {
            if(op==0){
                output.src = URL.createObjectURL(event.target.files[0]);
                resize.bind({
                    url: output.src,
                });
                imgEdit = true;
            }
        }
        return;
    }

    clickNuevoMenu = function clickNuevoMenu(){
        // Validar que la tasa actual > 0 y que hayan categorias.
        let errr;
        msjs = [];
        if (!userDOOMI.restaurante[0].pre_tasa){
            errr = true;
            msjs.push('La tasa actual debe ser mayor a cero');
        }
        if ($('#cat_men > option').length == 1){
            errr = true;
            msjs.push('No hay categorias');
        };
        // Fin de validacion.

        if (errr) {
            // Quitar enlace e interaccion con el puntero.
            document.getElementById('nuevoMenuBtn').setAttribute('href', '#');
            document.getElementById('nuevoMenuBtn').style.pointerEvents = 'none';
            
            msjs.forEach((msj)=>{
                Materialize.toast(msj, 4000, '', function(){
                    // Regresar enlace e interaccion luego del Toast.
                    document.getElementById('nuevoMenuBtn').setAttribute('href', '#md-nuevoMenu');
                    document.getElementById('nuevoMenuBtn').style.pointerEvents = 'auto';
                });
            });
            return false;
        }
        $('#imagen').attr('src', '../../static/img/user.png');
        resize.bind(output.src);
        img64_1 = '';imgEdit = false;

        $('#form_registrar_menu')[0].reset();
        $('#iden').val('');
        $('.bt_save').html('Guardar');
        $('#tit-mod').html('Nuevo menu');
        $('.all_img').attr('src', '../../static/img/logo.jpg');
        $('#cat_men, #est_men').select2({placeholder: 'Seleccione', allowClear: false});
        $('#lista-adicionales').html('');
    };

    menus = function menus() {
        data = {
            id_res: userDOOMI.restaurante[0].id
        };
        $.ajax({
            url: dominio + route,
            headers: {
                'token': userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'GET',
            data: data,
            success: function(data){
                actualizar_tabla_menus(data)
            }
        })
    }

    // Listar.
    menus();

    function actualizar_tabla_menus(data) {
        var table = $('.table').DataTable({
            "language": {
                "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0;
        table.clear().draw();

        for (var i = 0; i < data.length; i++) {
            menu = data[i];

            let precios = `$${menu.precio_usd} / Bs. ${formato.precio(menu.precio_usd * userDOOMI.restaurante[0].pre_tasa)}`;
            let estatusSwitch = `<div class="switch"><label><input type="checkbox" id="${menu.id}" ${menu.estatus?'checked="true"':''} class="btn btn-primary btn-floating tooltipped estatus-switch"><span class="lever"></span></label></div>`;


            estatusSwitch = `<div class="switch">
                            <small class="clr_primary" style="font-size: 10px;">No disponible</small>
                            <label>
                            
                            <input type="checkbox"  id="${menu.id}" ${menu.estatus?'checked="true"':''} class="btn btn-primary btn-floating tooltipped estatus-switch">
                                <span class="lever"></span> <small class="clr_primary" style="font-size: 10px;">Disponible</small>
                            </label>
                        </div>`;


            var options = `
                <a href="#!" id="${menu.id}" class="btn btn-primary btn-floating ver-info tooltipped" data-position="bottom" data-delay="50" data-tooltip="Ver información del menú"><i class="fa fa-eye"></i></a>
                <a href="#!" id="${menu.id}" class="btn btn-primary btn-floating edit-info tooltipped" data-position="bottom" data-delay="50" data-tooltip="Editar información del menú"><i class="fa fa-edit"></i></a>
                <a href="#!" id="${menu.id}" class="waves-effect btn btn-primary btn-floating tooltipped delete" data-position="bottom" data-delay="50" data-tooltip="Eliminar menú"><i class="fa fa-times"></i></a>
            `;
            var row = [count+=1
                ,menu.nombre
                ,menu.descripcion
                ,precios
                ,menu.categoria.nombre
                ,estatusSwitch
                ,options
            ];
            table.row.add(row).draw().node();
        }
        $('.tooltipped').tooltip();
    }

    $("#form_registrar_menu").on("submit", function(e){
        e.preventDefault();

        // Validaciones.
        var errr = false;
        var msj = false;

        let adi_precios_incompletos = 0;
        let adi_nombres_incompletos = 0;

        $('.adi-nombres').each(function() {
            let val = $(this).val();
            if (val.trim() == '') adi_nombres_incompletos++;
        });
        $('.adi-precios').each(function() {
            let val = $(this).val();
            if (val.trim() == '') adi_precios_incompletos++;
        });

        if ($("#nom_men").val().trim() == ""){
            errr=true;
            msj = "Ingrese el nombre del menú";
        }
        else if($("#exi_men").val().trim() == ""){
            errr = true;
            msj = "Completar campo existencias";
        }
        else if(isNaN($("#exi_men").val().trim())){
            errr = true;
            msj = "El campo existencias es numérico";
        }
        else if ($("#exi_men").val().trim() < 0){
            errr = true;
            msj = "El campo existencias debe ser mayor o igual a cero";
        }
        else if ($("#descripcion").val().trim() == ""){
            errr = true;
            msj = "Ingrese una descripción";
        }
        else if ($("#usd_men").val().trim() == "" ||
                $("#usd_men").val().trim() == 0){
            errr = true;
            msj = "Ingrese el precio";
        }
        else if ($("#cat_men").val().trim() == ""){
            errr = true;
            msj = "Seleccione la categoría";
        }
        else if ($("#est_men").val().trim() == ""){
            errr = true;
            msj = "Seleccione el estatus";
        }
        else if(adi_nombres_incompletos){
            errr = true;
            msj = 'Se deben ingresar los nombres de los adicionales';
        }
        else if(adi_precios_incompletos){
            errr = true;
            msj = 'Se deben ingresar los precios de los adicionales';
        }
        // Fin de validaciones.

        if (errr) {
            Materialize.toast(msj, 4000);
            return false;
        }
        // Data a enviar.
        var data = new FormData(this);
        data.append("id_res", userDOOMI.restaurante[0].id);

        // Convertir decimales a un flotante normal.
        data.set("usd_men", Quitarcoma($("#usd_men").val()));

        // Convertir precio de cada adicional.
        data.delete("adicionales_precios[]");
        data.delete("adicionales_nombres[]");

        $('.adi-precios').each(function() {
            const val = $(this).val();
            if (val.trim() == '') return;

            const this_tr_adi = $(this).attr('tr-adi'); // Numero de fila.
            const nomElem = $(`[iden="adinom_${this_tr_adi}"]`)[0]; // Donde ira el precio en Bs.

            data.append("adicionales_nombres[]", $(nomElem).val());
            data.append("adicionales_precios[]", Quitarcoma(val));
        });

        if($("#iden").val() != ''){
            url = "menu/edit";
            tipo = "PUT";
        }
        else {
            url = "menu/add";
            tipo = "POST";
        }
        resize.result('base64').then(function(base64) {
            if(imgEdit)
                img64_1 = base64;
            data.append("imgweb", "1");
            data.append("img", img64_1);
            $(".bt_save").prop("disabled", true);
            $(".fileprogress").removeClass("hide");
            $(".fileprogress").find("div")[0].style.width = "0%";
            $.ajax({
                url: dominio + url,
                headers: {
                    'token': userDOOMI.token,
                    'tipo_usuario': userDOOMI.tipo_usu
                },
                type: tipo,
                dataType: "json",
                cache: false,
                data: data,
                contentType: false,
                processData: false,
                xhr: function(){
                    var xhr = new window.XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function(e){
                        if (e.lengthComputable) {
                            var percentComplete = parseInt((e.loaded/e.total*100), 10);
                            $(".fileprogress").find("div")[0].style.width = `${percentComplete}%`;
                            if (percentComplete >= 100){
                                $(".fileprogress").addClass("hide");
                                $(".fileprogress").find("div")[0].style.width ="0%";
                            }
                        }
                    }, false);
                    
                    xhr.addEventListener("progress", function(e){
                        if (e.lengthComputable){
                            var percentComplete = parseInt((e.loaded/e.total*100), 10);
                            $(".fileprogress").find("div")[0].style.width = `${percentComplete}%`;
                            if(percentComplete>=100){
                                $(".fileprogress").addClass("hide");
                                $(".fileprogress").find("div")[0].style.width = "0%";
                            }
                        }
                    }, false);
                    
                    return xhr;
                },
                success: function(res){
                    Materialize.toast(res.msj, 4000)
                    if (res.r == true){
                        $("#form_registrar_menu")[0].reset();
                        menus();
                        $("#md-nuevoMenu").modal('close');
                        $("#cat_men, #est_men")
                            .select2({placeholder: "Seleccione", allowClear: false});
                    }
                    // Botones del form.
                    $(".bt_save").prop("disabled", false)
                },
                error: function(xhr, status, errorThrown){
                    console.log(errorThrown);
                    
                    $(".fileprogress").addClass("hide");
                    $(".fileprogress").find("div")[0].style.width = "0%";

                    Materialize.toast(errorThrown.msj, 4000)

                    //botones del form
                    $(".bt_save").prop("disabled", false)
                }
            });
        });
    });

    function buscar_data(idb, modal){
        data = {
            id_res: userDOOMI.restaurante[0].id
        };
        $.ajax({
            url: dominio + "menu/"+idb,
            headers: {
                "token": userDOOMI.token
            },
            type: "GET",
            data: data,
            success: function(data){
                imprimir_data_menu(data, modal)
            },
            error: function(xhr, status, errorThrown){
                console.log(errorThrown);
            }
        });
    };
    function imprimir_data_menu(data, modal) {
        if (modal == "ver"){
            $(".nom-view").html(data.nombre);
            $(".desc-view").html(data.descripcion);
            $(".pres-usd-view").html(data.precio_usd);
            $(".pres-bs-view").html(formato.precio(data.precio_usd * userDOOMI.restaurante[0].pre_tasa));
            $(".exi-view").html(data.existencias);
            $(".cat-view").html(data.categoria);
            $(".est-view").html(data.estatus?"Disponible":"No disponible");

            $(".adi-view").html("");
            if(data.adicionales[0]){
                let html_adi = `
                    <table class="responsive-table">
                        <tr>
                            <th>Adicionales:</th>
                            <th class="s2 m2 l2">Precio USD</th>
                            <th>Precio Bs</th>
                        </tr>
                    `;
                for (adicional of data.adicionales){
                    html_adi += `
                        <tr>
                            <td>${adicional.nombre}</td>
                            <td>${formato.precio(adicional.precio_usd)}</td>
                            <td>${formato.precio(adicional.precio_usd * userDOOMI.restaurante[0].pre_tasa)}</td>
                        </tr>
                    `;
                }
                html_adi += "</table>";
                $(".adi-view").html(html_adi);
            }
            else {
                $('.adi-view').html('<b>Adicionales: </b> Sin adicionales.');
            }

            $(".img-icono").attr("src", "../../static/img/logo.jpg");
            if(data.img)
                $(".img-icono").attr("src", data.img);

            $("#md-verMenu").modal("open");
        }
        else if (modal == "editar") {
            id_categoria_s = data.id_categoria;
            ajaxCategorias();


            $('#est_men').html('<option value="" selected disabled>Cargando...</option>');
            html = `
                    <option value="" selected>Seleccione</option>
                    <option value="1">Disponible</option>
                    <option value="0">No disponible</option>
                `;


            $("#est_men").html(html);
            $("#est_men").val(data.estatus).trigger("change");
            $("#est_men").select2({placeholder: "Seleccione", allowClear: false});


            $("#nom_men").val(data.nombre);
            $("#descripcion").val(data.descripcion);
            $("#usd_men").val(formato.precio(data.precio_usd));
            $("#bs_men").val(formato.precio(data.precio_usd * userDOOMI.restaurante[0].pre_tasa));
            $("#exi_men").val(data.existencias);

            if(data.img){
                output.src = data.img;
                resize.bind({url: output.src,zoom: 1});
                setTimeout(function(){ $(".cr-image").css("transform","translate3d(15px, 15px, 0px) scale(1)"); }, 200);
            }

            img64_1 = '';imgEdit = false;

            $('#lista-adicionales').html('');
            if(data.adicionales[0]){
                let html_adi = '';
                for (let i = 0; i < data.adicionales.length; i++) {
                    let adicional = data.adicionales[i];

                    html_adi += `
                        <tr id="tr-e${adicional.id}">
                            <td class="p-1">${i+1}</td>
                            <td class="p-1"><div class="input-field s12"><input value="${adicional.nombre}" placeholder="Ingrese el nombre" name="adicionales_nombres[]" type="text" tr-adi="${i+1}" iden="adinom_${i+1}" class="form-control mb-0 adi-nombres"></div></td>
                            <td class="p-1"><div class="input-field s12"><input value="${formato.precio(adicional.precio_usd)}" placeholder="Ingrese el precio" name="adicionales_precios[]" type="text" class="Amount validate form-control mb-0 adi-precios" tr-adi="${i+1}" iden='adiusd_${i+1}' data-decimal="," data-thousands="."></div></td>
                            <td class="p-1"><div class="input-field s12"><input disabled placeholder="" name="" type="text" class="form-control mb-0" tr-adi="${i+1}" iden='adibs_${i+1}'></div></td>
                            <td class="p-1"><div class="input-field col s12 p-0"><input type="hidden" name="adicionales_ids[]" value="${adicional.id}"></div></td>
                            <td class="text-center">
                                <div class="input-field m-1 p-0 col s12">
                                    <a class="waves-effect btn btn-primary btn-floating delete-adi" id="${adicional.id}"><i class="fa fa-trash"></i></a>
                                </div>
                            </td>
                        </tr>
                    `;

                    if(tr_adi < adicional.id) tr_adi = adicional.id+1;
                }
                $("#lista-adicionales").html(html_adi);

                // Para que reconozca lns elementos nuevos.
                $('.Amount').maskNumber({decimal:',',thousands:'.'}).trigger('change');
            }

            $("#iden").val(data.id);
            $("#tit-mod").html("Editar menú");
            $(".bt_save").html("Guardar cambios");

            $("#md-nuevoMenu").modal("open");
        }
    }

    // Adicional.
    $(document).on('click', '.delete-adi', function() {
        var idv = this.id;
        var toastContent = '<span>¿Desea eliminar éste registro?</span><br><button class="btn-flat toast-action conf_si_delete_adi" id="'+idv+'">Si</button><button class="btn-flat toast-action" onclick=" $(\'.toast\').hide(); ">No</button>';

        Materialize.toast(toastContent, 4000);
    });
    $(document).on('click', '.conf_si_delete_adi', function() {
        id = this.id;

        $('.toast').hide();
        $.ajax({
            url: dominio + 'menu/adicionales/'+id,
            headers: {
                'token':userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            type: 'DELETE',
            data: {},
            success: function(data) {
                if (data.msj) {
                    Materialize.toast(data.msj, 10000);
                    $("#tr-e"+id).remove();
                } else {
                    Materialize.toast(data.msj, 10000);
                }
            }
        })

    });


    tr_adi = 0;
    $(document).on('click', '.add-adi', function() {
        let html = `<tr id="tr-${tr_adi}">
                <td class="p-1"><span class="caja-new">Nuevo</span></td>
                <td class="p-1"><div class="input-field s12"><input placeholder="Ingrese el nombre" name="adicionales_nombres[]" type="text" tr-adi="${tr_adi}" iden="adinom_${tr_adi}" class="form-control mb-0 adi-nombres"></div></td>
                <td class="p-1"><div class="input-field s12"><input placeholder="Ingrese el precio" name="adicionales_precios[]" type="text" class="Amount validate form-control mb-0 adi-precios" tr-adi="${tr_adi}" iden='adiusd_${tr_adi}' data-decimal="," data-thousands="."></div></td>
                <td class="p-1"><div class="input-field s12"><input disabled placeholder="" name="" type="text" class="form-control mb-0" tr-adi="${tr_adi}" iden='adibs_${tr_adi}'></div></td>
                <td class="p-1"><div class="input-field col s12 p-0"><input type="hidden" name="adicionales_ids[]" value="0"></div></td>
                <td class="text-center">
                    <div class="input-field m-1 p-0 col s12">
                        <a class="waves-effect btn btn-primary btn-floating delete-adi-tr" id="-${tr_adi}"><i class="fa fa-trash"></i></a>
                    </div>
                </td>
            </tr>
        `;
        tr_adi++;
        $("#lista-adicionales").append(html);

        // Para que reconozca lns elementos nuevos.
        $('.Amount').maskNumber({decimal:',',thousands:'.'});
    });

    $(document).on('click', '.delete-adi-tr', function() {
        var idv = this.id;
        $("#tr"+idv).remove();
    });


    // Conversion de precio en Adicionales.
    $("#lista-adicionales").on('input selectionchange propertychange change', '.adi-precios', async function() {
        const this_tr_adi = $(this).attr('tr-adi'); // Numero de fila.
        const bsElem = $(`[iden="adibs_${this_tr_adi}"]`)[0]; // Donde ira el precio en Bs.
        const precio_usd = $(this).val();

        const precio_bs = formato.precio(Quitarcoma(precio_usd) * userDOOMI.restaurante[0].pre_tasa);

        $(bsElem).val(precio_bs);
    })



    $(document).on("click", ".ver-info", function(){
        var idv = this.id;
        buscar_data(idv, "ver");
    });
    $(document).on("click", ".edit-info", function(){
        var idv = this.id;
        buscar_data(idv, "editar")
    });
    $(document).on('click', '.delete', function() {
        var idv = this.id;
        var toastContent = '<span>¿Desea eliminar este menú?</span><br><button class="btn-flat toast-action conf_si" id="'+idv+'">Si</button><button class="btn-flat toast-action" onclick=" $(\'.toast\').hide(); ">No</button>';

        Materialize.toast(toastContent, 4000);
    });
    $(document).on('click', '.conf_si', function() {
        id = this.id
        var data = {
            id_res: userDOOMI.restaurante[0].id
        };

        $('.toast').hide();
        $.ajax({
            url: dominio + 'menu/delete/'+id,
            headers: {
                'token':userDOOMI.token
            },
            type: 'DELETE',
            data: data,
            success: function(data) {
                if (data.msj) {
                    Materialize.toast(data.msj, 10000);
                    menus();
                } else {
                    Materialize.toast(data.msj, 10000);
                }
            }
        })
    });

    $(document).on('click', '.sorting_1', function() {
        $('.tooltipped').tooltip();
    });

    // Para el cambio de estado en los switches.
    $("table").on('change', '.estatus-switch', async function() {
        let data = {
            iden:this.id,
            nuevo_estatus: this.checked ? 1 : 0
        };

        $.ajax({
            url: dominio + "menu/edit_estatus"
            ,headers: {token:userDOOMI.token}
            ,data: data
            ,type:"PUT"
            ,success: function(data) {Materialize.toast(data.msj, 3000)}
            ,error: function(xhr, status, errorThrown){
                console.log(errorThrown);
                menus();
            }
        });
    });

    // Para la conversion de usd a bolivares mientras se ingresa.
    $('#usd_men').on('input selectionchange propertychange change', function(){
        $("#bs_men").val(
            formato.precio(Quitarcoma($("#usd_men").val()) * userDOOMI.restaurante[0].pre_tasa)
        ).trigger('propertychange');
    });

    // Limpiar entrada numerica.
    $("#exi_men").on("input propertychange change", function() {
        this.value = extraerDigitos(this.value);
    });



    function ajaxCategorias() {
        $('#cat_men').html('<option value="" selected disabled>Cargando...</option>');
        data = {
            id_res: userDOOMI.restaurante[0].id
        };
        $.ajax({
            url: dominio + "categorias_menu/",
            type: "GET",
            data: data,
            headers: {
                'token': userDOOMI.token,
                'tipo_usuario': userDOOMI.tipo_usu
            },
            success: function(data){
                html = '<option value="" selected>Seleccione</option>';
                for (var i = 0; i < data.length; i++) {
                    html += '<option value="' + data[i].id + '">' + data[i].nombre + '</option>';
                }

                $("#cat_men").html(html);

                if (id_categoria_s) {
                    $("#cat_men").val(id_categoria_s).trigger("change");
                }
                $("#cat_men").select2({placeholder: "Seleccione", allowClear: false});
            }
        });
    }

    function ajaxEstatus() {
        $('#est_men').html('<option value="" selected disabled>Cargando...</option>');
        html = `
                <option value="" selected>Seleccione</option>
                <option value="1">Disponible</option>
                <option value="0">No disponible</option>
            `;
        $("#est_men").html(html);

        if (id_estatus_s) $("#est_men").val(id_estatus_s).trigger("change");

        $("#est_men").select2({placeholder: "Seleccione", allowClear: false});
    }
};
