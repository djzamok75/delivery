"use strict";

const fs = require("fs");
//const ruta = (__dirname + "/../assets/logo.png")
//var bitmap = fs.readFileSync(ruta)

const config = require("../config");

//const logo = bitmap.toString('base64');

module.exports = function(data) {
    return `
    <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <title>Document</title>
</head>
<body>
    <table cellpadding="0" cellspacing="0" border="0" align="center" style="background-color:#f9f9f9;border-collapse:collapse;line-height:100%!important;margin:0;padding:0;width:100%!important" bgcolor="#f9f9f9">
        <tbody>
            <tr>
                <td>
                    <table style="border-collapse:collapse;margin:auto;max-width:635px;min-width:320px;width:100%" class="m_5191768325981104092main-wrap">
                        <tbody>
                            <tr>
                                <td valign="top">
                                    <table cellpadding="0" cellspacing="0" border="0" class="m_5191768325981104092reply_header_table" style="border-collapse:collapse;color:#c0c0c0;font-family:'Helvetica Neue',Arial,sans-serif;font-size:13px;line-height:26px;margin:0 auto 26px;width:100%">
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" style="padding:0 20px">
                                    <table cellpadding="0" cellspacing="0" border="0" align="center" style="background-clip:padding-box;border-collapse:collapse;border-radius:3px;color:#545454;font-family:'Helvetica Neue',Arial,sans-serif;font-size:13px;line-height:20px;margin:0 auto;width:100%">
                                        <tbody>
                                            <tr>
                                                <td valign="top">
                                                    <table cellpadding="0" cellspacing="0" border="0" style="border:none;border-collapse:separate;font-size:1px;height:2px;line-height:3px;width:100%">
                                                        <tbody>
                                                            <tr>
                                                                <td valign="top" style="background-color:#40A85D;border:none;font-family:'Helvetica Neue',Arial,sans-serif;width:100%" bgcolor="#40A85D">&nbsp;</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                     <!--Header-->
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#FFFFFF;" bgcolor="#3498db">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                                                <tr>
                                                                    <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                                                        <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td align="center" valign="top" style="padding:5px;justify-content: space-between;"  class="textContent flex">
                                                                                    <div style="padding: 10px;" class="flex">
                                                                                        <div style="padding: 5px;border-radius: 50%;opacity: .5;" class="">
                                                                                            <span class="material-icons">
                                                                                                app_settings_alt
                                                                                                </span>
                                                                                           
                                                                                        </div>
                                                                                        <p style="margin: 0;" class="fuente" >App</p>
                                                                                    </div>
                                                                                    <div style="padding: 10px;" class="flex">
                                                                                        <div style="padding: 5px;border-radius: 50%;opacity: .5;" class="">
                                                                                            <span style="opacity: 1;" class="material-icons"> notifications_active</span>
                                                                                        </div>
                                                                                        <p style="margin: 0;" class="fuente" >Notificaciones</p>
                                                                                    </div>
                                                                                   
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                  <!--Header-->
                                                   <!--Banner-->
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="color:#FFFFFF;" bgcolor="#3498db">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                                                <tr>
                                                                    <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                                                        <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td align="center" valign="top" style="padding:0;" class="textContent">
                                                                                    <center>
                                                                                        <!--Logo-->
                                                                                        <img src="${config.dominioWeb}static/img/logo.png" title="logo" alt="logo" width="200px" height="auto" style="max-width:50%;"><br><br>
                                                                                        <!--Logo-->
                                                                                        <div mc:edit="body" style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;color:#5F5F5F;line-height:135%;">
                                                                                           
                                                                                    </center>
                                                                                   
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!--Banner-->
                                                    <table cellpadding="0" cellspacing="0" border="0" style="background-clip:padding-box;border-collapse:collapse;border-color:#dddddd;border-radius:0 0 3px 3px;border-style:solid solid none;border-width:0 1px 1px;width:100%">
                                                        <tbody>
                                                            <tr>
                                                                <td style="background-clip:padding-box;background-color:white;border-radius:0 0 3px 3px;color:#525252;font-family:'Helvetica Neue',Arial,sans-serif;font-size:15px;line-height:22px;overflow:hidden;padding-left:40px;padding-right: 40px;padding-top: 10px;padding-bottom: 30px;" bgcolor="white">
                                                                    <div style="margin-bottom:16px;margin-top:0;padding-top:0;text-align:center!important" align="center">
                                                                       
                                                                    </div>
                                                                    <h2 style="color:#282f33;font-size:18px;font-weight:bold;margin:30px 0 7px;text-align:center!important" align="center">
                                                                        FORMULARIO CONTACTO
                                                                    </h2>
                                                                    <p style="line-height:1.5;margin:0 0 17px;text-align:left!important" align="left"><br>Nombre: <b>${
                                                                        data.nombre
                                                                    }</b></p>
                                                                    <p>Apellido: <b>${
                                                                        data.apellido
                                                                    }</b></p>
                                                                    <p>Correo: <b>${
                                                                        data.correo
                                                                    }</b></p>
                                                                    <p>Descripción: <br><b>${
                                                                        data.descripcion
                                                                    }</b></p>
                                                                    <p style="line-height:1.5;margin:0 0 17px;text-align:left!important" align="left">
                                                                        
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                    <div style="display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;width:0">
                                        <table cellpadding="0" cellspacing="0" border="0" align="center" style="border-collapse:collapse;color:#545454;display:none;font-family:'Helvetica Neue',Arial,sans-serif;font-size:13px;height:0;line-height:20px;margin:0 auto;max-height:0;max-width:100%;opacity:0;overflow:hidden;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" width="80" style="color:#272727;height:18px;padding-left:40px;text-align:left" align="left">
                                                        <img alt="Triangle" height="18" src="https://ci4.googleusercontent.com/proxy/9Y1p5EWpjsisMhMUd9iRWBGSpCCzryQp9437rQYtk9fHQCqD3v0QXECLRlIEEDj9fpWGPiOKTWOEg5nKV1-hkYIVpzTmot4bPgS9jou00ShxYQlxF1EKNnUXKyba1xhXrNoF-0t5E9jf6NR1-BJ1LfJKzY5t_qM08f7pOESk=s0-d-e1-ft#https://marketing.intercomassets.com/assets/email/personal/triangle-8747882e9ef8882f9bc057241fd3c049.png" style="display:inline-block;max-width:100%;outline:none;text-decoration:none" width="40">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                    <div>
                                        <table cellpadding="0" cellspacing="0" border="0" align="center" style="border-collapse:collapse;margin:0 auto;max-width:100%;width:100%">
                                            <tbody>
                                                <tr>
                                                    <td valign="top" width="100%">
                                                        <img alt="arrow" src="https://ci5.googleusercontent.com/proxy/Wk07so5PXq8lsSwIUO9c6ah52RLjXWk7k2BErhQH_i6_zPLk9Q4si6YDsfkgAhE6IvUpjRmPRjPCWPeC01WAUiiFclRdCpUQWzpXH43-YUjPRSPGweOtYg32zpyIwPjzEbJJNCqdozRICpO9jGnYNwwJd2NQGeK1HA8LDLmk0S8SCDaxeK-FfGv_Se0LHYSH-5Kg6DCc1e599qBF=s0-d-e1-ft#https://marketing.intercomassets.com/assets/email/personal/arrow-37f6774809df6fd083bfc98e9d562e23ca6ede618e2b5e10c042de88d2f858dd.png" style="max-width:100%;width:100%">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    
                                    <table cellpadding="0" cellspacing="0" border="0" align="center" style="border-collapse:collapse;color:#545454;font-family:'Helvetica Neue',Arial,sans-serif;font-size:13px;line-height:20px;margin:0 auto;max-width:100%;width:100%">
                                        <tbody>
                                            <tr>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td width="75%">
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;color:#545454;font-family:'Helvetica Neue',Arial,sans-serif;font-size:13px;line-height:20px;margin:0 auto;max-width:100%;width:100%">
                                                        <tbody>
                                                            <tr>
                                                                <td width="40">&nbsp;</td>
                                                                <td valign="middle" width="50" style="color:#272727" align="left">
                                                                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQoaQkROP6x7DxU2aTxzcZFpnYtjRAXvUETy85m2xn7KanGK9km-A" height="40" width="40" alt="user admin" style="background-clip:padding-box;border-radius:50px;display:inline-block;height:40px;max-width:100%;outline:none;text-decoration:none;width:40px">
                                                                    <span class="HOEnZb">
                                                                      <font color="#888888"></font>
                                                                    </span>
                                                                </td>
                                                                <td style="color:#999999">
                                                                    &copy; 2018 - NodejsVirgin</span>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" height="20"></td>
            </tr>
        </tbody>
    </table>
    <style>
        .fuente{
            font-family:Helvetica,Arial,sans-serif;
        }
        .flex{
            display: flex;
            align-items: center;
        }
    </style>
</body>
</html>
`;
};
