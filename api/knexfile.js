module.exports = {
    Hosname: "mail.pladecompany.com",
    PORT: 1210,
    development: {
        client: "mysql",
        connection: {
            host: "localhost",
            user: "root",
            password: "",
            database: "doomi"
        },
        migrations: {
            directory: "./migrations"
        },
        seeds: {
            directory: "./seeds"
        },
        firebase : {
            firebase_key_server: ""
        }
    },
    production: {
        client: "mysql",
        connection: {
            host: "localhost",
            user: "root",
            password: "",
            database: "doomi"
        },
        migrations: {
            directory: "./migrations"
        },
        seeds: {
            directory: "./seeds"
        },
        firebase : {
            firebase_key_server: ""
        }
    }
};
