
//datatables
if(typeof userDOOMI == "undefined"){
    userDOOMI = {};
}
$('.table').DataTable( {
    "language": {
    "url": "../../static/lib/JTable/Spanish.json"
    }
} );
$('.table').DataTable( {
    responsive: true
} );

$('.modal').modal();
$("select").css("display","block");

$( "#openNotificaciones" ).click(function() {
  $( "#notifications-dropdown" ).dropdown();
});

$(".button-collapse").sideNav();
$('.tooltipped').tooltip();
//FIN datatables

//función para calcular edad a partir de una fecha
function calcularEdad(fecha) {

    var hoy = new Date();
    var cumpleanos = new Date(fecha);
    var edad = hoy.getFullYear() - cumpleanos.getFullYear();
    var m = hoy.getMonth() - cumpleanos.getMonth();

    if (m < 0 || (m === 0 && hoy.getDate() < cumpleanos.getDate())) {
        edad--;
    }
    return edad;

}

//retornar fecha formato yyyy--mm-dd
function retornar_fecha_ymd(fecha){
    var fecha_retornar = new Date(fecha)

    var mes = ""; var dia = "";
    if ( (fecha_retornar.getMonth()+1) < 10 ) {mes = "0"+(fecha_retornar.getMonth()+1)} else { mes = (fecha_retornar.getMonth()+1) }
    if ( ( fecha_retornar.getDate() ) < 10 ) {dia = "0"+(fecha_retornar.getDate())} else { dia = (fecha_retornar.getDate()) }

    fecha_retornar = fecha_retornar.getFullYear()+"-"+mes+"-"+dia;
    return fecha_retornar
}
//retornar fecha formato dd--mm-yyyy
function retornar_fecha_dmy(fecha, sumar_a_fecha){
    var fecha_retornar = new Date(fecha)

    //console.log(fecha_retornar);
    if (sumar_a_fecha) {
        dia = fecha_retornar.setDate(fecha_retornar.getDate() + 1)
        //dia = dias+1;
        //if(dia<10){ dia = "0"+dia; }
    }
    
    var mes = ""; var dia = "";
    if ( (fecha_retornar.getMonth()+1) < 10 ) {mes = "0"+(fecha_retornar.getMonth()+1)} else { mes = (fecha_retornar.getMonth()+1) }
    if ( ( fecha_retornar.getDate() ) < 10 ) {dia = "0"+(fecha_retornar.getDate())} else { dia = (fecha_retornar.getDate()) }

    fecha_retornar = dia+"-"+mes+"-"+fecha_retornar.getFullYear();
    return fecha_retornar
}

//finalizar sesión de usuarios
function salir(){
    localStorage.removeItem('userDOOMI');
    location.href = '../?op=iniciarsesion';
};

function Quitarcoma(value) {
    if(value){
        for (var i = 0; i < value.split(".").length+10; i++) {
            value = value.replace(".", "");
        }
        value = value.replace(",", ".");
    }
    if(value.length=="3" || value.length=="2"){
        if(value.split(".").length==0)
            value = "0."+""+value;
        else
        value = "0"+""+value;
    }
    return parseFloat(value);
}

function verificarRIF(rif) {
    /*
    Formato de RIF: X-00000000-N || X00000000N
    La expresion regular:

    ^             : Principio de texto.
    [JGVEPjgvep]  : La letra de la nacionalidad (X), mayuscula o minuscula.
    [-]?          : Puede o no haber un guion.
    [0-9]{7-9}    : Numeros, de 7 a 9 digitos.
    [0-9]{1}      : El digito de chequeo (N).
    $             : Fin de texto.
    */
    return (/^[JGVEPjgvep][-]?[0-9]{7,9}[-]?[0-9]{1}$/).test(rif);
}

function extraerDigitos(str) {
    // extraerDigitos('h01a mund0') -> '010'
    return (str.match(/\d/g) || []).join('');
}

function _parseAjaxSettings(args) {
    /*
    Busca los siguientes parametros y los ajusta para $.ajax

    ruta/route:  'modulo/',
    datos/data:  FormData,
    exito/success:  function(){},
    error:  function(){}
    tipo/type:  'GET' || 'POST' || 'PUT' || 'DELETE',
    */
    settings = {
        headers: {
            'token': userDOOMI.token,
            'tipo_usuario': userDOOMI.tipo_usu
        },
    };

    ruta = args.ruta || args.route;

    if (ruta) // Quitar '/' al principio en caso de tenerlo.
        ruta = ruta.slice(0,1)=='/' ? ruta.slice(1) : ruta;

    if(ruta) settings.url = dominio + ruta;
    if(args.tipo || args.type) settings.type = args.tipo || args.type;
    if(args.datos || args.data) {
        settings.data = args.datos || args.data;
        settings.cache = false;
        settings.dataType = 'json';
        settings.contentType = false;
        settings.processData = false;
    }

    if(args.success || args.exito) settings.success = (args.exito || args.success);
    if(args.error) settings.error = args.error;

    return settings;
}

function enviarForm(args) {
    /*
    ** NOTA: no usar funciones flecha para success y error,
    **       no mantienen el contexto del codigo.
    */

    settings = _parseAjaxSettings(args);

    settings.success = function exitoEnviandoForm(data) {
        $('.bt_save').prop('disabled', false); // Activar boton al finalizar.
        if(args.success)
            return args.success(data);
    }

    settings.error = function errorEnviandoForm(xhr, status, errorThrown) {
        $('.bt_save').prop('disabled', false); // Activar boton al finalizar.
        if (args.progressBar){
            $('.fileprogress').addClass('hide');
            $('.fileprogress').find('div')[0].style.width = '0%';
        }
        if(args.error)
            return args.error(xhr, status, errorThrown);
        else
            return console.log('Error al enviar form:', errorThrown);
    }

    if(args.progressBar){
        settings.xhr = function(){
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener('progress', function(e){
                if (e.lengthComputable) {
                  var percentComplete = parseInt( (e.loaded / e.total * 100), 10);
                  $('.fileprogress').find('div')[0].style.width = `${percentComplete}%`;
                  if(percentComplete>=100){
                    $('.fileprogress').addClass('hide');
                    $('.fileprogress').find('div')[0].style.width = '0%';
                  }
                }
            }, false);
            xhr.addEventListener('progress', function(e){
                if (e.lengthComputable) {
                    var percentComplete = parseInt( (e.loaded / e.total * 100), 10);
                    $('.fileprogress').find('div')[0].style.width = `${percentComplete}%`;
                    if(percentComplete>=100){
                        $('.fileprogress').addClass('hide');
                        $('.fileprogress').find('div')[0].style.width = '0%';
                    }
                }
            }, false);
            return xhr;
        }

        $('.fileprogress').removeClass('hide');
        $('.fileprogress').find('div')[0].style.width = '0%';
    }

    // Desactivar boton del form antes de enviar.
    $('.bt_save').prop('disable', true);

    return $.ajax(settings);
}


function _ajax(args) {
    if(!args) return;

    settings = _parseAjaxSettings(args);

    if (args.success) settings.success = args.success;

    if (args.error) settings.error = args.error;
    else
        settings.error = (xhr, status, errorThrown) => {
            console.log(`ajax${settings.type} ERROR:`, errorThrown);
        }

    return $.ajax(settings);
}
// Para las funciones success y error: EVITAR USAR FUNCIONES FLECHA.
function ajaxGET(ruta, success, error){ _ajax({tipo:'GET', ruta, success, error}) };
function ajaxDELETE(ruta, success, error){ _ajax({tipo:'DELETE', ruta, success, error}) };
function ajaxPOST(ruta, data, success, error){ _ajax({tipo:'POST', ruta, data, success, error}) };
function ajaxPUT(ruta, data, success, error){ _ajax({tipo:'PUT', ruta, data, success, error}) };


$('.materialboxed').materialbox();
//$('.Amount').maskNumber();
$(document).on('blur', '.Amount', function(){
    if(this.value.trim().length<=3){
        if(this.value.trim().length==3)
            this.value = formato.precio(this.value);
        }
});
//limite = debe ser enviado en tamaño kb
subirImg = function subirImg(input,div,limite) {
    if (input.files && input.files[0]) {
        if(limite){
            lim = parseFloat(limite)*1024;
            if(lim<input.files[0].size){
                Materialize.toast("La imagen seleccionada no debe ser mayor a "+limite+" Kbs", 4000);
                input.value="";
                return;
            }
        }
        var reader = new FileReader();
        reader.onload = function(e) {
            $(div).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

/*ultimas_notis = function ultimas_notis(){
    tipo_usu = userDOOMI.tipo_usuario;
    if(tipo_usu=='administrador')
        tipo_usu = 'administradores';
    
    $.ajax({
        url: dominio + "notificaciones/pendientes",
        type: "GET",
        data: {
            limit: 5,
            tipo_usuario: tipo_usu,
            id: userDOOMI.id
        },
        success: function(data){
            var html="";
            $(".notis_count").html(" "+data.count[0].count);
            for (var i = 0; i < data.noti.length; i++) {
                datos = data.noti[i];
                icono_noti = '';
                if(datos.tipo_noti == 1 || datos.tipo_noti == 2 || datos.tipo_noti == 3 || datos.tipo_noti == 7 || datos.tipo_noti == 8 || datos.tipo_noti == 9 || datos.tipo_noti == 10 || datos.tipo_noti == 13 || datos.tipo_noti == 14)
                    icono_noti = 'fa-user';
                else
                    icono_noti = 'fa-money-bill-alt';
                html += `<li class="boxNotificaciones">
                    <a href="#!" noti="${datos.id}" tipo="${datos.tipo_noti}" id_usable="${datos.id_usable}" class="clr_black p-1 vernotificacion">
                        <i class="fa ${icono_noti}"></i> ${datos.descripcion}<br> ${datos.contenido}
                    </a>
                        <span class="timeago text-right p-0">${moment(datos.fecha).toNow()}</span>
                    
                </li>`;
            }
            if(data.noti.length==0)
                html += '<li class="boxNotificaciones"><a href="#!" class="clr_black p-1 text-center"> Sin notificaciones pendientes</a></li>';
            
            $("#notifications-dropdown").append(html);
          
        },
        error: function(err) {
            console.log(err);
        }
    });
    
}
ultimas_notis();    */


