function listar_notificaciones(){
    var limit2 = 5;
    var CookieArray = [];
    
    obtener_mis_notis = function obtener_mis_notis() {
        var html = '';
        tipo_usu = userMEYERLAND.tipo_usuario;
        if(tipo_usu=='administrador')
            tipo_usu = 'administradores';
        else if(tipo_usu=='estudiante')
            tipo_usu = 'estudiantes';
        else if(tipo_usu=='profesor')
            tipo_usu = 'profesores';
        $.ajax({
            url: dominio + 'notificaciones/',
            type: 'GET',
            data: {
                tipo_usuario: tipo_usu,
                id: userMEYERLAND.id,
                limit: limit2
            },
            success: function(data) {
                $(".view_more").html('<a href="#!" onclick="$(this).html(\'Cargando...\'); obtener_mis_notis(); "><i class="fa fa-plus"></i> Cargar más</a>');

                if (data.length > 0 && data.length < 5 || data.length == 0)
                    $(".view_more").addClass("hide");
                if(data.length == 0)
                   $(".vertodasnotificacion").addClass("hide");

                for (i = data.length - 1; i >= 0; i--) {
                    CargarNoti(data[i]);
                }
                if(data.length<limit2)
                    $(".view_more").addClass("hide");

                if (limit2 == 5)
                    $(".misNotificaciones").scrollTop($('.misNotificaciones')[0].scrollHeight);
                
                if (data.length == 0)
                    $(".misNotificaciones").html('<div class="boxNotificaciones pb-2"> <p class="text-center"><a href="#" class="clr_black p-1">Sin notificaciones</a></p></div>');

                limit2 = (limit2 * 2);
            },
            error: function(err) {
                $(".view_more").addClass("hide");
                $(".misNotificaciones").html('<li class="boxNotificaciones"><a href="#!" class="clr_black p-1 text-center"> Sin notificaciones pendientes</a></li>');
            }
        })
    }
    if ( getop("op")=='notificaciones' )
        obtener_mis_notis();

    buscarID = function buscarID(id) {

        for (var i = 0; i < CookieArray.length; i++) {
            if (CookieArray[i].id == id)
                return true;
        }
        return false;
    }
    CargarNoti = function CargarNoti(datos) {
        //moment.locale("es");
        if (buscarID(datos.id)) {
            return false;
        }
        CookieArray.push(datos);
        var html = '';
        viewsts = 'no-vist';
        if (datos.estatus == 1)
            viewsts = '';
        icono_noti = '';
        if(datos.tipo_noti == 1 || datos.tipo_noti == 2 || datos.tipo_noti == 3 || datos.tipo_noti == 7 || datos.tipo_noti == 8 || datos.tipo_noti == 9 || datos.tipo_noti == 10 || datos.tipo_noti == 13 || datos.tipo_noti == 14)
            icono_noti = 'fa-user';
        else
            icono_noti = 'fa-money-bill-alt';
        html += `<div class="boxNotificaciones pb-4 ${viewsts}">
            <p>
                <a href="#" noti="${datos.id}" tipo="${datos.tipo_noti}" id_usable="${datos.id_usable}" class="clr_black p-1 vernotificacion">
                    <i class="fa ${icono_noti}"></i> ${datos.descripcion}<br><span class="pl-4">${datos.contenido}</span>
                </a>
            </p>
            <small class="timeago text-right p-0 pr-2 right">${moment(datos.fecha).toNow()}</small>
        </div>`;

        

        $(".misNotificaciones").prepend(html);
    }

    $(document).on('click', '.vernotificacion', function() {
        var tipo = $(this).attr("tipo");
        var usable = $(this).attr("id_usable");
        var id = $(this).attr("noti");
        

        $.ajax({
            url: dominio + 'notificaciones/vista/' + id,
            type: 'post',
            success: function(data) {
                
                if(tipo==1){
                    location.href = "?op=estudiantesretirados";
                }else if(tipo==2){
                    location.href = "?op=nuevogrupo&id="+usable;
                }else if(tipo==3 || tipo==2 || tipo==7 || tipo==10 || tipo==13){
                    location.href = "?op=ver_grupo&id="+usable;
                }else if(tipo==4 || tipo==5 || tipo==6 || tipo==12){
                    location.href = "?op=nuevafactura&id="+usable;
                }else if(tipo==8 || tipo==9 || tipo==11 || tipo==14){
                    location.href = "?op=vergrupoprofesor&id="+usable;
                }
                
            },
            error: function(err) {
                $(i).removeClass('');
            }
        })

    });

    $(document).on('click', '.vertodasnotificacion', function() {
        var id = userMEYERLAND.id;
        $.ajax({
            url: dominio + 'notificaciones/vistaall/' + id,
            type: 'GET',
            success: function(data) {
                if(data.r){
                    Materialize.toast("Todas las notificaciones se han marcado como vistas.", 4000);
                    $(".boxNotificaciones").removeClass("no-vist");
                }
            },
            error: function(err) {
                $(i).removeClass('');
            }
        })

    });
    

  


   




   
}; 