exports.up = knex => {
    return knex.schema.createTable("token_activacion_cliente", table => {
        table.increments("id").primary();
        table.integer("id_cliente").unsigned().references("id").inTable("clientes").onDelete('CASCADE');;
        table.timestamp("creado").defaultTo(knex.fn.now());
        table.string("token", 128).unique();
    });
};


exports.down = knex => {
    return knex.schema.dropTableIfExists("token_activacion_cliente")
};
