"use strict";

const { Modelo } = require("./Modelo");

class Metodos extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "metodos_de_pago";
    }

}
module.exports = Metodos;
