import { IMAGE_COMPRESSION_RATE } from './config'
var REGEXP_VALIDAR_CORREO = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var REGEXP_ES_HTTP_URL = /^https?:\/\//;
var REGEXP_ES_DATA_URL = /^data:/;
var REGEXP_DATA_URL_PROTOCOL = /^data:.*\/.*;base64,/;


//Get the picture
var processImageFile = function processImageFile(file, name, vue_context) {
  // let self = this;
  // Judging does not support FileReader.
  if (!file || !window.FileReader) return false;
  if (/^image/.test(file.type)) {
    // Create a reader.
    let reader = new FileReader();
    // Convert the image to base64 format.
    reader.readAsDataURL(file);
    // Read the callback after successful.
    reader.onloadend = function() {
      let result = this.result;
      let img = new Image();
      img.src = result;
      vue_context[name] = result;
      /*
      // img.src = URL.createObjectURL(new Blob([file], {type:'image/jpeg'}));
      console.log('******** Uncompressed image size **********');
      console.log(result.length / 1024);
      img.onload = function() {
        let data = compressImage(img, IMAGE_COMPRESSION_RATE);
        // self.uploadImg(data, name);
        vue_context[name] = data;
      }
      */
    }
  }
};

var compressImage = function compressImage(img, compressRate) {
  let canvas = document.createElement('canvas');
  let ctx = canvas.getContext('2d');
  let initSize = img.src.length;
  let width = img.width;
  let height = img.height;
  canvas.width = width;
  canvas.height = height;

  // bottom color
  ctx.fillStyle = '#fff';
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  ctx.drawImage(img, 0, 0, width, height);

  // Perform minimum compression
  let ndata = canvas.toDataURL('image/jpeg', compressRate);
  return ndata;
}


// var compressFileImage = function compressFileImage(file, compressionRate){
//   return new Promise((resolve, reject) => {

//     if (!window.FileReader) return reject('FileReader not found');
//     if (!file) return reject('compressFileImage expected a File');
//     if (!/^image/.test(file.type)) return reject('File is not an image');

//     const img = new Image();
//     const reader = new FileReader();
//     reader.readAsDataURL(file);
//     reader.onload = evt => img.src = evt.result;


//     const canvas = document.createElement('canvas');
//     const ctx = canvas.getContext('2d');
//     const initSize = img.src.length;
//     const width = img.width;
//     const height = img.height;
//     canvas.width = width;
//     canvas.height = height;

//     // bottom color
//     ctx.fillStyle = '#fff';
//     ctx.fillRect(0, 0, canvas.width, canvas.height);
//     ctx.drawImage(img, 0, 0, width, height);

//     console.log('compressionRate:', compressionRate);
//     // Perform minimum compression
//     // img.onload = () => canvas.toBlob(r=>resolve(r), compressionRate);

//     img.onload = () => {
//       console.log('compressFileImage.this:',this)
//     };
//     // return canvas.toBlob(r => resolve(r), compressionRate);
//   });
// };


var formatear = function(num, simbol) {
  simbol = simbol || '';
  let separador = "."; // separador para los miles
  let sepDecimal = ','; // separador para los decimales
  num += '';
  var splitStr = num.split('.');
  var splitLeft = splitStr[0];
  var splitRight = splitStr.length > 1 ? sepDecimal + splitStr[1] : '';
  var regx = /(\d+)(\d{3})/;
  while (regx.test(splitLeft)) {
      splitLeft = splitLeft.replace(regx, '$1' + separador + '$2');
  }
  return simbol + splitLeft + splitRight;
}

var dec2hex = function(dec) {
  /* Decimal a hexadecimal */
  return dec < 10
    ? '0' + String(dec)
    : dec.toString(16)
}

// Dias ordenados segun javascript.
var dias = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];


export var REGEXP_VALIDAR_CORREO = REGEXP_VALIDAR_CORREO;
// export var REGEXP_ES_HTTP_URL = REGEXP_ES_HTTP_URL;
// export var REGEXP_ES_DATA_URL = REGEXP_ES_DATA_URL;
// export var REGEXP_DATA_URL_PROTOCOL = REGEXP_DATA_URL_PROTOCOL;
export var processImageFile = processImageFile;
export var compressImage = compressImage;
export var formatear = formatear;
export var dec2hex = dec2hex;
export var dias = dias;
// export var compressFileImage = compressFileImage;
