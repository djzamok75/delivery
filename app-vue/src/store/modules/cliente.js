import { general, auth } from '../../api'

export default {
  namespaced: true,


  state: () => ({
    idDoomisFavoritos: [],
    ordenes: [],
    carrito: [],
  }),


  getters: {
    favoritos: state => state.idDoomisFavoritos,
    esFavorito: (state) => (id) => state.idDoomisFavoritos.includes(+id),
    ordenes: state => state.ordenes,
    carrito: state => state.carrito,

    doomisFavoritos: (state, getters, rootState, rootGetters) => {
      let d = rootGetters['doomis/doomialiados'].filter(doomi => state.idDoomisFavoritos.includes(doomi.id));
      return d;
    },
  },


  mutations: {
    AGREGAR_FAVORITO: (state, id) => state.idDoomisFavoritos.push(id),
    QUITAR_FAVORITO: (state, id) => state.idDoomisFavoritos = state.idDoomisFavoritos.filter(idFav => idFav != id),

    SET_FAVORITO: (state, args) => {
      const id = args[0], status = args[1];
      if(status && !state.idDoomisFavoritos.includes(id))
        state.idDoomisFavoritos.push(id);
      else if (!status)
        state.idDoomisFavoritos = state.idDoomisFavoritos.filter(idFav => idFav != id);
    },
  },


  actions: {
    favorito: ({ commit }, args) => {
      const idDoomi = args[0],
        status = args[1];

      const userData = auth.obtenerDatos();
      const peticion = status ? general.put : general.eliminarruta;

      peticion(`/clientes/${userData.id}/doomis_favoritos/${idDoomi}`)
        .then(r => commit('SET_FAVORITO', [idDoomi, status]));
    },

    actualizarFavoritos: ({ commit }) => {
      if(!auth.autenticado()) return;

      const userData = auth.obtenerDatos();

      general.recuperarData(`/clientes/${userData.id}/doomis_favoritos`)
        .then(function(data){
              for (let idDoomi of data.map(e => e.id_restaurante))
                commit('SET_FAVORITO', [idDoomi, true]);
        });
    },

  },
};
