
const config = Object.freeze({
	dominio:'http://localhost/',
	apiURL:'http://localhost:1210/',
	headers: {
		'Access-Control-Allow-Origin': '*',
		// 'Access-Control-Allow-Methods': '*',
		'Access-Control-Allow-Headers': '*',
		'tipo-cliente': 'app',
		},

	STORAGE_KEY_AUTH: 'authDOOMI',
	STORAGE_KEY_DATA: 'userDOOMI',
	IMAGE_COMPRESSION_RATE: 1,


	imagenes: {
		userIcon: 'https://www.kindpng.com/picc/m/269-2697881_computer-icons-user-clip-art-transparent-png-icon.png',
		autoDefault: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSJXxXVxwp6qA1ny9LW-LT8duEREoqeRkUbew&usqp=CAU',
	},
});

export default config;

export const apiURL = config.apiURL;
export const loginURL = config.loginURL;
export const headers = config.headers;
export const dominio = config.dominio;
export const STORAGE_KEY = config.STORAGE_KEY;
export const STORAGE_KEY_AUTH = config.STORAGE_KEY_AUTH;
export const STORAGE_KEY_DATA = config.STORAGE_KEY_DATA;
export const IMAGE_COMPRESSION_RATE = config.IMAGE_COMPRESSION_RATE;
export const imagenes = config.imagenes;

