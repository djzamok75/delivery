exports.up = knex => {
	return knex.schema.createTable("menus_adicionales", table => {
		table.increments("id").primary();
		table.string("nombre", 100);
		table.float("precio_usd").unsigned();
		table.integer("id_menu").unsigned().references('id').inTable('menus');
	});
};

exports.down = knex => {
	return knex.schema.dropTableIfExists("menus_adicionales");
};
