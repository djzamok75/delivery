exports.up = knex => {
    return knex.schema.createTable("horarios", table => {
        table.increments("id").primary();
        table.integer("id_restaurante").unsigned().references('id').inTable('restaurantes');
    	table.string("dia", 100);
    	table.time("inicio_hor");
    	table.time("fin_hor");
    	table.boolean("estatus").defaultTo(false);
    	table.string("horario", 2);
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("horarios");
};
