"use strict";

const { Modelo } = require("./Modelo");
const MenuAdicionales = require("./MenuAdicionales");
const CategoriaMenu = require("./CategoriaMenu");


class Menu extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "menus";
    }

    $afterGet(queryContext) {
        return this.ag();
    }

    async adicionales() {
        let adis = await MenuAdicionales.query().where("id_menu", this.id);
        this.adicionales = adis[0] ? adis : [];
    }

    async categoria() {
        let cat = await CategoriaMenu.query().where("id", this.id_categoria);
        this.categoria = cat[0] ? cat[0] : {};
    }

    async ag() {
        await this.adicionales();
        await this.categoria();
    }

}
module.exports = Menu;
