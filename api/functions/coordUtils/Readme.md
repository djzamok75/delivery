# coordUtils
Funciónes para trabajar con coordenadas.

NOTA: Un punto se define de las siguientes maneras:
```javascript
var punto = {lon: 0, lat: 0}
var punto = {longitud: 0, latitud: 0}
```


### Funciones

**distanciaEntreCoordenadas**
Argumentos: (punto1, punto2)
Acepta dos puntos como argumento, devuelve la distancia entre ellos en kilómetros.

```javascript
var distancia = distanciaEntreCoordenadas(punto1, punto2);
```


**puntoEnRadio**
Argumentos: (punto, centro, radio)
Devuelve verdadero si el punto dado se encuentra dentro del radio desde el centro.

```javascript
if (puntoEnRadio(punto1, punto2, 5))
// Si el punto 1 esta en un rango de 5km del punto 2
```
