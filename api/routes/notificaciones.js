const Notificaciones = require("../models/Notificacion");

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const moment = require("moment");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
var archivos = require("../Archivos");
const config = require("../config");
var uuid = require("uuid");
var sesion = require("./sesion");
var sesi = new sesion();

const mailer = require("../mails/Mailer");
const mailp = require("../mails/correo");
const validarMail = require("../functions/validateMail");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

//Manejador de Errores
const { AuthError, ValidationError } = require("../Errores");

var router = express.Router();
 

//Obtener usuarios
router.get('/',upload.none(),async (req, res, next) => {
    var data = req.query;
    //console.log(data);
    if(data.limit!=null && data.limit!="")
            var limit = data.limit;
        else 
            var limit ="5";

    var string = " ORDER BY ID DESC LIMIT "+limit+" ";
    
    if(data.limit=="todas")
        string = "";
    var notificaciones = await Notificaciones.raw('select * from notificacion where id_receptor='+data.id+' and tabla_usuario="'+data.tipo_usuario+'" '+string+';') 
  AR.enviarDatos(notificaciones[0], res);
});

router.get('/pendientes',upload.none(),async (req, res, next) => {
    var data = req.query;
    if(data.limit!=null && data.limit!="")
        var limit = data.limit;
    else 
        var limit ="5";

    var string = " ORDER BY ID DESC LIMIT "+limit+" ";
    
    if(data.limit=="todas")
        string = "";
    var notificaciones = await Notificaciones.raw('select * from notificacion where id_receptor='+data.id+' and tabla_usuario="'+data.tipo_usuario+'" and estatus=0 '+string+';') 
    
    var notificaciones2 = await Notificaciones.raw('select count(*) as count from notificacion where id_receptor='+data.id+' and tabla_usuario="'+data.tipo_usuario+'" and  estatus=0;')
    
    AR.enviarDatos({noti : notificaciones[0], count: notificaciones2[0]}, res);
});


router.post('/vista/:id',upload.none(),async (req, res, next) => {
    var datos = {
        estatus:1
    }
    await Notificaciones.query()
    .updateAndFetchById(req.params.id, datos)
    .then(notificacion => {
    notificacion.msj = "Se editó correctamente";
    AR.enviarDatos(notificacion, res);
    })
    .catch(err => {
    console.log(err);
        res.send(err);
    });
});

router.get('/vistaall/:id',upload.none(),async (req, res, next) => {
    await Notificaciones.query()
    .update({ estatus:1})
    .where("id_receptor",req.params.id)
    .where("tabla_usuario","administradores")
    .catch(err => {
    console.log(err);
        res.send(err);
    });
    datos = {};
    datos.r = true;
    AR.enviarDatos(datos, res);
});

router.post('/count',upload.none(),async (req, res, next) => {
    data = req.body;
    var notificaciones = await Notificaciones.raw('select count(*) as count from notificacion where id_receptor='+data.id+' and tabla_usuario="'+data.tipo_usuario+'" and  estatus=0;')
    
    AR.enviarDatos(notificaciones[0], res);
});




module.exports = router;
