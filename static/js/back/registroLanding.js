$(document).ready(function() { 
        data = {
        }
        $.ajax({
            url: dominio + 'normativas/',
            type: 'GET',
            data: data,
            success: function(data) {
                
                $("#normativas").html(data.normativas);
                 
            }, error: function(xhr, status, errorThrown) {
                console.log(errorThrown);
            }

        });
		$('.checkbutton').click(function(){
			
			$('#modalTerminos').modal();
			$('#modalTerminos').modal('open');
			$('.modal-overlay').remove();

			
		})
		
		$('.IconCerrarModal').click(function(){
			
			$('#modalTerminos').modal('close');
			
		});
        function imprSelec(elemento){
              var ventana = window.open('', 'PRINT', 'height=' + document.body.clientHeight + ',width=' + document.body.clientWidth + '');
              ventana.document.write('<html><head><title>' + document.title + '</title>');
              ventana.document.write('<link rel="stylesheet" href="static/css/style.css">');
              ventana.document.write('</head><body >');
              ventana.document.write(elemento.innerHTML);
              ventana.document.write('</body></html>');
              ventana.document.close();
              ventana.focus();
              ventana.onload = function() {
                        ventana.print();
                        ventana.close();
              };
              return true;
        }
        $('.BTPrint').click(function(){
            imprSelec(document.getElementById('Printnormativas'));
        });
		$('.BTterminos').click(function(){
			
			termChecked();
			
		});
		function termChecked() {
			document.getElementById("termino").checked = true;
			$('#modalTerminos').modal('close');

			//document.getElementById("modal-close-reg").click();
			//var f = document.createElement('style');
			//f.innerHTML = ".lean-overlay {display: none !important;}";
			//document.body.appendChild(f);
		}
			$("#id_servicio").select2({
	            placeholder: "Nivel de curso",
	            allowClear: true
	        });
			function listadser() {
                data = {
                     tipo_ser: "publico"
                }
                $.ajax({
                    url: dominio + 'servicios/publicos',
                    type: 'GET',
                    data: data,
                    success: function(data) {
                        html = '<option value="">Nivel de curso</option>';
                  
                         for (var i = 0; i < data.length; i++) {
                             html += '<option value="' + data[i].id + '">' + data[i].nom_ser + '</option>';
                         }
                         
                         $("#id_servicio").html(html);
                         $("#id_servicio").select2({
				            placeholder: "Nivel de curso",
				            allowClear: true
				         });
                    }

                })
            }
            listadser();
    $("#formLanding").on('submit', function(e) {
            e.preventDefault();
            if($("#nombre").val().trim() == "" || $("#nombre").val().trim().length <=2){
                Materialize.toast("Ingrese un nombre, no debe estar vacío y debe tener mínimo 3 caráceres.", 5000);
                return;
            }
            if($("#apellido").val().trim() == "" || $("#apellido").val().trim().length <=2){
                Materialize.toast("Ingrese un apellido, no debe estar vacío y debe tener mínimo 3 caráceres.", 5000);
                return;
            }
            if($("#correo").val().trim() == "" || $("#correo").val().trim().length <=2){
                Materialize.toast("Ingrese un correo válido.", 5000);
                return;
            }
            if(!$("#id_servicio").val()){
                Materialize.toast("Seleccione un nivel.", 5000);
                return;
            }
            if($("#cedula").val().trim() == "" || $("#cedula").val().trim().length <=6){
                Materialize.toast("Ingrese una identificación válida.", 5000);
                return;
            }
            if($("#pass").val().trim() == "" || $("#pass").val().trim().length <=4){
                Materialize.toast("Ingrese una contraseña válida, minímo 5 caráceres.", 5000);
                return;
            }
            if($("#pass2").val().trim() == "" || $("#pass2").val().trim().length <=4){
                Materialize.toast("Ingrese una contraseña(repetir) válida, minímo 5 caráceres.", 5000);
                return;
            }
            if($("#pass").val().trim() != $("#pass2").val().trim()){
                Materialize.toast("La contraseñas no coinciden.", 5000);
                return;
            }
            $(".button_censo").attr("disabled",true);
            var data = new FormData(this);
            
            $.ajax({
                url: dominio + "estudiantes/landing",
                type: "POST",
                dataType: "json",
                cache: false,
                data: data,
                contentType: false,
                processData: false,
                success: function(res) {
                    Materialize.toast(res.msj, 4000);
                    if(res.r){
                            localStorage.setItem('userMEYERLAND',JSON.stringify(res));
                            //localStorage.setItem('IDSERVICIO', $("#id_servicio").val());
                            location.href = 'panel/vistas/index.html?op=servicios_estudiantes&postularse=yes&servicio=' + $("#id_servicio").val() + '&id='+res.id;
                            
                    } else {
                        $(".button_censo").attr("disabled",false);
                    }
                },
                error: function(xhr, status, errorThrown) {
                    console.log(errorThrown);
                    Materialize.toast(errorThrown.msj, 4000);
                    $(".button_censo").attr("disabled",false);
                }
            });

    });

    //al tipear campo cedula
    $("#cedula").keyup(function(){
        
        Buscar_estudiante_ya_Registrado()
        
    })

    function Buscar_estudiante_ya_Registrado() {

        var cedula = $("#cedula").val();
        var identifi = $("#identificacion").val();

        //validaciones
        var errr = false;
        var msj = false;

        if (identifi == "" || identifi == undefined) {
            errr = true; msj = "Selecciona un tipo de identificación";
        }
        if (cedula == "" || cedula == undefined) {
            errr = true; msj = "Escribe un número de cédula o identificación";
        }

        if (errr) {
            Materialize.toast(msj, 2000); return false;
        }
        //fin validaciones

        data = {
            cedula: cedula,
            identificacion: identifi
        }
        $.ajax({
            url: dominio + 'estudiantes/cedula_identificacion',
            type: 'GET',
            data: data,
            success: function(data) {
                 //console.log(data);
                 if (data.length > 0) {

                    $("#formLanding input").attr('disabled', true)
                    $("#formLanding select").attr('disabled', true)
                    $(".button_censo").attr("disabled", true);
                    Materialize.toast("Ya te encuentras registrado en la plataforma, inicia sesión o dirigete a nuestras oficinas para activar tu usuario de acceso", 10000); return false;

                 }else{

                    $("#formLanding input").attr('disabled', false)
                    $("#formLanding select").attr('disabled', false)
                    $(".button_censo").attr("disabled", false);

                 }
            }, error: function(xhr, status, errorThrown) {
                console.log(errorThrown);
            }

        })
    }

    //habilitar campos de landing censo
    $(".modal-close").click(function(){
            $("#formLanding input").attr('disabled', false)
            $("#formLanding input").val('')
            $("#formLanding select").attr('disabled', false)
            $("#formLanding select").val('')
            $(".button_censo").attr("disabled", false);
    })

});