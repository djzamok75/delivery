function listar_credenciales(){

   
    //listar 
    credenciales();

    function credenciales() {
        data = {  
        }
        $.ajax({
            url: dominio + 'administradores/',
            headers: {
                'token':userDOOMI.token
            },
            type: 'GET',
            data: data,
            success: function(data) {
                //console.log(data);
                actualizar_tabla_credenciales(data)
            }
        })
    }

    function actualizar_tabla_credenciales(data) {

        var table = $('.table').DataTable({
            "language": {
                "url": "../static/<lib></lib>/JTables/Spanish.json"
            }
        });
        count = 0
        table.clear().draw();

        for (var i = 0; i < data.length; i++) {
            datos = data[i];

            var options = `
                <a href="#!" id="${datos.id}" class="btn btn-primary btn-floating ver-info tooltipped" data-position="bottom" data-delay="50" data-tooltip="Ver información del administrador"><i class="fa fa-eye"></i></a>
                
                <a href="#!" id="${datos.id}" class="btn btn-primary btn-floating edit-info tooltipped" data-position="bottom" data-delay="50" data-tooltip="Editar información del administrador"><i class="fa fa-edit"></i></a>       

                <a href="#!" id="${datos.id}" class="waves-effect btn btn-primary btn-floating tooltipped delete" data-position="bottom" data-delay="50" data-tooltip="Eliminar administrador"><i class="fa fa-times"></i></a>
            `;

            var row = [count += 1, datos.nombre, datos.apellido, datos.usuario, datos.correo, options];
            table.row.add(row).draw().node();
        }
        $('.tooltipped').tooltip();
    }

    $("#form_registrar_admins").on('submit', function(e) {

        
        
        e.preventDefault();

        /*validaciones*/
        var errr = false;
        var msj = false;

        if ( $("#nombre").val().trim() == "" ){ errr = true; msj = "Ingresa un nombre";}

        else if ( $("#apellido").val().trim() == "" ){ errr = true; msj = "Ingresa un apellido";}
        
        else if ( $("#identificacion").val() == "" ){ errr = true; msj = "Selecciona un tipo de identificación";}

        else  if ( $("#cedula").val().trim() == "" ){ errr = true; msj = "Ingresa cédula";}

        else if ( $("#cedula").val().length < 7 || $("#cedula").val().length > 8 ){ errr = true; msj = "Campo cédula: mínimo 7 carácteres y máximo 8";}
        
        else if ( $("#correo").val().trim() == "" ){ errr = true; msj = "Ingresa correo";}

        else if ( $("#pass").val().trim().length < 5 && ($("#pass_2").val().trim() != '' || $("#iden").val() == '')){ errr = true; msj = "Ingresa una contraseña";}
        
        else if ( $("#pass_2").val().trim().length < 5 && ($("#pass").val().trim() != '' || $("#iden").val() == '')){ errr = true; msj = "Ingresa contraseña en: Confirmar contraseña";}
        
        else if ( $("#pass_2").val().trim() != $("#pass").val().trim() ){ errr = true; msj = "Los dos campos de contraseñas no coinciden";}

        
        /*fin validaciones*/
        if (errr) {
            Materialize.toast(msj, 4000); return false;
        }
        //data a enviar
        var data = new FormData(this);
        
        if($("#iden").val() != ''){
            url = "administradores/edit";
            tipo = "PUT";
        }else{
            url = "administradores/add";
            tipo = "POST";
        }
        $(".bt_save").prop("disabled", true);
        $(".fileprogress").removeClass("hide");
        $(".fileprogress").find("div")[0].style.width = "0%";
        $.ajax({
            url: dominio + url,
            headers: {
                'token':userDOOMI.token
            },
            type: tipo,
            dataType: "json",
            cache: false,
            data: data,
            contentType: false,
            processData: false,
            xhr: function(){
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(e){
                    if (e.lengthComputable) {
                      var percentComplete = parseInt( (e.loaded / e.total * 100), 10);
                      $(".fileprogress").find("div")[0].style.width = `${percentComplete}%`;
                      if(percentComplete>=100){
                        $(".fileprogress").addClass("hide");
                        $(".fileprogress").find("div")[0].style.width = "0%";
                      }
                    }
                }, false);
                xhr.addEventListener("progress", function(e){
                    if (e.lengthComputable) {
                        var percentComplete = parseInt( (e.loaded / e.total * 100), 10);
                        $(".fileprogress").find("div")[0].style.width = `${percentComplete}%`;
                        if(percentComplete>=100){
                            $(".fileprogress").addClass("hide");
                            $(".fileprogress").find("div")[0].style.width = "0%";
                        }
                    }
                }, false);
                return xhr;
            },
            success: function(res) {
                Materialize.toast(res.msj, 4000)
                if (res.r == true) {
                    $("#form_registrar_admins")[0].reset();
                    credenciales();
                    $("#md-nuevoAdmin").modal('close');
                }
                //botones del form
                $(".bt_save").prop("disabled", false)
            },
            error: function(xhr, status, errorThrown) {
                console.log(errorThrown);
                $(".fileprogress").addClass("hide");
                $(".fileprogress").find("div")[0].style.width = "0%";
                Materialize.toast(errorThrown.msj, 4000)

                //botones del form
                $(".bt_save").prop("disabled", false)
            }
        });
    });
    $(document).on('click', '.ver-info', function() {
        var idv = this.id;
        buscar_data(idv,'ver')
    });
    $(document).on('click', '.edit-info', function() {
        var idv = this.id;
        buscar_data(idv,'editar')
    });
    function buscar_data(idb,modal){
        data = {
        }
        $.ajax({
            url: dominio + 'administradores/'+idb,
            headers: {
                'token':userDOOMI.token
            },
            type: 'GET',
            data: data,
            success: function(data) {

                 //console.log(data);
                imprimir_data_credencial(data,modal)
                 
            }, error: function(xhr, status, errorThrown) {
                console.log(errorThrown);
            }

        })
    }
    function imprimir_data_credencial(data,modal){
        if(modal=='ver'){
            $(".nom-view").html(data.nombre);
            $(".ape-view").html(data.apellido);
            $(".iden-view").html(data.identificacion+"-"+data.cedula);
            $(".cor-view").html(data.correo);
            $(".usu-view").html(data.usuario);
            $("#md-verAdmin").modal("open");
        }else if(modal=='editar'){
            $("#nombre").val(data.nombre);
            $("#apellido").val(data.apellido);
            $("#cedula").val(data.cedula);
            $("#correo").val(data.correo);
            $("#identificacion").val(data.identificacion).attr("change");
            $("#usuario").val(data.usuario);
            $("#iden").val(data.id);
            $("#pass , #pass_2").val('');
            $("#tit-mod").html("Editar administrador");
            $(".bt_save").html('Guardar cambios');
            $("#md-nuevoAdmin").modal("open");
        }
    }

    $(document).on('click', '.delete', function() {
        var idv = this.id;
        var toastContent = '<span>¿Desea eliminar éste registro?</span><br><button class="btn-flat toast-action conf_si" id="'+idv+'">Si</button><button class="btn-flat toast-action" onclick=" $(\'.toast\').hide(); ">No</button>';

        Materialize.toast(toastContent, 4000);
    });

    $(document).on('click', '.conf_si', function() {
        id = this.id
        var data = {};

        $('.toast').hide();
        $.ajax({
            url: dominio + 'administradores/delete/'+id,
            headers: {
                'token':userDOOMI.token
            },
            type: 'DELETE',
            data: data,
            success: function(data) {
                if (data.msj) {
                    Materialize.toast(data.msj, 10000);
                    credenciales();
                } else {
                    Materialize.toast(data.msj, 10000);
                }
            }
        })

    });

    $(document).on('click', '.sorting_1', function() {
        $('.tooltipped').tooltip();
    })

}