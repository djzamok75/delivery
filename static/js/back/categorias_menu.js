function listar_categorias_menu() {

	// Listar.
	categorias();

	function categorias() {
		ajaxGET('categorias_menu/?id_res='+userDOOMI.restaurante[0].id, actualizar_tabla_categorias);
	}

	function actualizar_tabla_categorias(data) {
		var table = $('.table').DataTable({
			"language": {
				"url": "../static/<lib></lib>/JTables/Spanish.json"
			}
		});
		count = 0;
		table.clear().draw();

		for (var i = 0; i < data.length; i++) {
			datos = data[i];

			var options = `
				<a href="#!" id="${datos.id}" class="btn btn-primary btn-floating ver-info tooltipped" data-position="bottom" data-delay="50" data-tooltip="Ver información de la categoría"><i class="fa fa-eye"></i></a>
				<a href="#!" id="${datos.id}" class="btn btn-primary btn-floating edit-info tooltipped" data-position="bottom" data-delay="50" data-tooltip="Editar información de la categoría"><i class="fa fa-edit"></i></a>
				<a href="#!" id="${datos.id}" class="waves-effect btn btn-primary btn-floating tooltipped delete" data-position="bottom" data-delay="50" data-tooltip="Eliminar categoría"><i class="fa fa-times"></i></a>
			`;

			var row = [count+=1, datos.nombre, options];
			table.row.add(row).draw().node();
		}
		$('.tooltipped').tooltip();
	}

	$("#form_registrar_categoria").on("submit", function(e){
		e.preventDefault();

		// Validaciones.
		var errr = false;
		var msj = false;

		if ($("#nombre").val().trim() == ""){
			errr=true;
			msj = "Ingresa el nombre de la categoría";
		}
		// Fin de validaciones.

		if (errr) {
			Materialize.toast(msj, 4000);
			return false;
		}
		// Data a enviar.
		var data = new FormData(this);
		data.append("id_res", userDOOMI.restaurante[0].id);
		if($("#iden").val() != ''){
			url = "categorias_menu/edit";
			tipo = "PUT";
		}
		else {
			url = "categorias_menu/add";
			tipo = "POST";
		}

		enviarForm({
			ruta: url,
			tipo: tipo,
			data: data,
			progressBar: true,
			success: function(res){
				Materialize.toast(res.msj, 4000)
				if (res.r == true){
					$("#form_registrar_categoria")[0].reset();
					categorias();
					$("#md-nuevaCategoria").modal('close');
				}
			},
			error: function(xhr, status, errorThrown){
				console.log(errorThrown);
				Materialize.toast(errorThrown.msj, 4000)
			}
		});
	});
	$(document).on("click", ".ver-info", function(){
		var idv = this.id;
		buscar_data(idv, "ver");
	});
	$(document).on("click", ".edit-info", function(){
		var idv = this.id;
		buscar_data(idv, "editar")
	});
	function buscar_data(idb, modal){
		ajaxGET(`categorias_menu/${idb}`
			,function(data){imprimir_data_categoria(data, modal)}
			,function(xhr, status, errorThrown){console.log(errorThrown)}
		);
	};
	function imprimir_data_categoria(data, modal) {
		if (modal == "ver"){
			$(".nom-view").html(data.nombre);
			$(".desc-view").html(data.descripcion);
			$("#md-verCategoria").modal("open");
		}
		else if (modal == "editar") {
			$("#nombre").val(data.nombre);
			$("#descripcion").val(data.descripcion);
			$("#iden").val(data.id);
			$("#tit-mod").html("Editar categoría");
			$(".bt_save").html("Guardar cambios");
			$("#md-nuevaCategoria").modal("open");
		}
	}
	$(document).on('click', '.delete', function() {
		var idv = this.id;
		var toastContent = '<span>¿Desea eliminar esta categoría?</span><br><button class="btn-flat toast-action conf_si" id="'+idv+'">Si</button><button class="btn-flat toast-action" onclick=" $(\'.toast\').hide(); ">No</button>';

		Materialize.toast(toastContent, 4000);
	});
	$(document).on('click', '.conf_si', function() {
		id = this.id
		$('.toast').hide();

		ajaxDELETE(`categorias_menu/delete/${id}`, function(data) {
				if (data.msj) {
					Materialize.toast(data.msj, 10000);
					categorias();
				} else {
					Materialize.toast(data.msj, 10000);
				}
			});
	});

	$(document).on('click', '.sorting_1', function() {
		$('.tooltipped').tooltip();
	})
};
