// llamar al modelo.
const Restaurante = require("../models/Restaurante");
// const Categoria = require("../models/Categoria");
const Subcategoria = require("../models/Subcategoria");
const RestauranteSubcategorias = require("../models/RestauranteSubcategorias")

// Otras librerias.
const AR = require("../ApiResponser");
var sesion = require("./sesion");
var sesi = new sesion();

// Obligatorio para trabajar con FormData.
const multer = require("multer");
const upload = multer();

const express = require("express");
var router = express.Router();

// Listar subcategorias.
router.get("/", sesi.validar_admin, async(req, res)=>{
    await Subcategoria.query()
        .then(subcategorias=>{AR.enviarDatos(subcategorias, res)})
        .catch(err=>{console.log(err)});
});

// Obtener una subcategoria.
router.get("/:id", sesi.validar_admin, async(req, res)=>{
    await Subcategoria.query().where("id", req.params.id)
        .then(sub=>{AR.enviarDatos(sub[0], res)})
        .catch(err=>{console.log(err)});
});

// Agregar subcategoria.
router.post("/add", upload.none(), sesi.validar_admin, async(req, res) => {
    data = req.body;

    // Validar datos.
    errr = false;
    if (!data.nombre) errr = "Ingrese el nombre de la categoría";
    if (!data.categoria) errr = "Seleccione una categoría";

    const verificar_n = await Subcategoria.query()
        .where({nombre: data.nombre, id_categoria: data.categoria});
    if (verificar_n[0]) errr = "La subcategoría ya existe";

    if (errr)
        return AR.enviarDatos({r: false, msj: errr}, res);
    // Fin validacion.


    await Subcategoria.query()
        .insert({
            nombre: data.nombre,
            id_categoria: data.categoria
        })
        .then(async resp => {
            // await Categoria.query()
            //     .where("id", data.categoria).increment("veces_usada", 1);

            resp.msj = "Subcategoría agregada";
            resp.r = true;

            AR.enviarDatos(resp, res);
        })
        .catch(err => {
            console.log(err);
            err.r = false;
            err.msj = "Error al crear subcategoría";
            AR.enviarDatos(err, res)
        });
});

// Editar subcategoria.
router.put("/edit", upload.none(), sesi.validar_admin, async(req, res)=>{
    data = req.body;

    errr = false;
    if (!data.iden) errr = "Error: No se especificó el id";
    if (!data.nombre) errr = "No se especificó el nombre";
    if (!data.categoria) errr = "Seleccione una categoría";

    if(errr)
        return AR.enviarDatos({r:false, msj:errr}, res);

    // const info = await Subcategoria.query().where({id: data.iden}); // Obtener antes de actualizar.

    // if (info[0].id_categoria != data.categoria)
    //     await Categoria.query().where("id", info[0].id_subcategoria).decrement("veces_usada", 1);
    //     await Categoria.query().where("id", data.subcategoria).increment("veces_usada", 1);

    await Subcategoria.query()
        .updateAndFetchById(data.iden, {nombre:data.nombre, id_categoria:data.categoria})
        .then(resp=>{
            resp.msj = "Datos actualizados con éxito";
            resp.r = true;
            AR.enviarDatos(resp, res);
        })
        .catch(err => {
            console.log(err);
            res.send(err);
        });
});

// Eliminar subcategoria.
router.delete("/delete/:id", sesi.validar_admin, async(req, res)=>{

    // const subcategoria = await Subcategoria.query().where("id", req.params.id);
    const subcategorias = await RestauranteSubcategorias.query().where({id_subcategoria: req.params.id})
        .catch(err => console.log(err));

    if (subcategorias[0]){
        return AR.enviarDatos({r: false, msj: "Ésta subcategoría está siendo utilizada, no se puede eliminar"}, res)
    }

    await Subcategoria.query().delete().where({id:req.params.id})
        .then(async(r)=>{
            console.log(r);
            // await Categoria.query().where({id:req.params.id}).decrement("veces_usada", 1);

            AR.enviarDatos({r: true, msj: "Subcategoria eliminada exitosamente"}, res);
        })
        .catch(err => {
            console.log(err);
            res.send(err);
        });
});

module.exports = router;
