"use strict";

const { Modelo } = require("./Modelo");

class ciudad extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "ciudad";
    }

}

module.exports = ciudad;
