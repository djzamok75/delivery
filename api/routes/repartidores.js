//llamar al modelo
const Repartidor = require("../models/Repartidor");


//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const moment = require("moment");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../config");
var uuid = require("uuid");
var sesion = require("./sesion");
var sesi = new sesion();
var archivos = require("../Archivos");
var fs = require("fs-extra");

const mailer = require("../mails/Mailer");
const mailp = require("../mails/correo");
const validarMail = require("../functions/validateMail");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();
const crypto = require("crypto");

//Manejador de Errores
const { AuthError, ValidationError } = require("../Errores");

var router = express.Router();

// Obtener un nuevo ID de repartidor.
router.get("/nuevo_id", upload.none(), /*sesi.validar_admin, */async(req, res) => {
	await Repartidor.query().orderBy("id_repartidor", "desc")
		.then(repartidores=>{
			let nuevoID = 1;
			if (repartidores[0])
				nuevoID = repartidores[0].id_repartidor + 1;
			AR.enviarDatos({r:true, nuevoID:nuevoID}, res);
		})
		.catch(err=>{console.log(err)});

});

//registrar un Repartidor
router.post("/add", archivos.fields([{ name: "img", maxCount: 1 },{ name: "img1", maxCount: 1 },{ name: "img2", maxCount: 1 },{ name: "img3", maxCount: 1 },{ name: "img_rif", maxCount: 1 }]), /*sesi.validar_admin,*/ async (req, res, next) => {
    data = req.body;
    let errr = false;

    // inicio validaciones
    if (!data.identificacion) errr = "Selecciona un tipo de identificación";
    if (!data.cedula) errr = "Ingresa un número de identificación";
    if (!data.nombre) errr = "Ingresa un nombre";
    if (!data.apellido) errr = "Ingresa un apellido";
    if (!data.tipo_veh) errr = "Selecciona el tipo de vehiculo";
    if (!data.correo) errr = "Ingresa un correo";
    if (!data.sexo) errr = "Selecciona el sexo";
    if (!data.telefono) errr = "Ingresa el teléfono";
    if (!data.whatsapp) errr = "Ingresa el whatsapp";
    if (!data.usuario) errr = "Ingresa un usuario";
    if (!data.pass) errr = "Ingresa una contraseña";
    // if (!data.id_nuevo_repartidor) errr = "Error interno: no se especificó id_repartidor";
    if (req.files && !req.files["img1"] && !req.files["img2"] && !req.files["img3"] && !data.imgweb) errr = "Selecciona una foto del vehículo";
    if (!data.img1 && !data.img2 && !data.img3 && data.imgweb) errr = "Selecciona una foto del vehículo";

    if(data.cedula){
      const verificar_c = await Repartidor.query().where({ cedula: data.cedula });
      if (verificar_c[0]) errr = "El número de cédula ya existe";
    }
    if(data.usuario){
      const verificar_u = await Repartidor.query().where({ usuario: data.usuario });
      if (verificar_u[0]) errr = "El usuario ya existe";
    }
    if(data.correo){
      const verificar_e = await Repartidor.query().where({ correo: data.correo });
      if (verificar_e[0]) errr = "El correo ya existe";
    }
    if(data.telefono){
      const verificar_t = await Repartidor.query().where({ telefono: data.telefono });
      if (verificar_t[0]) errr = "El teléfono ya existe";
    }
    if(data.whatsapp){
      const verificar_w = await Repartidor.query().where({ whatsapp: data.whatsapp });
      if (verificar_w[0]) errr = "El whatsapp ya existe";
    }
    //fin validaciones
    if(errr){
      //ELIMINAR IMAGENES SI NO PASA LAS VALIDACIONES
      if(!data.imgweb){ //saber si se cargan las imagenes desde movil
        if(req.files && req.files["img"])
          await fs.unlink(__dirname + "/../public/uploads/" + req.files["img"][0].filename, function (err) { });
        if(req.files && req.files["img1"])
          await fs.unlink(__dirname + "/../public/uploads/" + req.files["img1"][0].filename, function (err) { });
        if(req.files && req.files["img2"])
          await fs.unlink(__dirname + "/../public/uploads/" + req.files["img2"][0].filename, function (err) { });
        if(req.files && req.files["img3"])
          await fs.unlink(__dirname + "/../public/uploads/" + req.files["img3"][0].filename, function (err) { });
        if(req.files && req.files["img_rif"])
          await fs.unlink(__dirname + "/../public/uploads/" + req.files["img_rif"][0].filename, function (err) { });
      }
      return AR.enviarDatos({r: false, msj: errr}, res);
    }

    let ultimaID = await Repartidor.query().orderBy("id_repartidor", "desc")
      .catch(err=>console.log(err));

    let tipo_cliente = req.headers['tipo-cliente'];

    var datos = {
      nombre: data.nombre,
      apellido: data.apellido,
      identificacion: data.identificacion,
      usuario: data.usuario,
      cedula: data.cedula,
      correo: data.correo,
      pass: bcrypt.hashSync(data.pass, 8),
      id_tipo_vehiculo: data.tipo_veh,
      sexo: data.sexo,
      telefono: data.telefono,
      whatsapp: data.whatsapp,
      domicilio: data.domicilio,
      id_repartidor: ultimaID[0] ? ultimaID[0].id_repartidor+1 : 1,
      estatus: tipo_cliente === 'app' ? false : true,
    };
    //guardar ruta imagen
    if(!data.imgweb){
      if(req.files && req.files["img"])
        datos.img = config.rutaArchivo(req.files["img"][0].filename);
      if(req.files && req.files["img1"])
        datos.foto_1 = config.rutaArchivo(req.files["img1"][0].filename);
      if(req.files && req.files["img2"])
        datos.foto_2 = config.rutaArchivo(req.files["img2"][0].filename);
      if(req.files && req.files["img3"])
        datos.foto_3 = config.rutaArchivo(req.files["img3"][0].filename);
      if(req.files && req.files["img_rif"])
        datos.img_rif = config.rutaArchivo(req.files["img_rif"][0].filename);
    }else if(data.imgweb){
      if(data.img){
        var file = "img_per__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.img.split(",")[1], 'base64'), 'base64', function(err) {
          //console.log(err);
        });
        datos.img = config.rutaArchivo(file);
      }
      if(data.img1){
        var file1 = "fot_veh__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file1, new Buffer.from(data.img1.split(",")[1], 'base64'), 'base64', function(err) {
          // console.log(err);
        });
        datos.foto_1 = config.rutaArchivo(file1);
      }
      if(data.img2){
        var file2 = "fot_veh__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file2, new Buffer.from(data.img2.split(",")[1], 'base64'), 'base64', function(err) {
          //console.log(err);
        });
        datos.foto_2 = config.rutaArchivo(file2);
      }
      if(data.img3){
        var file3 = "fot_veh__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file3, new Buffer.from(data.img3.split(",")[1], 'base64'), 'base64', function(err) {
          // console.log(err);
        });
        datos.foto_3 = config.rutaArchivo(file3);
      }
      if(data.img4){
        var file4 = "fot_rif__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file4, new Buffer.from(data.img4.split(",")[1], 'base64'), 'base64', function(err) {
          // console.log(err);
        });
        datos.img_rif = config.rutaArchivo(file4);
      }
    }

    await Repartidor.query()
    .insert(datos)
    .then(async resp => {
      resp.msj = "Repartidor registrado";
      resp.r = true;
      
      /*var cuerpo = `Hola, ${data.nombre} ${data.apellido}
        <br> Ahora formas parte
        de la Repartidoristración de nuestra plataforma. Tus datos de acceso a
        la plataforma de DOOMI son: <br>
        <b>Usuario: </b> ${data.iden}${data.cedula} <br><b>Contraseña: </b> ${data.pass}
      `;
      mailer.enviarMail({
          direccion: data.correo,
          titulo: "BIENVENIDO A DOOMI.app",
          mensaje: mailp({
              cuerpo: cuerpo,
              titulo: "BIENVENIDO A DOOMI.app"
          })
      });
      await Notificaciones.query()
      .insert({
          descripcion: "Bienvenido a DOOMI.app",
          contenido: "Bienvenido <b>"+data.nombre+" "+data.apellido+"</b> formas parte de nuestra Repartidoristración en MEYERLANDONLINE.COM",
          estatus: 0,
          id_receptor: resp.id,
          tabla_usuario: 'Repartidoristradores',
          tipo_noti: 15,
          id_usable: resp.id,
          fecha: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
      }).catch(err => {console.log(err)});
      */
      AR.enviarDatos(resp, res);
    })
    .catch(async err => {
      //ELIMINAR IMAGENES POR SI EXISTE ALGUN ERROR
      if(!req.body.imgweb){
        if(req.files && req.files["img"])
          await fs.unlink(__dirname + "/../public/uploads/" + req.files["img"][0].filename, function (err) { });
        if(req.files && req.files["img1"])
          await fs.unlink(__dirname + "/../public/uploads/" + req.files["img1"][0].filename, function (err) { });
        if(req.files && req.files["img2"])
          await fs.unlink(__dirname + "/../public/uploads/" + req.files["img2"][0].filename, function (err) { });
        if(req.files && req.files["img3"])
          await fs.unlink(__dirname + "/../public/uploads/" + req.files["img3"][0].filename, function (err) { });
        if(req.files && req.files["img_rif"])
          await fs.unlink(__dirname + "/../public/uploads/" + req.files["img_rif"][0].filename, function (err) { });
      }
      console.log(err)
      err.r = false;
      err.msj = 'Error al registrar Repartidor';
      res.send(err);
    });

});

//editar Repartidor
router.put("/edit", archivos.fields([{ name: "img", maxCount: 1 },{ name: "img1", maxCount: 1 },{ name: "img2", maxCount: 1 },{ name: "img3", maxCount: 1 },{ name: "img_rif", maxCount: 1 }]), sesi.validar_general, async (req, res, next) => {
  data = req.body;
  let errr = false;

  // inicio validaciones
  if (!data.identificacion) errr = "Selecciona un tipo de identificación";
  if (!data.cedula) errr = "Ingresa un número de identificación";
  if (!data.nombre) errr = "Ingresa un nombre";
  if (!data.apellido) errr = "Ingresa un apellido";
  if (!data.tipo_veh) errr = "Selecciona el tipo de vehiculo";
  if (!data.correo) errr = "Ingresa un correo";
  if (!data.sexo) errr = "Selecciona el sexo";
  if (!data.telefono) errr = "Ingresa el teléfono";
  if (!data.whatsapp) errr = "Ingresa el whatsapp";
  if (!data.usuario) errr = "Ingresa un usuario";
  if (!data.iden) errr = "Ingresa la id";

  if(data.cedula && data.iden){
    const verificar_c = await Repartidor.query().whereNot('id', data.iden).where({ cedula: data.cedula });
    if (verificar_c[0]) errr = "El número de cédula ya existe";
  }
  if(data.usuario && data.iden){
    const verificar_u = await Repartidor.query().whereNot('id', data.iden).where({ usuario: data.usuario });
    if (verificar_u[0]) errr = "El usuario ya existe";
  }
  if(data.correo && data.iden){
    const verificar_e = await Repartidor.query().whereNot('id', data.iden).where({ correo: data.correo });
    if (verificar_e[0]) errr = "El correo ya existe";
  }
  if(data.telefono && data.iden){
    const verificar_t = await Repartidor.query().whereNot('id', data.iden).where({ telefono: data.telefono });
    if (verificar_t[0]) errr = "El teléfono ya existe";
  }
  if(data.whatsapp && data.iden){
    const verificar_w = await Repartidor.query().whereNot('id', data.iden).where({ whatsapp: data.whatsapp });
    if (verificar_w[0]) errr = "El whatsapp ya existe";
  }

  //fin validaciones
  if(errr){
    //ELIMINAR IMAGENES SI NO PASA LAS VALIDACIONES
    if(!data.imgweb){
      if(req.files["img"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img"][0].filename, function (err) { });
      if(req.files["img1"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img1"][0].filename, function (err) { });
      if(req.files["img2"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img2"][0].filename, function (err) { });
      if(req.files["img3"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img3"][0].filename, function (err) { });
      if(req.files["img_rif"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img_rif"][0].filename, function (err) { });
    }
    return AR.enviarDatos({r: false, msj: errr}, res);
  }

  var datos = {
    nombre: data.nombre,
    apellido: data.apellido,
    identificacion: data.identificacion,
    usuario: data.usuario,
    cedula: data.cedula,
    correo: data.correo,
    id_tipo_vehiculo: data.tipo_veh,
    sexo: data.sexo,
    telefono: data.telefono,
    whatsapp: data.whatsapp,
    domicilio: data.domicilio,
  }
  if (data.pass && data.pass.length > 0) {
    datos.pass = bcrypt.hashSync(data.pass, 8);
  }
  //guardar ruta imagen
  if(!data.imgweb){
    if(req.files["img"])
      datos.img = config.rutaArchivo(req.files["img"][0].filename);
    if(req.files["img1"])
      datos.foto_1 = config.rutaArchivo(req.files["img1"][0].filename);
    if(req.files["img2"])
      datos.foto_2 = config.rutaArchivo(req.files["img2"][0].filename);
    if(req.files["img3"])
      datos.foto_3 = config.rutaArchivo(req.files["img3"][0].filename); 
    if(req.files["img_rif"])
      datos.img_rif = config.rutaArchivo(req.files["img_rif"][0].filename); 
  }else if(data.imgweb){
      if(data.img){
        var file = "img_per__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.img.split(",")[1], 'base64'), 'base64', function(err) {
          //console.log(err);
        });
        datos.img = config.rutaArchivo(file);
      }
      if(data.img1){
        var file1 = "fot_veh__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file1, new Buffer.from(data.img1.split(",")[1], 'base64'), 'base64', function(err) {
          // console.log(err);
        });
        datos.foto_1 = config.rutaArchivo(file1);
      }
      if(data.img2){
        var file2 = "fot_veh__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file2, new Buffer.from(data.img2.split(",")[1], 'base64'), 'base64', function(err) {
          //console.log(err);
        });
        datos.foto_2 = config.rutaArchivo(file2);
      }
      if(data.img3){
        var file3 = "fot_veh__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file3, new Buffer.from(data.img3.split(",")[1], 'base64'), 'base64', function(err) {
          // console.log(err);
        });
        datos.foto_3 = config.rutaArchivo(file3);
      }
      if(data.img_rif){
        var file4 = "fot_rif__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file4, new Buffer.from(data.img4.split(",")[1], 'base64'), 'base64', function(err) {
          // console.log(err);
        });
        datos.img_rif = config.rutaArchivo(file4);
      }
    }
  var info = await Repartidor.query().where({ id: data.iden });//OBTENER DATA ANTES DE ACTUALIZAR
  await Repartidor.query().updateAndFetchById(data.iden, datos)
  .then(async resp => {
    //ELIMINAR IMAGENES ANTERIOR SI HA GUARDADO UNA NUEVA
    if (!req.body.imgweb){
      if((req.files["img"] || data.img) && info[0].img)
        await fs.unlink(__dirname + "/../public/uploads/" + info[0].img.split(`\\public\\uploads\\`)[1], function (err) { });
      if((req.files["img1"] || data.img1) && info[0].foto_1)
        await fs.unlink(__dirname + "/../public/uploads/" + info[0].foto_1.split(`\\public\\uploads\\`)[1], function (err) { });
      if((req.files["img2"] || data.img2) && info[0].foto_2)
        await fs.unlink(__dirname + "/../public/uploads/" + info[0].foto_2.split(`\\public\\uploads\\`)[1], function (err) { });
      if((req.files["img3"] || data.img3) && info[0].foto_3)
        await fs.unlink(__dirname + "/../public/uploads/" + info[0].foto_3.split(`\\public\\uploads\\`)[1], function (err) { });
      if((req.files["img_rif"] || data.img4) && info[0].img_rif)
        await fs.unlink(__dirname + "/../public/uploads/" + info[0].img_rif.split(`\\public\\uploads\\`)[1], function (err) { });
    }
    resp.msj = "Datos actualizados con éxito";
    resp.r = true;
    AR.enviarDatos(resp, res);
  })
  .catch(async err => {
    if(!req.body.imgweb){
      if(req.files["img"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img"][0].filename, function (err) { });
      if(req.files["img1"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img1"][0].filename, function (err) { });
      if(req.files["img2"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img2"][0].filename, function (err) { });
      if(req.files["img3"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img3"][0].filename, function (err) { });
      if(req.files["img_rif"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img_rif"][0].filename, function (err) { });
    }
    console.log(err)
    err.r = false;
    err.msj = 'Error al actualizar datos';
    res.send(err);
  });

});

//editar Estatus
router.put("/edit_sts", upload.none(), sesi.validar_admin, async (req, res, next) => {
  data = req.body;
  let errr = false;

  // inicio validaciones
  if (!data.sts) errr = "Ingresa el estatus";
  if (!data.iden) errr = "Ingresa la id del Repartidor";
  
  //fin validaciones
  if(errr){
    return AR.enviarDatos({r: false, msj: errr}, res);
  }

  var datos = {
    estatus: data.sts
  }
  

  await Repartidor.query().updateAndFetchById(data.iden, datos)
  .then(async resp => {
    resp.msj = "Estatus actualizado con éxito";
    resp.r = true;
    AR.enviarDatos(resp, res);
  })
  .catch(async err => {
    console.log(err)
    err.r = false;
    err.msj = 'Error al actualizar estatus';
    res.send(err);
  });

});

// Aprobar o rechazar.
router.patch("/aprobacion", upload.none(), sesi.validar_admin, async(req, res) => {
  let id = req.body.id || req.body.iden;
  let aprobado = req.body.aprobado || req.body.aprobacion;

  // Verificar.
  if(!id || !aprobado)
    return res.status(400).send({r:false, msj:'Datos incompletos'});

  aprobado = (aprobado==1 || aprobado=="true") ? 1 : 0;

  await Repartidor.query()
    .findById(id)
    .patch({aprobado, estatus: aprobado ? 1 : undefined})
    .catch(err => console.log(err));

  return AR.enviarDatos({r:true, msj:'Aprobacion exitosa'}, res);

});

//listar todos los Repartidor
router.get("/", sesi.validar_general, async(req, res) => {
  await Repartidor.query()
      .then(ret => {
      AR.enviarDatos(ret, res);
  });
});

//ver un Repartidor
router.get("/:id", sesi.validar_general, async(req, res) => {
  await Repartidor.query().where("id",req.params.id).then(ret => {
      AR.enviarDatos(ret[0], res);
  });

});

//eliminar Repartidor
/*router.delete("/delete/:id", upload.none(), sesi.validar_admin, async(req, res) => {
  data = req.query;
  var info = await Repartidor.query().where({ id: req.params.id })
  const eliminado = await Repartidor.query().delete().where({ id: req.params.id })
    .catch(err => {
      console.log(err);
      res.send(err);
    });

    if (eliminado){
      if(info[0]){
        if(info[0].img)
          await fs.unlink(__dirname + "/../public/uploads/" + info[0].img.split(`\\public\\uploads\\`)[1], function (err) { });
        if(info[0].foto_1)
          await fs.unlink(__dirname + "/../public/uploads/" + info[0].foto_1.split(`\\public\\uploads\\`)[1], function (err) { });
        if(info[0].foto_2)
          await fs.unlink(__dirname + "/../public/uploads/" + info[0].foto_2.split(`\\public\\uploads\\`)[1], function (err) { });
        if(info[0].foto_3)
          await fs.unlink(__dirname + "/../public/uploads/" + info[0].foto_3.split(`\\public\\uploads\\`)[1], function (err) { });
      }
      AR.enviarDatos({ msj: "Registro eliminado exitosamente" }, res);
    }
    else{ AR.enviarDatos({ msj: "No se puede eliminar el Repartidor" }, res);}
});*/

module.exports = router;
