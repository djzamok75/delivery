"use strict";

const { Modelo } = require("./Modelo");
const Metodos_en_cuentas = require("../models/Metodos_en_cuentas");

class Cuentas extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "cuentas";
    }
    $afterGet(queryContext) {
        return this.ag();
    }

    async metodos() {
            var met = await Metodos_en_cuentas.query()
                .join('metodos_de_pago', 'metodos_de_pago.id', 'metodos_en_cuentas.id_metodo')
                .where("metodos_en_cuentas.id_cuenta", this.id)
                .select("metodos_en_cuentas.*","metodos_de_pago.nombre")
                .catch(err => { console.log(err); });
            this.metodos = met;
    }

    async ag() {
        await this.metodos();
    }
}
module.exports = Cuentas;
