// llamar al modelo.
const Categoria = require("../models/Categoria");
const Subcategoria = require("../models/Subcategoria");
const RestauranteSubcategorias = require("../models/RestauranteSubcategorias");

// Otras librerias.
const AR = require("../ApiResponser");
var sesion = require("./sesion");
var sesi = new sesion();

// Obligatorio para trabajar con FormData.
const multer = require("multer");
const upload = multer();

const express = require("express");
const router = express.Router();


//----CATEGORIAS----//

// Listar categorias.
router.get("/", upload.none(), async(req, res)=>{
    const con_subs = req.query.con_subcategorias;
    let query;


    if (con_subs) // Obtener solo las que tengan subcategorias.
        query = Categoria.raw("SELECT DISTINCT c.* FROM categorias c INNER JOIN subcategorias s ON s.id_categoria=c.id")
    else
        query = Categoria.query();

    await query
        .then(categorias => {
            AR.enviarDatos(con_subs ? categorias[0] : categorias,  res)
        })
        .catch(err => console.log(err));
});

// Ver categoria.
router.get("/:id",  async(req, res)=>{
    await Categoria.query().where("id", req.params.id)
        .then(cat=>{AR.enviarDatos(cat[0], res)})
        .catch(err=>{console.log(err)});
});

// Agregar categoria.
router.post("/add", upload.none(), sesi.validar_general, async(req, res) => {
    data = req.body;

    // Validar datos.
    errr = false;
    if (!data.nombre) {errr = "Ingrese el nombre de la categoría"}
    else {
        const verificar_n = await Categoria.query().where("nombre", data.nombre);
        if (verificar_n[0]) errr = "La categoría ya existe";
    }

    if (errr)
        return AR.enviarDatos({r: false, msj: errr}, res);
    // Fin validacion.

    await Categoria.query().insert({nombre:data.nombre})
        .then((resp) => {
            resp.r = true;
            resp.msj = "Categoría agregada";
            AR.enviarDatos(resp, res);
        })
        .catch(err => {
            console.log(err);
            AR.send({r:false, msj:"Error al crear categoria"}, res);
        });
});

// Editar categoria.
router.put("/edit", upload.none(), sesi.validar_general, async(req, res)=>{
    data = req.body;

    errr = false;
    if (!data.iden) errr = "Error interno: No se especificó el id";
    if (!data.nombre) errr = "No se especificó el nombre";

    if(errr)
        return AR.enviarDatos({r:false, msj:errr}, res);

    await Categoria.query().updateAndFetchById(data.iden, {nombre:data.nombre})
        .then(async function(resp){
            resp.msj = "Datos actualizados con éxito";
            resp.r = true;
            AR.enviarDatos(resp, res);
        })
        .catch(err => {
            console.log(err);
            AR.enviarDatos({r: false, msj:"Error al actualizar datos"}, res);
        });
});

// Eliminar categoria.
router.delete("/delete/:id", sesi.validar_general, async(req, res)=>{

    // const categorias = await Categoria.query().where("id", req.params.id);
    const categorias = await RestauranteSubcategorias.query().where({id_categoria: req.params.id})
        .catch(err => console.log(err));

    if (categorias[0]){
        return AR.enviarDatos({r: false, msj: "Ésta categoría está siendo utilizada, no se puede eliminar"}, res)
    }

    const eliminado = await Categoria.query().delete().where({id:req.params.id})
        .catch(err => {
            console.log(err);
            res.send(err);
        });

    if (eliminado){
        await Subcategoria.query().delete().where({id_categoria: req.params.id});
        AR.enviarDatos({r: true, msj: "Categoría eliminada exitosamente"}, res);
    }
    else{
        AR.enviarDatos({msj: "No se pudo eliminar la categoria"}, res);
    }
});


// Listar subcategorias dentro de categoria ID.
router.get("/:id/subcategorias", async(req, res)=>{
    const subcategorias = await Subcategoria.query()
        .where({id_categoria:req.params.id})
        .catch(err => {res.send(err)});
    AR.enviarDatos(subcategorias, res);
});


module.exports = router;
