//llamar al modelo
const Firebase = require("../models/Firebase");
const express = require("express");
const config = require("../knexfile");


var FCM = require('fcm-node');
var serverKey = config.production.firebase_key_server;
var fcm = new FCM(serverKey);

var htmlToText = require('html-to-text');
function html_a_text(html) {
    var text = htmlToText.fromString(html, {
      format: {
        heading: function (elem, fn, options) {
          var h = fn(elem.children, options);
          return '====\n' + h.toUpperCase() + '\n====';
        }
      }
    });

    return text;
}

var firebase = function(){


    this.enviarPushNotification = async function(data, retorno){
      
                var sql = "SELECT * FROM firebases WHERE id_usuario='"+data.id_usuario+"' and tablausuario='"+data.tablausuario+"';";
                await Firebase.raw(sql
                ).then(Usuarios => {
                    
                    if(Usuarios[0].length == 0)
                            retorno({r: false, msj: 'No envió la notificación'});
                    else{
                        data.title = html_a_text(data.title);
                        data.body = html_a_text(data.body);
                        var resul = Usuarios[0];
                        var arreglo = [];
                        for(i = 0 ; i < resul.length; i++){
                            var fila = resul[i];
                            arreglo[i] = fila.token;
                        }
                        var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera) 
                            registration_ids: arreglo, 

                            notification: {
                                title: data.title, 
                                body: data.body,
                                sound : "default",
                                badge: "1"
                            },
                            data: data,
                            priority: 'high'
                        };

                        fcm.send(message, function(err, response){
                            if (err) {
                                console.log("Err Fcm: " + err);
                            } else {
                                console.log("Envio push bien");
                            }
                        });
                        retorno({r: true});
                    }
                })
                .catch(err => {
                     retorno({r: true});
                });
    }
}

module.exports = firebase;
