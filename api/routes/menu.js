// llamar al modelo.
const Menu = require("../models/Menu");
const CategoriaMenu = require("../models/CategoriaMenu");
const MenuAdicionales = require("../models/MenuAdicionales");

// Otras librerias.
const AR = require("../ApiResponser");
const config = require("../config");
var sesion = require("./sesion");
var sesi = new sesion();
var archivos = require("../Archivos");
var fs = require("fs-extra");

// Obligatorio para trabajar con FormData.
const multer = require("multer");
const upload = multer();
const crypto = require("crypto");

const express = require("express");
var router = express.Router();


// Registrar un menu.
router.post("/add", archivos.fields([{ name: "img", maxCount: 1 }]), sesi.validar_restaurante, async function(req, res, next){
    data = req.body;

    // Validar los datos.
    let errr = false;
    if (!data.nom_men) errr = "Ingrese el nombre del menú";
    if (!data.descripcion) errr = "Ingrese la descripción del menú";
    if (!data.usd_men) errr = "Ingrese el precio del menú";
    if (!data.cat_men) errr ="Seleccione la categoría del menú";
    if (!data.est_men) errr = "Seleccione el estatus del menú"
    if (!data.id_res) errr = "Ingrese id del restaurante";
    if (!data.exi_men) errr = "Ingrese las existencias";

    const verificar_n = await Menu.query().where({nombre: data.nom_men, id_restaurante:data.id_res});
    if (verificar_n[0]) errr = "El menú ya existe";

    if (errr) {
        if(!data.imgweb){ 
            if(req.files["img"])
                await fs.unlink(__dirname + "/../public/uploads/" + req.files["img"][0].filename, function (err) { });
        }
        return AR.enviarDatos({r: false, msj: errr}, res);
    }
    // Fin de validacion.

    const adicionales = {
        nombres: data.adicionales_nombres,
        precios: data.adicionales_precios
    };
    if(data.adicionales_nombres)
        elementos_adicionales = data.adicionales_nombres.length;
    else
        elementos_adicionales = 0;

    datos = { nombre:    data.nom_men
        ,descripcion:    data.descripcion
        ,precio_usd:     data.usd_men
        ,estatus:        data.est_men
        ,id_categoria:   data.cat_men
        ,id_restaurante: data.id_res
        ,existencias:    data.exi_men
    };

    // Guardar la ruta a la imagen.
    if(!data.imgweb){ 
        if(req.files["img"])
            datos.img = config.rutaArchivo(req.files["img"][0].filename);
    }else if(data.imgweb){
      if(data.img){
        var file = "img_per__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.img.split(",")[1], 'base64'), 'base64', function(err) {
          //console.log(err);
        });
        datos.img = config.rutaArchivo(file);
      }
    }

    await Menu.query()
        .insert(datos)
        .then(async resp => {
            resp.msj = "Menú agregado";
            resp.r = true;

            // Evitar que se elimine categoria
            //  si hay menus que pertenecen a ella.
            await CategoriaMenu.query()
                .where("id", datos.id_categoria).increment("veces_usada", 1);

            for (let i = 0; i < elementos_adicionales; i++) {
                let adi_nombre = adicionales.nombres[i];
                let adi_precio = adicionales.precios[i];

                await MenuAdicionales.query()
                    .insert({nombre: adi_nombre, precio_usd: adi_precio, id_menu: resp.id})
                    .catch(err => console.log(err));
            }

            AR.enviarDatos(resp, res);
        })
        .catch(async err => {
            //ELIMINAR IMAGENES POR SI EXISTE ALGUN ERROR
            if(!req.body.imgweb){
                if(req.files["imagen"])
                    await fs.unlink(__dirname + "/../public/uploads/" + req.files["imagen"][0].filename, function (err) { });
            }
            console.log(err);
            err.r = false;
            err.msj = "Error al crear menú";
            res.send(err);
        });
});


// Editar menu.
router.put("/edit", archivos.fields([{ name: "img", maxCount: 1 }]), sesi.validar_restaurante, async (req,res,next)=>{
    data = req.body;

    // Validar los datos.
    let errr = false;
    if (!data.nom_men) errr = "Ingrese el nombre del menú";
    if (!data.descripcion) errr = "Ingrese la descripción del menú";
    if (!data.usd_men) errr = "Ingrese el precio del menú";
    if (!data.cat_men) errr ="Seleccione la categoría del menú";
    if (!data.est_men) errr = "Seleccione el estatus del menú"
    if (!data.id_res) errr = "Ingrese id del restaurante";
    if (!data.exi_men) errr = "Ingrese las existencias";

    // Fin de validacion.

    if (errr){
        //ELIMINAR IMAGENES SI NO PASA LAS VALIDACIONES.
        if(!data.imgweb){ 
            if(req.files["img"])
                await fs.unlink(__dirname + "/../public/uploads/" + req.files["img"][0].filename, function (err) { });
        }
        return AR.enviarDatos({r: false, msj: errr}, res);
    }

    // Actualizar datos.
    let datos = {nombre:   data.nom_men
            ,descripcion:  data.descripcion
            ,precio_usd:   data.usd_men
            ,estatus:      data.est_men
            ,id_categoria: data.cat_men
            ,existencias:    data.exi_men
    };

    const adicionales = {
        nombres: data.adicionales_nombres,
        precios: data.adicionales_precios,
        ids: data.adicionales_ids
    };
    if(data.adicionales_ids)
        elementos_adicionales = data.adicionales_ids.length;
    else
        elementos_adicionales = 0;



    // Guardar ruta de la imagen.
    if(!data.imgweb){ 
        if(req.files["img"])
            datos.img = config.rutaArchivo(req.files["img"][0].filename);
    }else if(data.imgweb){
      if(data.img){
        var file = "img_per__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.img.split(",")[1], 'base64'), 'base64', function(err) {
          //console.log(err);
        });
        datos.img = config.rutaArchivo(file);
      }
    }
    var info = await Menu.query().where({ id: data.iden });
    await Menu.query().updateAndFetchById(data.iden, datos)
        .then(async function(resp){
            //ELIMINAR IMAGENES ANTERIOR SI HA GUARDADO UNA NUEVA.
            if((req.files["img"] || data.img) && + info[0].img)
                await fs.unlink(__dirname + "/../public/uploads/" + info[0].img.split(`\\public\\uploads\\`)[1], function (err) { });
            resp.msj = "Datos actualizados con éxito";
            resp.r = true;

            for (let i = 0; i < elementos_adicionales; i++) {
                let adi_nombre = adicionales.nombres[i];
                let adi_precio = adicionales.precios[i];
                let adi_id = adicionales.ids[i];

                if (adi_id == 0){
                    await MenuAdicionales.query()
                        .insert({nombre: adi_nombre, precio_usd: adi_precio, id_menu: resp.id})
                        .catch(err => console.log(err));
                }
                else {
                    await MenuAdicionales.query().updateAndFetchById(adi_id, {nombre: adi_nombre, precio_usd: adi_precio, id_menu: resp.id})
                        .catch(err => console.log(err));
                }

            }

            AR.enviarDatos(resp, res);
        })
        .catch(async (err) => {
            if(!req.body.imgweb){
                if(req.files["img"])
                    await fs.unlink(__dirname + "/../public/uploads/" + req.files["img"][0].filename, function (err) { });
            }
            console.log(err);
            err.r = false;
            err.msj = "Error al actualizar datos";
            res.send(err);
        });

});


// Cambiar estatus de un menu.
router.put("/edit_estatus", upload.none(), sesi.validar_restaurante, async(req, res)=>{
    data = req.body;

    await Menu.query().updateAndFetchById(data.iden, {estatus: data.nuevo_estatus})
        .then((resp)=>{
            if (resp) {
                resp.estatus_actual = data.nuevo_estatus;
                resp.msj = "Estatus actualizado con éxito";
                resp.r = true;
                AR.enviarDatos(resp, res);
            }
            else {
                res.send(null);
            }
        })
        .catch(async(err)=>{
            console.log(err);
            err.estatus_actual = (await Menu.query().where("id", data.iden)).estatus
            err.msj = "No se pudo actualizar el estatus";
            err.r = false;
            res.send(err);
        });
});


// Listar todos los menus.
router.get("/", async(req, res) => {
    data = req.query;
    query = Menu.query();

    if (data.id_res)
        query.where("id_restaurante", data.id_res);

    await query.then(menus => AR.enviarDatos(menus, res))
        .catch(err => {
            console.log(err);
            AR.enviarDatos({r:false}, res);
        });

    // await Menu.query().where("id_restaurante", data.id_res)
    //     .then(async menus => {

    //         // Incluir los nombres de categoria y estatus segun su id.
    //         for (var i = 0; i < menus.length; i++) {
    //             categoria = await CategoriaMenu.query().where("id", menus[i].id_categoria);
    //             menus[i].categoria = categoria[0].nombre;
    //         }
    //         AR.enviarDatos(menus, res);
    //     });



});



// Ver un menu.
router.get("/:id", sesi.validar_restaurante, async(req, res) =>{
    await Menu.query().where("id", req.params.id)
        .then(async menus => {
            categoria = await CategoriaMenu.query().where("id", menus[0].id_categoria);
            menus[0].categoria = categoria[0].nombre;
            AR.enviarDatos(menus[0], res);
        });
});


// Listar menus por categoria.
router.get("/categoria/:id", sesi.validar_general, async(req, res) => {
    await Menu.query().where("id_categoria", req.params.id)
        .then(data => { data.r=true; AR.enviarDatos(data, res) })
        .catch(err => AR.enviarDatos({r:false, msj:"Error al recuperar categoria"}, res));
});


// Eliminar menu.
router.delete("/delete/:id", upload.none(), sesi.validar_restaurante, async(req,res)=>{
    data = req.body;

    // Decrementar las veces que se usa la categoria del menu.
    await Menu.query().where("id", req.params.id)
        .then(async menus => {
            await CategoriaMenu.query().where("id", menus[0].id_categoria).decrement("veces_usada", 1);
        });
    info = await Menu.query().where({ id: req.params.id });
    const eliminado = await Menu.query().delete().where({id:req.params.id})
        .catch(err => {
            console.log(err);
            res.send(err);
        });

    if (eliminado)
    {
        if(info[0])
        {
            MenuAdicionales.query().delete().where("id_menu", info[0].id).catch(err=>console.log(err));
            if(info[0].img){
                await fs.unlink(
                    __dirname + "/../public/uploads/" + info[0].img.split(`\\public\\uploads\\`)[1],
                    function (err){}
                );
            }
        }
        AR.enviarDatos({msj: "Menú eliminado exitosamente"}, res);
    }
    else {
        AR.enviarDatos({msj: "No se pudo eliminar el menú"}, res);
    }
});

// Eliminar adicional.
router.delete("/adicionales/:id", upload.none(), sesi.validar_restaurante, async(req,res)=>{
    let eliminado = await MenuAdicionales.query().delete().where("id", req.params.id)
        .catch(err=>console.log(err));

    if(eliminado)
        return AR.enviarDatos({r:true, msj:"Registro eliminado exitosamente"}, res);

    return AR.enviarDatos({r:false, msj:"Error al eliminar registro"}, res);
});

module.exports = router;