exports.up = knex => {
	return knex.schema.createTable("categorias_menu", table => {
		table.increments("id").primary();
		table.string("nombre", 100);
		table.string("descripcion");
		table.integer("veces_usada").unsigned().defaultTo(0);
		table.integer("id_restaurante").unsigned().references('id').inTable('restaurantes');
	});
};

exports.down = knex => {
	return knex.schema.dropTableIfExists("categorias_menu");
};