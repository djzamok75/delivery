"use strict";

const { Modelo } = require("./Modelo");


class Cliente extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "clientes";
    }
}
module.exports = Cliente;
