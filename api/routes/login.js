//llamar al modelo
const Admin = require("../models/Admin");
const Usuario_info = require("../models/Usuario_info");
const Usuario = require("../models/Usuario");
const Cliente = require("../models/Cliente");
const Repartidor = require("../models/Repartidor");

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../config");
var uuid = require("uuid");
var sesion = require("./sesion");
var sesi = new sesion();


const mailer = require("../mails/Mailer");
const mailp = require("../mails/correo");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();

//Manejador de Errores
const { AuthError, ValidationError } = require("../Errores");

var router = express.Router();


//hacer login - mediante web.
router.post("/", upload.none(), async (req, res, next) => {
  data = req.body;
  clavecorrecta = false;
  dataall = [];

  let errr = false;
  // inicio validaciones
  if (!data.correo) errr = "Ingresa el correo";
  if (!data.pass) errr = "Ingresa una contraseña";
  //fin validaciones
  if(errr) return AR.enviarDatos({r: false, msj: errr}, res);


  admin = await Admin.query().where({'correo':data.correo}).catch(async err => {console.log("error admin: "+err)})

  if(admin[0]){
    admin[0].token  = uuid.v1();
    admin[0].r=true;
    admin[0].tipo_usu  = 'administrador';
    admin[0].text_tipo_usu  = 'Administrador';
    dataall.push(admin[0]);

    let passwordIsValid = bcrypt.compareSync(data.pass, admin[0].pass);
    if (passwordIsValid == true) {
      clavecorrecta = true;
    }
  }

  user = await Usuario_info.query().where({'correo':data.correo}).catch(async err => {console.log("error usuario: "+err)})

  if(user[0]){
    user[0].token  = uuid.v1();
    user[0].r=true;
    user[0].tipo_usu  = 'restaurante';
    user[0].text_tipo_usu  = 'Doomialiado';
    dataall.push(user[0]);

    let passwordIsValid = bcrypt.compareSync(data.pass, user[0].pass);
    if (passwordIsValid == true) {
      clavecorrecta = true;
    }
  }

  if(clavecorrecta){
    //GUARDAR TOKEN SI HAY CUENTAS EN LOS DIFERENTES NIVELES DE USUARIOS
    if(admin[0])
      await Admin.query().updateAndFetchById(admin[0].id, {token:admin[0].token});
    else if(user[0])
      await Usuario_info.query().updateAndFetchById(user[0].id, {token:user[0].token});
    AR.enviarDatos(dataall, res);

  }else{
    dataall = {}
    dataall.msj = "Datos incorrectos";
    dataall.r = false;
    AR.enviarDatos(dataall, res);
  }
});

// Hacer login - mediante app.
router.post("/app", upload.none(), async (req, res, next) => {
  data = req.body;
  clavecorrecta = false;
  dataall = [];
  tipocliente = req.headers['tipo-cliente'];

  let errr = false;
  // inicio validaciones
  if (!data.correo) errr = "Ingresa el correo";
  if (!data.pass) errr = "Ingresa una contraseña";
  //fin validaciones
  if(errr) return AR.enviarDatos({r: false, msj: errr}, res);


  // admin = await Admin.query().where({'correo':data.correo}).catch(async err => {console.log("error admin: "+err)})

  // if(admin[0]){
  //   admin[0].token  = uuid.v1();
  //   admin[0].r=true;
  //   admin[0].tipo_usu  = 'administrador';
  //   admin[0].text_tipo_usu  = 'Administrador';
  //   dataall.push(admin[0]);

  //   let passwordIsValid = bcrypt.compareSync(data.pass, admin[0].pass);
  //   if (passwordIsValid == true) {
  //     clavecorrecta = true;
  //   }
  // }

  repar = await Repartidor.query().where({'correo':data.correo}).catch(async err => {console.log("error repartidor: "+err)})

  if(repar[0]){
    repar[0].token  = uuid.v1();
    repar[0].r=true;
    repar[0].tipo_usu  = 'repartidor';
    repar[0].text_tipo_usu  = 'Repartidor';
    dataall.push(repar[0]);

    let passwordIsValid = bcrypt.compareSync(data.pass, repar[0].pass);
    if (passwordIsValid == true) {
      clavecorrecta = true;
    }
    if(tipocliente && tipocliente == 'app' && !repar[0].estatus){
      errr = 'El usuario no ha sido verificado';
      clavecorrecta = false;
    }

  }


  client = await Cliente.query().where({'correo':data.correo})
    .catch(async err => {console.log("error usuario: "+err)});

  if(client[0] && !client[0].activado)
    return AR.enviarDatos({r: false, msj: "El correo no ha sido activado"}, res);

  if(client[0]){
    client[0].token  = uuid.v1();
    client[0].r=true;
    client[0].tipo_usu  = 'cliente';
    client[0].text_tipo_usu  = 'Cliente';
    dataall.push(client[0]);

    let passwordIsValid = bcrypt.compareSync(data.pass, client[0].pass);
    if (passwordIsValid == true) {
      clavecorrecta = true;
    }
  }

  if(clavecorrecta){
    //GUARDAR TOKEN SI HAY CUENTAS EN LOS DIFERENTES NIVELES DE USUARIOS
    // if(admin[0])
    //   await Admin.query().updateAndFetchById(admin[0].id, {token:admin[0].token});
    if(repar[0])
      await Repartidor.query().updateAndFetchById(repar[0].id, {token:repar[0].token});
    else if(client[0])
      await Cliente.query().updateAndFetchById(client[0].id, {token:client[0].token});
    
    AR.enviarDatos(dataall, res);

  }else{
    dataall = {}
    dataall.msj = errr || "Datos incorrectos";
    dataall.r = false;
    AR.enviarDatos(dataall, res);
  }
});

router.post("/cambiar-clave", upload.none(), sesi.validar_general, async(req, res, next) => {
    data = req.body;
    //console.log(data.usuario);
    try {
        if (!data.pass) {
            AR.enviarError('Por favor introduzca la contraseña actual', res, 200);
            return;
        }
        if (!data.pass_new) {
            AR.enviarError('Por favor introduzca la nueva contraseña', res, 200);
            return;
        }
        if (!data.pass_new_con) {
            AR.enviarError('Por favor introduzca el confirmar contraseña', res, 200);
            return;
        }
        if (data.pass_new == data.pass) {
            AR.enviarError('La contraseña nueva no debe coincidir con la actual', res, 200);
            return;
        }
        if (data.pass_new != data.pass_new_con) {
            AR.enviarError('Las contraseñas no coinciden', res, 200);
            return;
        }

        if(data.usuario=='administrador')
          usuario = await Admin.query().findOne("id", data.id).throwIfNotFound().catch(err => console.log(err));
        if(data.usuario=='restaurante')
          usuario = await Usuario.query().findOne("id", data.id).throwIfNotFound().catch(err => console.log(err));
        if(data.usuario=='repartidor')
          usuario = await Repartidor.query().findOne("id", data.id).throwIfNotFound().catch(err => console.log(err));
        if(data.usuario=='cliente')
          usuario = await Cliente.query().findOne("id", data.id).throwIfNotFound().catch(err => console.log(err));
        
        let passwordIsValid = bcrypt.compareSync(data.pass, usuario.pass);

        if (!passwordIsValid) {
          AR.enviarError('Su contraseña actual es incorrecta.', res, 200);
          return;
        }
        if(data.usuario=='administrador'){
            var obj = {
              pass: bcrypt.hashSync(req.body.pass_new, 8)
            }
            await Admin.query()
              .patchAndFetchById(
                data.id, obj)
              .then(usuario => {
                usuario.r = true;
                usuario.msg = "Ha cambiado su contraseña satisfactoriamente.";
                return AR.enviarDatos(usuario, res);
              })
              .catch(err => {
                console.log(err);
                res.send(err);
              });
        }else if(data.usuario=='restaurante'){
            var obj = {
              pass: bcrypt.hashSync(req.body.pass_new, 8)
            }
            await Usuario.query()
              .patchAndFetchById(
                data.id, obj)
              .then(usuario => {
                usuario.r = true;
                usuario.msg = "Ha cambiado su contraseña satisfactoriamente.";
                return AR.enviarDatos(usuario, res);
              })
              .catch(err => {
                console.log(err);
                res.send(err);
              });
        }else if(data.usuario=='repartidor'){
            var obj = {
              pass: bcrypt.hashSync(req.body.pass_new, 8)
            }
            await Repartidor.query()
              .patchAndFetchById(
                data.id, obj)
              .then(usuario => {
                usuario.r = true;
                usuario.msg = "Ha cambiado su contraseña satisfactoriamente.";
                return AR.enviarDatos(usuario, res);
              })
              .catch(err => {
                console.log(err);
                res.send(err);
              });
        }else if(data.usuario=='cliente'){
            var obj = {
              pass: bcrypt.hashSync(req.body.pass_new, 8)
            }
            await Cliente.query()
              .patchAndFetchById(
                data.id, obj)
              .then(usuario => {
                usuario.r = true;
                usuario.msg = "Ha cambiado su contraseña satisfactoriamente.";
                return AR.enviarDatos(usuario, res);
              })
              .catch(err => {
                console.log(err);
                res.send(err);
              });
        }
    } catch (err) {
        console.log(err)
        next(err);
    }
});

router.post("/recuperar", upload.none(), async (req, res, next) => {
  data = req.body;


  let errr = false;
  // inicio validaciones
  if (!data.correo) errr = "Ingrese el correo";
  
  //fin validaciones
  if(errr) return  res.send({r: false, msj: errr}, res);
   const ced = await Admin.query()
      .where({ correo: data.correo })
      .then(async resp => {
        if (resp[0]) {
          if (resp[0].correo == data.correo) {
            admin = resp[0];
            admin.token  = uuid.v1();

            await Admin.query().updateAndFetchById(admin.id, {token:admin.token, pass: bcrypt.hashSync(admin.token.substr(0,10), 8)});
            var cuerpo = `Hola, ${admin.nombre} ${admin.apellido}
              <br> Tus datos de acceso a
              la plataforma de Deliveryapp son: <br>
              <b>Usuario: </b> ${admin.correo} <br><b>Contraseña: </b> ${admin.token.substr(0,10)}
            `;
            mailer.enviarMail({
                direccion: admin.correo,
                titulo: "RECUPERAR CREDENCIALES DE ACCESO",
                mensaje: mailp({
                    cuerpo: cuerpo,
                    titulo: "RECUPERAR CREDENCIALES DE ACCESO"
                })
            });
            return  AR.enviarDatos({r: true, msj: "Verifique el correo que se ha enviado" }, res);
          } else {
            return  AR.enviarDatos({r: false, msj: "No existe un administrador con esta información."}, res);
          }
        } else {
          return  AR.enviarDatos({r: false, msj: "No existe un administrador con esta información."}, res);
        }
      }).catch(err => {
        return  AR.enviarDatos({r: false, msj: "No existe un administrador con esta información."}, res);
      });
});

router.post("/recuperar_app", upload.none(), async (req, res, next) => {
  data = req.body;


  let errr = false;
  // inicio validaciones
  if (!data.correo) errr = "Ingrese el correo";
  
  //fin validaciones
  if(errr) return  res.send({r: false, msj: errr}, res);
   
   const ced = await Repartidor.query()
      .where({ correo: data.correo })
      .skipUndefined()
      .then(async resp => {
        if (resp[0]) {
          admin = resp[0];
          admin.token  = uuid.v1();
          await Repartidor.query().updateAndFetchById(admin.id, {token:admin.token, pass: bcrypt.hashSync(admin.token.substr(0,10), 8)});
          var cuerpo = `Hola, ${admin.nombre} ${admin.apellido}
            <br> Tus datos de acceso a
            la plataforma de Deliveryapp son: <br>
            <b>Correo: </b> ${admin.correo} <br><b>Contraseña: </b> ${admin.token.substr(0,10)}
          `;
          mailer.enviarMail({
              direccion: admin.correo,
              titulo: "RECUPERAR CREDENCIALES DE ACCESO",
              mensaje: mailp({
                  cuerpo: cuerpo,
                  titulo: "RECUPERAR CREDENCIALES DE ACCESO"
              })
          });
          return  AR.enviarDatos({r: true, msj: "Verifique el correo que se ha enviado" }, res);
        } else {
          const ced = await Cliente.query()
            .where({ correo: data.correo })
            .skipUndefined()
            .then(async resp => {
              if (resp[0]) {
                admin = resp[0];
                admin.token  = uuid.v1();
                await Cliente.query().updateAndFetchById(admin.id, {token:admin.token, pass: bcrypt.hashSync(admin.token.substr(0,10), 8)});
                var cuerpo = `Hola, ${admin.nombre} ${admin.apellido}
                  <br> Tus datos de acceso a
                  la plataforma de Deliveryapp son: <br>
                  <b>Correo: </b> ${admin.correo} <br><b>Contraseña: </b> ${admin.token.substr(0,10)}
                `;
                mailer.enviarMail({
                    direccion: admin.correo,
                    titulo: "RECUPERAR CREDENCIALES DE ACCESO",
                    mensaje: mailp({
                        cuerpo: cuerpo,
                        titulo: "RECUPERAR CREDENCIALES DE ACCESO"
                    })
                });
                return  AR.enviarDatos({r: true, msj: "Verifique el correo que se ha enviado" }, res);
              } else {
                return  AR.enviarDatos({r: false, msj: "No existe un usuario con esta información."}, res);
              }
            }).catch(err => {
              return  AR.enviarDatos({r: false, msj: "No existe un usuario con esta información."}, res);
            });
        }
      }).catch(err => {
        return  AR.enviarDatos({r: false, msj: "No existe un usuario con esta información."}, res);
      });
});

// Verificar si el token sigue siendo valido.
router.post("/app/validar_token", upload.none(), async(req, res)=>{
  let token = req.body.token || req.query.token || req.headers.token;
  let tipo_usuario = req.body.tipo_usuario || req.query.tipo_usuario || req.headers.tipo_usuario;
  let tipo_cliente = req.headers['tipo-cliente'];
  let query;

  if(!token || tipo_cliente!='app' || !tipo_usuario && tipo_usuario.length == 0)
    return res.sendStatus(401);

  switch(tipo_usuario){
    case 'cliente': query = Cliente.query(); break;
    case 'repartidor': query = Repartidor.query(); break;
    default: return res.sendStatus(401);
  }

  await query.where("token", tok)
      .then(usuario=>{
          if(usuario.length>0)
              return res.sendStatus(204); // Exito, nada se devuelve.
          else
              return res.sendStatus(401);
      })
      .catch(err=>{
          return res.sendStatus(401);
      });

});

module.exports = router;
