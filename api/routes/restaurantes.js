//llamar al modelo
const Restaurante = require("../models/Restaurante");
const Usuario = require("../models/Usuario");
const Categoria = require("../models/Categoria");
const Subcategoria = require("../models/Subcategoria");
const RestauranteSubcategorias = require("../models/RestauranteSubcategorias");
const CategoriaMenu = require("../models/CategoriaMenu");
const Menu = require("../models/Menu");

//Llamar otras librerias
const AR = require("../ApiResponser");
const express = require("express");
const moment = require("moment");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const config = require("../config");
var uuid = require("uuid");
var sesion = require("./sesion");
var sesi = new sesion();
var archivos = require("../Archivos");
var fs = require("fs-extra");

const mailer = require("../mails/Mailer");
const mailp = require("../mails/correo");
const validarMail = require("../functions/validateMail");
const verificarRIF = require("../functions/verificarRIF");
const puntoEnRadio = require("../functions/coordUtils/puntoEnRadio");

//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();
const crypto = require("crypto");

//const configuracion = JSON.parse(fs.readFileSync(__dirname+"/../configuracion.json"));

//Manejador de Errores
const { AuthError, ValidationError } = require("../Errores");

var router = express.Router();


//registrar un Restaurante
router.post("/add", archivos.fields([{ name: "img", maxCount: 1 }, { name: "img_portada", maxCount: 1 }]), sesi.validar_admin, async (req, res, next) => {
  data = req.body;
  //data usuario
  infoced = data.cedulas;
  infocor = data.correos;
  infopas = data.pass;
  infona = data.nombres_apellidos;
  RIF = `${data.rif_letra}-${data.rif_numero}-${data.rif_chequeo}`;

  let errr = false;
  let errr_u = false;
  var cont_u = 0;
  let errr_c = false;
  var cont_c = 0;

  // inicio validaciones
  if (!data.nombre) errr = "Ingresa el nombre del doomialiado";
  if (!data.ubicacion) errr = "Ingresa la ubicación";
  if (!data.pais) errr = "Selecciona el pais";
  if (!data.estado) errr = "Selecciona el estado";
  if (!data.ciudad) errr = "Selecciona la ciudad";
  if (!data.subcategoria) errr = "Seleccione una subcategoría";
  if (!data.categoria) errr = "Seleccione una categoría";
  if (!data.lat || !data.lon) errr = "Selecciona la ubicación en el mapa";
  if (!verificarRIF(RIF))
    errr = "RIF no válido";

  if (RIF){
    const verificar_rif = await Restaurante.query()
      .where({rif_letra:data.rif_letra, rif_numero:data.rif_numero, rif_chequeo:data.rif_chequeo});

    if (verificar_rif[0]) errr = "El RIF ya existe";
  }

  if(infoced){
    for (var i = 0; i < infoced.length; i++) {
      const verificar_u = await Usuario.query().where({ cedula: infoced[i] });
      if (verificar_u[0]){
        if(errr_u)
          errr_u += ", "+infoced[i];
        else
          errr_u = infoced[i];
        cont_u++;
      }else{
        const verificar_c = await Usuario.query().where({ correo: infocor[i] });
        if (verificar_c[0]){
          if(errr_c)
            errr_c += ", "+infocor[i];
          else
            errr_c = infocor[i];
          cont_c++;
        }
      } 
    }
  }
  if(errr_u){
    if(cont_u==1)
      errr = "El usuario "+errr_u+" ya existe";
    else
      errr = "Los usuarios "+errr_u+" ya existen";
  }
  if(errr_c && !errr_u){
    if(cont_c==1)
      errr = "El correo "+errr_c+" ya existe";
    else
      errr = "Los correos "+errr_c+" ya existen";
  }

  //fin validaciones
  if(errr){
    //ELIMINAR IMAGENES SI NO PASA LAS VALIDACIONES
    if(!data.imgweb){ //saber si se cargan las imagenes desde movil
      if(req.files["img"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img"][0].filename, function (err) { });
      if(req.files["img_portada"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img_portada"][0].filename, function (err) { });
    }
    return AR.enviarDatos({r: false, msj: errr}, res);
  }

  var datos = {
    nombre: data.nombre,
    descripcion: data.descripcion,
    ubicacion: data.ubicacion,
    lat: data.lat,
    lon: data.lon,
    rif_letra: data.rif_letra,
    rif_numero: data.rif_numero,
    rif_chequeo: data.rif_chequeo,
    telefono: data.telefono,
    facebook: data.facebook,
    instagram: data.instagram,
    id_categoria: data.categoria,
    // id_subcategoria: data.subcategoria,
    id_pais: data.pais,
    id_estado: data.estado,
    id_ciudad: data.ciudad,
    pre_tasa: 0
  };
  //guardar ruta imagen
  if(!data.imgweb){ 
    if(req.files["img"])
      datos.img = config.rutaArchivo(req.files["img"][0].filename);
    if(req.files["img_portada"])
      datos.img_portada = config.rutaArchivo(req.files["img_portada"][0].filename);
  }else if(data.imgweb){
    if(data.img){
      var file = "img_log__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
      await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.img.split(",")[1], 'base64'), 'base64', function(err) {
        //console.log(err);
      });
      datos.img = config.rutaArchivo(file);
    }
    if(data.img1){
      var file1 = "fot_por__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
      await require("fs").writeFile(__dirname + "/../public/uploads/" + file1, new Buffer.from(data.img1.split(",")[1], 'base64'), 'base64', function(err) {
        // console.log(err);
      });
      datos.img_portada = config.rutaArchivo(file1);
    }
  }


  await Restaurante.query()
  .insert(datos)
  .then(async resp => {
    if(infoced){
      for (var i = 0; i < infoced.length; i++) {
        datos = {
          id_restaurante: resp.id,
          cedula: infoced[i],
          pass: bcrypt.hashSync(infopas[i], 8),
          correo: infocor[i],
          nombre_apellido: infona[i]
        };
        await Usuario.query().insert(datos).catch(err => { console.log(err); });


        // Guardar las relaciones de varias subcategorias.
        let subs = await Subcategoria.query().whereIn("id", data.subcategoria).catch(e => console.log(e));

        // let relaciones_categoricas = [];
        for (subcat of subs){
          // relaciones_categoricas.push({id_categoria: data.categoria, id_subcategoria: subcat.id})
          await RestauranteSubcategorias.query()
            .insert({id_restaurante: resp.id, id_categoria:resp.id_categoria, id_subcategoria: subcat.id})
            .catch(e => console.log(e));
        }


        // await Subcategoria.query()
        //   .where("id", resp.id_subcategoria).increment("veces_usada", 1);
      }
    }
    resp.msj = "Doomialiado registrado";
    resp.r = true;
    
    
    AR.enviarDatos(resp, res);
  })
  .catch(async err => {
    //ELIMINAR IMAGENES POR SI EXISTE ALGUN ERROR
    if(!req.body.imgweb){
      if(req.files["img"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img"][0].filename, function (err) { });
      if(req.files["img_portada"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img_portada"][0].filename, function (err) { });
    }
    console.log(err)
    err.r = false;
    err.msj = 'Error al registrar Doomialiado';
    res.send(err);
  });
});

//editar Restaurante
router.put("/edit", archivos.fields([{ name: "img", maxCount: 1 }, { name: "img_portada", maxCount: 1 }]), sesi.validar_general, async (req, res, next) => {
  data = req.body;
  infoced = data.cedulas;
  infopas = data.pass;
  idinfousu = data.idusu;
  infocor = data.correos;
  infona = data.nombres_apellidos;
  RIF = `${data.rif_letra}-${data.rif_numero}-${data.rif_chequeo}`;


  let errr = false;
  let errr_u = false;
  var cont_u = 0;
  let errr_c = false;
  var cont_c = 0;

  // inicio validaciones
  if (!data.nombre) errr = "Ingresa el nombre del doomialiado";
  if (!data.ubicacion) errr = "Ingresa la ubicación";
  if (!data.pais) errr = "Selecciona el pais";
  if (!data.estado) errr = "Selecciona el estado";
  if (!data.ciudad) errr = "Selecciona la ciudad";
  if (!data.subcategoria) errr = "Seleccione una subcategoría";
  if (!data.categoria) errr = "Seleccione una categoría";
  if (!data.lat || !data.lon) errr = "Selecciona la ubicación en el mapa";
  if (!data.iden) errr = "Ingresa la id";
  if (! (data.modalidad_delivery | data.modalidad_pickup | data.modalidad_programados) )
    errr = "Seleccione una modalidad de trabajo";
  if (!verificarRIF(RIF))
    errr = "RIF no válido";

  if (RIF && data.iden){
    const verificar_rif = await Restaurante.query()
      .whereNot("id", data.iden)
      .where({rif_letra:data.rif_letra, rif_numero:data.rif_numero, rif_chequeo:data.rif_chequeo});

    if (verificar_rif[0]) errr = "El RIF ya existe";
  }


  if(infoced){
    for (var i = 0; i < infoced.length; i++) {
      if(idinfousu[i]==0)
        verificar_u = await Usuario.query().where({ cedula: infoced[i] });
      else
        verificar_u = await Usuario.query().where({ cedula: infoced[i] }).where("id","<>",idinfousu[i]); 
      if (verificar_u[0]){
        if(errr_u)
          errr_u += ", "+infoced[i];
        else
          errr_u = infoced[i];
        cont_u++;
      }else{
        if(idinfousu[i]==0)
          verificar_c = await Usuario.query().where({ correo: infocor[i] });
        else
          verificar_c = await Usuario.query().where({ correo: infocor[i] }).where("id","<>",idinfousu[i]);
        if (verificar_c[0]){
          if(errr_c)
            errr_c += ", "+infocor[i];
          else
            errr_c = infocor[i];
          cont_c++;
        }
      } 
    }
  }
  if(errr_u){
    if(cont_u==1)
      errr = "El usuario "+errr_u+" ya existe";
    else
      errr = "Los usuarios "+errr_u+" ya existen";
  }
  if(errr_c && !errr_u){
    if(cont_c==1)
      errr = "El correo "+errr_c+" ya existe";
    else
      errr = "Los correos "+errr_c+" ya existen";
  }
  
  //fin validaciones
  if(errr){
    //ELIMINAR IMAGENES SI NO PASA LAS VALIDACIONES
    if(!data.imgweb){
      if(req.files["img"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img"][0].filename, function (err) { });
      if(req.files["img_portada"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img_portada"][0].filename, function (err) { });
    }
    return AR.enviarDatos({r: false, msj: errr}, res);
  }

  var datos = {
    nombre: data.nombre,
    descripcion: data.descripcion,
    ubicacion: data.ubicacion,
    rif_letra: data.rif_letra,
    rif_numero: data.rif_numero,
    rif_chequeo: data.rif_chequeo,
    telefono: data.telefono,
    facebook: data.facebook,
    instagram: data.instagram,
    // pedidos_programados: data.pedidos_programados,
    // pedidos_programados_dias: data.pedidos_programados=='D' ? data.pedidos_programados_dias : null,
    lat: data.lat,
    lon: data.lon,
    id_categoria: data.categoria,
    // id_subcategoria: data.subcategoria,
    id_pais: data.pais,
    id_estado: data.estado,
    id_ciudad: data.ciudad,
    modalidad_delivery: data.modalidad_delivery || 0,
    modalidad_pickup: data.modalidad_pickup || 0,
    modalidad_programados: data.modalidad_programados || 0,
    modalidad_programados_cantidad: data.modalidad_programados_cantidad || 0,
  }
  //guardar ruta imagen
  if(!data.imgweb){
    if(req.files["img"])
      datos.img = config.rutaArchivo(req.files["img"][0].filename);
    if(req.files["img_portada"])
      datos.img_portada = config.rutaArchivo(req.files["img_portada"][0].filename);
  }else if(data.imgweb){
      if(data.img){
        var file = "img_log__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file, new Buffer.from(data.img.split(",")[1], 'base64'), 'base64', function(err) {
          //console.log(err);
        });
        datos.img = config.rutaArchivo(file);
      }
      if(data.img1){
        var file1 = "fot_por__" + crypto.randomBytes(18).toString("hex") + "__.jpg";
        await require("fs").writeFile(__dirname + "/../public/uploads/" + file1, new Buffer.from(data.img1.split(",")[1], 'base64'), 'base64', function(err) {
          // console.log(err);
        });
        datos.img_portada = config.rutaArchivo(file1);
      }
    }

  var info = await Restaurante.query().where({ id: data.iden });//OBTENER DATA ANTES DE ACTUALIZAR

  // Ajustar las veces que se usaron las categorias.
  // if (info[0].id_subcategoria != data.id_subcategoria)
  //   await Categoria.query().where("id", info[0].id_subcategoria).decrement("veces_usada", 1);
  //   await Categoria.query().where("id", data.subcategoria).increment("veces_usada", 1);

  await Restaurante.query().updateAndFetchById(data.iden, datos)
  .then(async resp => {
    //ELIMINAR IMAGENES ANTERIOR SI HA GUARDADO UNA NUEVA
    if((req.files["img"] || data.img) && + info[0].img)
      await fs.unlink(__dirname + "/../public/uploads/" + info[0].img.split(`\\public\\uploads\\`)[1], function (err) { });
    if((req.files["img_portada"] || data.img1) && info[0].img_portada)
      await fs.unlink(__dirname + "/../public/uploads/" + info[0].img_portada.split(`\\public\\uploads\\`)[1], function (err) { });
    if(infoced){
      for (var i = 0; i < infoced.length; i++) {
        //validar si es nuevo o ya existe el usuario
        if(idinfousu[i]==0){ //si es nuevo
          datos = {
            id_restaurante: resp.id,
            cedula: infoced[i],
            pass: bcrypt.hashSync(infopas[i], 8),
            correo: infocor[i],
            nombre_apellido: infona[i]
          };
          await Usuario.query().insert(datos).catch(err => { console.log(err); });
        }else{ //ya existe
          datos = {
            cedula: infoced[i],
            correo: infocor[i],
            nombre_apellido: infona[i]
          };
          if(infopas[i].length>1)
            datos.pass=bcrypt.hashSync(infopas[i], 8);
          await Usuario.query().update(datos).where("id",idinfousu[i]).catch(err => { console.log(err); });
        }
      }
        // Guardar las relaciones de varias subcategorias.
        let subs = await Subcategoria.query().whereIn("id", data.subcategoria).catch(e => console.log(e));

        // Limpiar subcategorias anteriores.
        await RestauranteSubcategorias.query().delete().where("id_restaurante", resp.id)
        for (subcat of subs){
          await RestauranteSubcategorias.query()
            .insert({id_restaurante: resp.id, id_categoria:resp.id_categoria, id_subcategoria: subcat.id})
            .catch(e => console.log(e));
        }
    }
    resp.msj = "Datos actualizados con éxito";
    resp.r = true;
    AR.enviarDatos(resp, res);
  })
  .catch(async err => {
    if(!req.body.imgweb){
      if(req.files["img"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img"][0].filename, function (err) { });
      if(req.files["img_portada"])
        await fs.unlink(__dirname + "/../public/uploads/" + req.files["img_portada"][0].filename, function (err) { });
    }
    await Categoria.query().where("id", info.id_subcategoria).decrement("veces_usada", 1);
    console.log(err)
    err.r = false;
    err.msj = 'Error al actualizar datos';
    res.send(err);
  });

});

//editar Restaurante
router.put("/edit_tasa", upload.none(), sesi.validar_restaurante, async (req, res, next) => {
  data = req.body;
  let errr = false;

  // inicio validaciones
  if (!data.tasa) errr = "Ingresa la tasa";
  if (!data.iden) errr = "Ingresa la id del Doomialiado";
  
  //fin validaciones
  if(errr){
    return AR.enviarDatos({r: false, msj: errr}, res);
  }

  var datos = {
    pre_tasa: data.tasa
  }
  

  var info = await Restaurante.query().where({ id: data.iden });//OBTENER DATA ANTES DE ACTUALIZAR
  await Restaurante.query().updateAndFetchById(data.iden, datos)
  .then(async resp => {
    resp.msj = "Tasa actualizada con éxito";
    resp.r = true;
    AR.enviarDatos(resp, res);
  })
  .catch(async err => {
    console.log(err)
    err.r = false;
    err.msj = 'Error al actualizar tasa';
    res.send(err);
  });

});

//editar Estatus
router.put("/edit_sts", upload.none(), sesi.validar_admin, async (req, res, next) => {
  data = req.body;
  let errr = false;

  // inicio validaciones
  if (!data.sts) errr = "Ingresa el estatus";
  if (!data.iden) errr = "Ingresa la id del Doomialiado";
  
  //fin validaciones
  if(errr){
    return AR.enviarDatos({r: false, msj: errr}, res);
  }

  var datos = {
    estatus: data.sts
  }
  

  await Restaurante.query().updateAndFetchById(data.iden, datos)
  .then(async resp => {
    resp.msj = "Estatus actualizado con éxito";
    resp.r = true;
    AR.enviarDatos(resp, res);
  })
  .catch(async err => {
    console.log(err)
    err.r = false;
    err.msj = 'Error al actualizar estatus';
    res.send(err);
  });

});

//editar Estatus laborando
router.put("/edit_sts_laborando", upload.none(), sesi.validar_general, async (req, res, next) => {
  data = req.body;
  let errr = false;

  // inicio validaciones
  if (!data.sts) errr = "Ingresa el estatus";
  if (!data.iden) errr = "Ingresa la id del Doomialiado";
  
  //fin validaciones
  if(errr){
    return AR.enviarDatos({r: false, msj: errr}, res);
  }

  await Restaurante.query().updateAndFetchById(data.iden, {laborando: data.sts})
    .then(async resp => {
      resp.msj = "Estatus actualizado con éxito";
      resp.r = true;
      AR.enviarDatos(resp, res);
    })
    .catch(async err => {
      console.log(err)
      err.r = false;
      err.msj = 'Error al actualizar estatus';
      res.send(err);
    });
});

//listar todos los Restaurante
router.get("/", /*sesi.validar_general,*/ async(req, res) => {
  await Restaurante.query()
      .then(ret => {
      AR.enviarDatos(ret, res);
  });
});

//listar todos los Restaurante
router.get("/app", /*sesi.validar_general,*/ async(req, res) => {
  const configuracion = JSON.parse(fs.readFileSync(__dirname+"/../configuracion.json"));
  data = req.query;
  //console.log(data);
  var lat_act = data.lat;
  var lon_act = data.lon;
  filtro = ",(acos(sin(radians(lat)) * sin(radians(" + lat_act + ")) + cos(radians(lat)) * cos(radians(" + lat_act + ")) * cos(radians(lon) - radians(" + lon_act + "))) * 6371) as distancia";
  filtro1 = " GROUP BY id having distancia < (" + configuracion.km + ") ORDER BY distancia asc ";
  var sqlC = "SELECT *, (SELECT c.nombre FROM categorias c WHERE c.id=id_categoria) as nombre_cat" + filtro + " from restaurantes where estatus=1 " + filtro1 + ";";   
  //await Restaurante.query().where("estatus",1)
  await Restaurante.raw(sqlC)
    .then(ret => {
      //console.log(ret.length,lat_act,lon_act,configuracion.km);
      /*let restCercanos = ret.filter(rest=>{
        return puntoEnRadio(
          {lat:rest.lat, lon:rest.lon},
          {lat:lat_act, lon:lon_act}, //ubicacion actual
          configuracion.km,
        );
      });*/

      return AR.enviarDatos(ret[0], res);
    });
});

router.get("/app_categoria", /*sesi.validar_general,*/ async(req, res) => {
  const configuracion = JSON.parse(fs.readFileSync(__dirname+"/../configuracion.json"));
  data = req.query;
  //console.log(data);
  var lat_act = data.lat;
  var lon_act = data.lon;
  await Categoria.query()
    .then(async ret => {
      //console.log(ret);
      filtro = ",(acos(sin(radians(lat)) * sin(radians(" + lat_act + ")) + cos(radians(lat)) * cos(radians(" + lat_act + ")) * cos(radians(lon) - radians(" + lon_act + "))) * 6371) as distancia";
      filtro1 = " GROUP BY id having distancia < (" + configuracion.km + ") ORDER BY distancia asc ";
      for (var i = 0; i < ret.length; i++) {
        var sqlC = "SELECT *, (SELECT c.nombre FROM categorias c WHERE c.id=id_categoria) as nombre_cat" + filtro + " from restaurantes where estatus=1 AND id_categoria="+ret[i].id+" " + filtro1 + ";";
        rest = await Restaurante.raw(sqlC);
        ret[i].restaurantes = rest[0];
      };

      return AR.enviarDatos(ret, res);
    });
});

router.get("/escritorio", sesi.validar_restaurante, async(req, res) => {
  dataall = {categorias:0,pedidos:0,menu:0};

  categorias = await CategoriaMenu.query().where("id_restaurante", req.query.id_res)
  .catch(async err => {console.log("error categoria: "+err)});

  menus = await Menu.query().where("id_restaurante", req.query.id_res)
  .catch(async err => {console.log("error categoria: "+err)});
  
  if(categorias)
    dataall.categorias=categorias.length;

  if(menus)
    dataall.menu=menus.length;

  AR.enviarDatos(dataall, res);

});

//ver un Restaurante
router.get("/:id", /*sesi.validar_general,*/ async(req, res) => {
  await Restaurante.query().where("id", req.params.id).then(async (rest) => {
    AR.enviarDatos(rest[0], res);
  });
});

//eliminar Restaurante
router.delete("/delete/:id", upload.none(), sesi.validar_admin, async(req, res) => {
  data = req.query;
  var info = await Restaurante.query().where({ id: req.params.id })
  const eliminado = await Restaurante.query().delete().where({ id: req.params.id })
    .catch(err => {
      console.log(err);
      res.send(err);
    });

    if (eliminado){
      if(info[0]){
        await RestauranteSubcategorias.query().delete().where("id_restaurante", info[0].id).catch(err=>console.log(err));
        await Usuario.query().delete().where("id_restaurante", info[0].id).catch(err=>console.log(err));
        if(info[0].img)
          await fs.unlink(__dirname + "/../public/uploads/" + info[0].img.split(`\\public\\uploads\\`)[1], function (err) { });
      }
      // await Categoria.query()
      //   .where("id", info[0].id_categoria).decrement("veces_usada", 1);
      // await Subcategoria.query()
      //   .where("id", info[0].id_subcategoria).decrement("veces_usada", 1);
      AR.enviarDatos({ msj: "Registro eliminado exitosamente" }, res);
    }
    else{ AR.enviarDatos({ msj: "No se puede eliminar el Doomialiado" }, res);}
});
//eliminar usuario
router.delete("/delete_usu/:id", upload.none(), sesi.validar_admin, async(req, res) => {
  data = req.query;
  const eliminado = await Usuario.query().delete().where({ id: req.params.id })
    .catch(err => {
      console.log(err);
      res.send(err);
    });

    if (eliminado){
      AR.enviarDatos({ msj: "Usuario eliminado exitosamente" }, res);
    }
    else{ AR.enviarDatos({ msj: "No se puede eliminar el usuario" }, res);}
});

module.exports = router;
