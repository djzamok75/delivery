import { auth } from '../api'


const routes = [
  {
    path: '/Home',
    component: () => import('layouts/Menu.vue'),
    children: [
      { name: 'home', path: '', component: () => import('pages/Home.vue') }
    ]
  },
  {
    path: '/Home/:id',
    component: () => import('layouts/Menu.vue'),
    children: [
      { name: 'home', path: '', component: () => import('pages/Home.vue') }
    ]
  },
  {
    path: '/business',
    name: 'business',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Business.vue') }
    ]
  },
  {
    path: '/registro/',
    name: 'registro',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Registro.vue') }
    ]
  },
  {
    path: '/registroRepartidor',
    name: 'registro',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/RegistroRepartidor.vue') }
    ]
  },
  {
    path: '/profile',
    name: 'profile',
    component: () => import('layouts/Menu.vue'),
    beforeEnter: auth.requiereAutenticacion,
    children: [
      { path: '', component: () => import('pages/Profile.vue') }
    ]
  },
  {
    path: '/profileRepartidor',
    name: 'profileRepartidor',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/ProfileRepartidor.vue') }
    ]
  },
  {
    path: '/notifications',
    name: 'profile',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Notifications.vue') }
    ]
  },
  {
    path: '/Cambiarcontraseña',
    name: 'Cambiarcontraseña',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Cambiarcontraseña.vue') }
    ]
  },
  {
    path: '/Iniciodesesion',
    beforeEnter: auth.siEstaAutenticado,
    component: () => import('layouts/Menu.vue'),
    children: [
      { name: 'Iniciodesesion', path: '', component: () => import('pages/Iniciodesesion.vue') }
    ]
  },
  {
    path: '/Opcionesderegistro',
    name: 'Opcionesderegistro',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Opciones.vue') }
    ]
  },
  {
    path: '/Recuperarcontraseña',
    name: 'Recuperarcontraseña',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Recuperarcontraseña.vue') }
    ]
  },
  {
    path: '/Listadirecciones',
    name: 'Misdirecciones',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Listadirecciones.vue') }
    ]
  },
  {
    path: '/Misdirecciones',
    name: 'Midireccion',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Misdirecciones.vue') }
    ]
  },
  {
    path: '/Editardireccion/:id',
    name: 'editardireccion',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Misdirecciones.vue') }
    ]
  },
  {
    path: '/Configuraciones',
    name: 'Configuraciones',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Configuraciones.vue') }
    ]
  },
  {
    path: '/Menudoomialiado',
    name: 'Menudoomialiado',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', props:true, component: () => import('pages/Menudoomialiado.vue') }
    ]
  },
  {
    path: '/Menudoomialiadohome/',
    component: () => import('layouts/Menu.vue'),
    children: [
      {name: 'Menudoomialiadohome', path: '', props:true, component: () => import('pages/Menudoomialiadohome.vue') }
    ]
  },
  {
    path: '/Menudoomialiadohome/:id',
    component: () => import('layouts/Menu.vue'),
    children: [
      {name: 'Menudoomialiadohome', path: '', props:true, component: () => import('pages/Menudoomialiadohome.vue') }
    ]
  },
  {
    path: '/Menudetallehome',
    component: () => import('layouts/Menu.vue'),
    children: [
      {name: 'Menudetallehome', path: '', props:true, component: () => import('pages/Menudetallehome.vue') }
    ]
  },
  {
    path: '/Menudetallehome/:id',
    component: () => import('layouts/Menu.vue'),
    children: [
      {name: 'Menudetallehome', path: '', props:true, component: () => import('pages/Menudetallehome.vue') }
    ]
  },
  {
    path: '/Pedidos',
    component: () => import('layouts/Menu.vue'),
    beforeEnter: auth.requiereAutenticacion,
    children: [
      { name: 'Pedidos', path: '', component: () => import('pages/Pedidos.vue') }
    ]
  },
  {
    path: '/Categoria',
    component: () => import('layouts/Menu.vue'),
    children: [
      { name:'verCategoria', path:'', props:true, component: () => import('pages/Categorias.vue') }
    ]
  },
  {
    path: '/Vermas1',
    name: 'Vermas1',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Vermas1.vue') }
    ]
  },
  {
    path: '/Vermas2',
    name: 'Vermas2',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Vermas2.vue') }
    ]
  },
  {
    path: '/Vermas3',
    name: 'Vermas3',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Vermas3.vue') }
    ]
  },
  {
    path: '/Vermas4',
    name: 'Vermas4',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Vermas4.vue') }
    ]
  },
  {
    path: '/Sesionrepartidor',
    name: 'sesionrepartidor',
    component: () => import('layouts/Menu.vue'),
    beforeEnter: auth.siEstaAutenticado,
    children: [
      { path: '', component: () => import('pages/Sesionrepartidor.vue') }
    ]
  },
  {
    path: '/Mapa',
    name: 'mapa',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Mapa.vue') }
    ]
  },
  /*{
    path: '/Mapa2',
    name: 'Vermas1',
    component: () => import('layouts/Menurepartidor.vue'),
    children: [
      { path: '', component: () => import('pages/Mapa2.vue') }
    ]
  },
  {
    path: '/Mapa3',
    name: 'Vermas1',
    component: () => import('layouts/Menurepartidor.vue'),
    children: [
      { path: '', component: () => import('pages/Mapa3.vue') }
    ]
  },
  {
    path: '/Mapa4',
    name: 'Vermas1',
    component: () => import('layouts/Menurepartidor.vue'),
    children: [
      { path: '', component: () => import('pages/Mapa4.vue') }
    ]
  },*/
  {
    path: '/businessrepartidor',
    name: 'bussinesrepartidor',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/businessrepartidor.vue') }
    ]
  },
  {
    path: '/Notificationsrepartidor',
    name: 'notificacionesrepartidor',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Notificationsrepartidor.vue') }
    ]
  },
  {
    path: '/Loginrepartidor',
    name: 'loginrepartidor',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Loginrepartidor.vue') }
    ]
  },
  {
    path: '/Contraseñarepartidor',
    name: 'contraseñarepatidor',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Cambiarcontraseña.vue') }
    ]
    /*children: [
      { path: '', component: () => import('pages/Contraseñarepartidor.vue') }
    ]*/
  },
  /*{
    path: '/Platillo',
    name: 'Vermas1',
    component: () => import('layouts/Menunologeado.vue'),
    children: [
      { path: '', component: () => import('pages/Platillo.vue') }
    ]
  },
  {
    path: '/Platillologeado',
    name: 'Vermas1',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Platillologeado.vue') }
    ]
  },*/
  {
    path: '/Historial',
    name: 'historial',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Historial.vue') }
    ]
  },
  {
    path: '/Vermashistorial1',
    name: 'Vermash1',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Vermashistorial1.vue') }
    ]
  },
  {
    path: '/Recuperarrepartidor',
    name: 'recurepartidor',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Recuperarrepartidor.vue') }
    ]
  },
  {
    path: '/Favoritos',
    component: () => import('layouts/Menu.vue'),
    children: [
      { name: 'favoritos', path: '', component: () => import('pages/Favoritos.vue') }
    ]
  },
  {
    path: '/Vermashistorial2',
    name: 'Vermashistorial2',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Vermashistorial2.vue') }
    ]
  },
  {
    path: '/Vermashistorial3',
    name: 'Vermashistorial3',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Vermashistorial3.vue') }
    ]
  },
  {
    path: '/Vermashistorial4',
    name: 'Vermashistorial4',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Vermashistorial4.vue') }
    ]
  },
  {
    path: '/Carrito',
    component: () => import('layouts/Menu.vue'),
    children: [
      { name: 'carrito', path: '', component: () => import('pages/Carrito.vue') }
    ]
  },
  {
    path: '/Formasdepago',
    component: () => import('layouts/Menu.vue'),
    children: [
      { name: 'formasdepago', path: '', props: true, component: () => import('pages/Formasdepago.vue') }
    ]
  },
  {
    path: '/Pago',
    component: () => import('layouts/Menu.vue'),
    children: [
      { name: 'pago', path: '', props: true, component: () => import('pages/Pago.vue') }
    ]
  },
  {
    path: '/Vertodos',
    name: 'Vertodo',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Vertodos.vue') }
    ]
  },
  {
    path: '/Misordenes',
    component: () => import('layouts/Menu.vue'),
    children: [
      { name: 'Misordenes', path: '', component: () => import('pages/Misordenes.vue') }
    ]
  },
  {
    path: '/Mapahome',
    name: 'Mapahome',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/MapaHome.vue') }
    ]
  },
  {
    path: '/Ventanadecarga',
    name: 'Ventanadecarga',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Ventanadecarga.vue') }
    ]
  },
  {
    path: '/Detallescliente',
    name: 'Detallescliente',
    component: () => import('layouts/Menu.vue'),
    children: [
      { path: '', component: () => import('pages/Detallescliente.vue') }
    ]
  },
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
