function loginjs(){


    $('.modal').modal();
    //Enviar datos por método POST
    $('#formLogin').on('submit', function(e) {

        e.preventDefault();
        /*validaciones*/
        var errr = false;
        var msj = false;

        if (errr) {
            Materialize.toast(msj, 4000); return false;
        }

        //data a enviar
        var data = new FormData();
        data.append("correo", $("#correo").val());
        data.append("pass", $("#pass").val());
        $(".BTentrar").attr("disabled", true);
        $.ajax({
            url: dominio + "login/",
            type: "POST",
            dataType: "json",
            cache: false,
            data: data,
            contentType: false,
            processData: false,
            success: function(res) {

                //console.log(res);
                if(res.length>0){
                    if (res[0].r == true) {

                        localStorage.setItem('userDOOMI',JSON.stringify(res[0]));
                        userDOOMI = JSON.parse(localStorage.getItem('userDOOMI'))
                        location.href = 'vistas/index.html?op=inicio';

                    }
                }else {
                    $(".BTentrar").attr("disabled", false);
                }
                if(res.msj)
                    Materialize.toast(res.msj, 4000);
            },
            error: function(xhr, status, errorThrown) {
                console.log(errorThrown);
                Materialize.toast(errorThrown.msj, 4000)
                $(".BTentrar").attr("disabled", false);
            }
        });

    });



  

}
