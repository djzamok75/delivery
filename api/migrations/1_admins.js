exports.up = knex => {
    return knex.schema.createTable("administradores", table => {
        table.increments("id").primary();
        table.string("nombre");
        table.string("apellido");
        table.string("identificacion", 2);
        table.string("cedula", 20).unique();
        table.string("usuario", 50).unique();
        table.string("correo", 150).unique();
        table.text("pass");
        table.string("token", 150).unique();
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("administradores");
};
