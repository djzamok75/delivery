import Vue from 'vue'
import { utils } from '../../api'



export default {
  namespaced: true,


  state: () => ({
    pedidos: {},
    idDoomialiadoActual: null, // Solo se puede tener productos de un doomi a la vez.
    modalidad: null,
  }),


  getters:
  {
    items: state => Object.keys(state.pedidos).length,

    all: (state) => Object.values(state.pedidos),

    idDoomialiadoActual: state => state.idDoomialiadoActual,

    pedidosDetallados: (state, getters, rootState, rootGetters) => {
      const pedidos = Object.values(state.pedidos);

      // Incluir el producto.
      pedidos.map(e => e.producto = rootGetters['productos/porId'](e.ID));

      // Incluir adicionales.
      pedidos.map(p => {
        p.adicionales = (p.producto.adicionales || []) // Filtrar si no estan en idAdicionales.
          .filter(function(adi){return this.idAdicionales ? this.idAdicionales.includes(adi.id) : false}, p)
      });

      return pedidos;
    },

    total: state => {
      /* Precio total del carrito con adicionales */
      let total = 0;

      for (let pedido of Object.values(state.pedidos)){
        total = utils.sumarDinero(total, pedido.TOTAL);
      }

      return total;
    },

    subtotal: (state) =>{
      /* Precio total del carrito sin adicionales */
      let subtotal = 0;

      for (let pedido of Object.values(state.pedidos)){
        // subtotal = utils.sumarDinero(subtotal, (pedido.PRECIO *100) * pedido.cantidad / 100);
        subtotal = utils.sumarDinero(subtotal, pedido.SUBTOTAL);
      }

      return subtotal;
    },

    subtotalAdicionales: (state) => {
      /* Precio total de todos los adicionales */
      let subtotal = 0;

      for (let pedido of Object.values(state.pedidos)){
        // subtotal = utils.sumarDinero(subtotal, (pedido.SUBTOTAL_ADICIONALES *100) * pedido.cantidad / 100);
        subtotal = utils.sumarDinero(subtotal, pedido.SUBTOTAL_ADICIONALES);
      }

      return subtotal;
    },

    // itemPorId: (state) => (id) => state.carrito.filter(e => e.id == id).pop(),

    pedidoPorId: (state) => (idProducto) => state.pedidos[idProducto],

    cantidadPedido: (state) => (idProducto) => {
      let pedido = state.pedidos[idProducto];

      if(pedido && pedido.cantidad != undefined)
        return pedido.cantidad;

      return 0;
    }
  },


  mutations: {
    VACIAR: state => state.pedidos = {},

    SET_ID_DOOMIALIADO: (state, id) => state.idDoomialiadoActual = id,

    SET_ITEM: (state, item) => Vue.set(state.pedidos, item.ID, item),

    ELIMINAR_ITEM: (state, id) =>{
      state.carrito = state.carrito.filter(e => e.ID != id);
      Vue.delete(store.pedidos, id);
    },

    CAMBIAR_CANTIDAD_PEDIDO: (state, args) => {
      const id = args[0];
      const cambio = args[1];

      const pedido = state.pedidos[id].cantidad += cambio;
    },

    SET_MODALIDAD(state, modo){ state.modalidad = modo },
  },


  actions: {

    agregar: ({commit, getters, rootGetters}, datos) => {
      /*
      ID: Es el mismo id del menu/producto.
      PRECIO: Precio unitario del producto.
      SUBTOTAL: Precio total sin adicional.
      SUBTOTAL_ADICIONALES: Precio total de solo los adicionales.
      TOTAL: Precio completo incluyendo cantidad y adicionales.
      */

      return new Promise(function(resolve, reject){

        const item = {
          ID: datos.idProducto || datos.idMenu || datos.idItem,
          idAdicionales: datos.idAdicionales || [],
          notas: datos.nota || datos.notas || '',
          cantidad: datos.cantidad || 0,
        };

        const producto = rootGetters['productos/porId'](item.ID);
        const idDoomiActual = getters['idDoomialiadoActual'];
        const idDoomiProducto = producto ? producto.id_restaurante : null;

        // Verificar datos del datos.
        let errr = false;

        if(!item.ID) return reject(false);
        else if(!producto)
          errr = 'El producto no existe';
        else if(idDoomiActual !== null && idDoomiActual != idDoomiProducto)
          errr = 'Hay productos de otro doomialiado en el carrito';
        else if(producto.existencias < 1)
          errr = 'El producto está agotado';

        if(errr) return reject(errr);


        // Convertir ids de adicionales a int.
        item.idAdicionales = item.idAdicionales.map(id => parseInt(id, 10));

        // Precio unitario del producto.
        item.PRECIO = producto.precio || producto.precio_usd;

        // Calcular subtotal: Precio * Cantidad.
        item.SUBTOTAL = ((item.PRECIO * 100) * item.cantidad) / 100;

        item.SUBTOTAL_ADICIONALES = 0;
        if(producto.adicionales){
          // Iterar en los adicionales del producto referenciados en idAdicionales.
          for (let adi of producto.adicionales.filter(e => item.idAdicionales.includes(e.id))){
            // Sumar monto de cada adicional.
            item.SUBTOTAL_ADICIONALES = utils.sumarDinero(item.SUBTOTAL_ADICIONALES, adi.precio || adi.precio_usd);
          }
        }

        // Multiplicar el costo de los adicionales por la cantidad.
        item.SUBTOTAL_ADICIONALES = ((item.SUBTOTAL_ADICIONALES * 100) * item.cantidad) / 100;

        // Calcular total: Subtotal + Adicionales.
        item.TOTAL = item.SUBTOTAL;
        item.TOTAL = utils.sumarDinero(item.TOTAL, item.SUBTOTAL_ADICIONALES);

        commit('SET_ITEM', item);
        commit('SET_ID_DOOMIALIADO', producto.id_restaurante);
        return resolve('Agregado al carrito');
      });
    },

    cambiarCantidadPedido: ({ getters, rootGetters, commit }, args) => {
      const idProducto = args[0];
      const cambio = args[1];

      if (!utils.esNumerico(idProducto) || !utils.esNumerico(cambio) || cambio==0)
        return;

      let pedido = getters['pedidoPorId'](idProducto);

      const existencias = rootGetters['productos/existenciasPorId'](idProducto);

      if(!pedido)
        pedido = {};

      if((pedido.cantidad+cambio >= 0) && (pedido.cantidad+cambio < existencias)){
        commit('CAMBIAR_CANTIDAD_PEDIDO', [idProducto, cambio]);
      }
    },

    eliminar: ({ commit }, idProducto) => {
      if(!idProducto || !utils.esNumerico(idProducto))
        return;

      commit('ELIMINAR_ITEM', idProducto);
      return true;
    },

    vaciar: (context) => context.commit('VACIAR'),

    modalidad: (context, modalidad) => context.commit('SET_MODALIDAD', modalidad),
  },
};
