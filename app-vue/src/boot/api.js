import Vue from 'vue'
import * as api from '../api'

Vue.prototype.$api = api;
Vue.prototype.$auth = api.auth;
Vue.prototype.$utils = api.utils;
Vue.prototype.$config = api.config;
