
exports.up = function(knex, Promise) {
    return knex.schema.createTable("metodos_de_pago", table => {
        table.increments("id").primary();
        table.string("nombre");
        table.integer("id_restaurante").unsigned().references('id').inTable('restaurantes');
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTableIfExists("metodos_de_pago");
};
