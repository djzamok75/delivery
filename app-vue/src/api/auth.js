/* globals LocalStorage */

// import Vue from 'vue'
// import router from '../router'
import axios from 'axios'
import { LocalStorage } from 'quasar'
import { apiURL, STORAGE_KEY_AUTH, STORAGE_KEY_DATA } from './config'

// const LocalStorage = localStorage;
// const REDIRECCION_AL_AUTENTICAR = function() {
//   router.push({ name:'Pedidos' });
// }

// const REDIRECCION_AL_SALIR = function() {
//   router.push({ name:'Home' });
// }

const clearAuthFromData = obj => {
  if(obj && typeof obj != 'string'){
    delete obj['token'];
    delete obj['tipo_usu'];
    delete obj['tipo_usuario'];
    delete obj['pass'];
    delete obj['password'];
  }
  return obj;
};

export default {

  autenticar(correo, pass){
    const _auth = this;
    const data = {correo, pass};
    return new Promise(function(resolve, reject){
      axios.post(apiURL+'login/app', data)
        .then(function(resp){
          let data = resp.data[0] || resp.data;
          if(data && data.r == false) return reject(data);
          // if(!data || !data.token) return reject('Autenticacion fallida');

          LocalStorage.set(STORAGE_KEY_AUTH, {token:data.token, tipo_usuario: data.tipo_usu});
          data = clearAuthFromData(data);
          LocalStorage.set(STORAGE_KEY_DATA, data);
          return resolve(data);
        })
        .catch(function(err){
          _auth.salir();
          return reject(err);
        });
    });
  },

  salir(callback) {
    return new Promise((resolve, reject) => {
      LocalStorage.remove(STORAGE_KEY_AUTH);
      LocalStorage.remove(STORAGE_KEY_DATA);
      // LocalStorage.remove(STORAGE_KEY);
      if (callback) callback();
      resolve(true);
    });
  },

  autenticado() {
    return LocalStorage.has(STORAGE_KEY_AUTH);
  },

  obtenerToken() {
    return  LocalStorage.has(STORAGE_KEY_AUTH) ? (LocalStorage.getItem(STORAGE_KEY_AUTH)).token : null;
  },

  obtenerDatos(){
    let datos = LocalStorage.getItem(STORAGE_KEY_DATA);
    datos = clearAuthFromData(datos);
    return datos || null;
  },

  obtenerTipoUsuario(){
    if (LocalStorage.has(STORAGE_KEY_AUTH))
    {
      let data = LocalStorage.getItem(STORAGE_KEY_AUTH);
      return data.tipo_usu || data.tipo_usuario || null;
    }
  },

  requiereAutenticacion(to, from, next){
    if(!LocalStorage.has(STORAGE_KEY_AUTH)){
      next({
        name: 'Iniciodesesion',
        // query: { redirect: to.fullPath }
      });
    }
    else {
      next();
    }
  },

  siEstaAutenticado(to, from, next) {
    if(LocalStorage.has(STORAGE_KEY_AUTH)) {
      let data = LocalStorage.getItem(STORAGE_KEY_AUTH);
      let tipoUsuario = data.tipo_usu || data.tipo_usuario || null;

      switch (tipoUsuario){
        case 'cliente': return next({name:'home'});
        case 'repartidor': return next({name:'Pedidos'});
        default: return next();
      }
    }
    else {
      next();
    }
  },

  // requireAuth (to, from, next) {
  //     if (loggedIn()) {
  //         next({
  //             path: '/login',
  //             query: { redirect: to.fullPath }
  //         })
  //     } else {
  //         next()
  //     }
  // },

  // async execute (metodo, ruta, data){
  //   let token = await Vue.prototype.$auth.getAccessToken()
  //   return client({
  //     metodo,
  //     url: ruta,
  //     data,
  //     headers: {
  //       token,
  //       tipo_usuario: ''
  //     }
  //   }).then(req=>{return req.data})
  // },
}
