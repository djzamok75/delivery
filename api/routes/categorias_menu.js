// llamar al modelo.
const CategoriaMenu = require("../models/CategoriaMenu");

// Otras librerias.
const AR = require("../ApiResponser");
var sesion = require("./sesion");
var sesi = new sesion();

// Obligatorio para trabajar con FormData.
const multer = require("multer");
const upload = multer();

const express = require("express");
var router = express.Router();


// Registrar una categoria.
router.post("/add", upload.none(), sesi.validar_restaurante, async function(req, res, next){
	data = req.body;

	// Validar los datos.
	let errr = false;
	if (!data.nombre) errr = "Ingrese el nombre de la categoria";
	// if (!data.descripcion) errr = "Ingrese la descripción";
	if (!data.id_res) errr = "Ingresa id del restaurante";

	const verificar_n = await CategoriaMenu.query()
		.where({nombre:data.nombre, id_restaurante: data.id_res});
	if (verificar_n[0]) errr = "La categoria ya existe";

	if (errr) return AR.enviarDatos({r: false, msj: errr}, res);
	// Fin de validacion.

	await CategoriaMenu.query()
		.insert({
			nombre:data.nombre,
			descripcion:data.descripcion,
			id_restaurante: data.id_res
		})
		.then(async resp => {
			resp.msj = "Categoria agregada";
			resp.r = true;

			AR.enviarDatos(resp, res);
		})
		.catch(err => {
			console.log(err);
			err.r = false;
			err.msj = "Error al crear categoria";
			res.send(err);
		});
});


// Editar categoria.
router.put("/edit", upload.none(), sesi.validar_restaurante, async (req,res,next)=>{
	data = req.body;

	// Validar los datos.
	let errr = false;
	if (!data.nombre) errr = "Ingrese el nombre de la categoria";
	// if (!data.descripcion) errr = "Ingrese la descripción";

	//const verificar_n = await CategoriaMenu.query().where({nombre:data.nombre});
	//if (verificar_n[0]) errr = "La categoria ya existe";

	if (errr) return AR.enviarDatos({r: false, msj: errr}, res);
	// Fin de validacion.

	// Actualizar datos.
	let datos = {
		nombre: data.nombre,
		descripcion: data.descripcion
	};

	await CategoriaMenu.query().updateAndFetchById(data.iden, datos)
		.then(async function(resp){
			resp.msj = "Datos actualizados con éxito";
			resp.r = true;
			AR.enviarDatos(resp, res);
		})
		.catch(err => {
			console.log(err);
			err.r = false;
			err.msj = "Error al actualizar datos";
			res.send(err);
		});

});


// Listar todas las categorias.
router.get("/", upload.none(), async(req, res) => {
	data = req.query;
	// await CategoriaMenu.query().where("id_restaurante", data.id_res)
	// 	.then(categorias => {
	// 		AR.enviarDatos(categorias, res);
	// 	});

	let query = CategoriaMenu.query();

	if(data.id_res)
		query.where("id_restaurante", data.id_res)

	await query.then(categorias => AR.enviarDatos(categorias, res));
});



// Ver una categoria.
router.get("/:id", sesi.validar_general, async(req, res) =>{
	await CategoriaMenu.query().where("id", req.params.id)
		.then(categorias => {
			AR.enviarDatos(categorias[0], res);
		});
});


// Eliminar categoria.
router.delete("/delete/:id", upload.none(), sesi.validar_restaurante, async(req,res)=>{

	// // Asegurarse de que no este siendo usada.
	// await CategoriaMenu.query().where({id:req.params.id})
	// 	.then(async categorias => {

	// 		// Si no lo esta, eliminarla.
	// 		if (categorias[0].veces_usada == 0){
	// 			const eliminado = await CategoriaMenu.query().delete().where({id:req.params.id})
	// 				.catch(err => {
	// 					console.log(err);
	// 					res.send(err);
	// 				});
	// 			if (eliminado) {
	// 				AR.enviarDatos({msj: "Categoria eliminada exitosamente"}, res);
	// 			}
	// 			else {
	// 				AR.enviarDatos({msj: "No se puede eliminar la categoria"}, res);
	// 			}
	// 		}
	// 		else {
	// 			AR.enviarDatos({msj: "La categoría está siendo utilizada, no se puede eliminar"}, res);
	// 		}

	// 	});

    const categorias = await CategoriaMenu.query().where("id", req.params.id);

    if (categorias[0] && categorias[0].veces_usada > 0){
        return AR.enviarDatos({r: false, msj: "Ésta categoría está siendo utilizada, no se puede eliminar"}, res)
    }

    const eliminado = await CategoriaMenu.query().delete().where({id:req.params.id})
        .catch(err => {
            console.log(err);
            // res.send(err);
        });

    if (eliminado)
        AR.enviarDatos({r: true, msj: "Categoría eliminada exitosamente"}, res);
    else
        AR.enviarDatos({msj: "No se pudo eliminar la categoria"}, res);
});

module.exports = router;
