"use strict";

const { Modelo } = require("./Modelo");

class Estado extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "estado";
    }

}

module.exports = Estado;
