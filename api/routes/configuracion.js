const AR = require("../ApiResponser");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
var router = express.Router();
var archivos = require("../Archivos");
var config = require("../config");
//Llamar al helper de errores
const { ValidationError } = require("../Errores");
const { NotFoundError } = require("objection");
//Obligatorio para trabajar con FormData
const multer = require("multer");
const upload = multer();
//llamo validacion de sesion
var sesion = require("./sesion");
var sesi = new sesion();
const jsonfile = require('jsonfile');
var fs = require('fs');

var file = __dirname+ '/../configuracion.json'

function Quitarcoma(value) {
  if(value){
    for (var i = 0; i < value.split(".").length+10; i++) {
      value = value.replace(".", "");
    }
    value = value.replace(",", ".");
  }
  if(value.length=="3" || value.length=="2"){
    if(value.split(".").length==0)
      value = "0."+""+value;
    else
    value = "0"+""+value;
  }
  return parseFloat(value);
}

router.get("/",archivos.none(), async (req, res,next) => {
  var files = jsonfile.readFileSync(file);
  AR.enviarDatos(files, res);
});

router.post("/",archivos.none(),sesi.validar_admin, async (req, res,next) => {
    err = false;
    data = req.body;
    if (data.tar_min.length <1) {
      err = 'Introduzca la tarifa mínima'
    }
    if (data.pre_km.length <1) {
      err = 'Introduzca el precio del kilometro'
    }
    if (data.ran_km.length <1) {
      err = 'Introduzca el rango del kilometro'
    }
    if(err){return AR.enviarDatos({r: false, msg: err},res, 200);}
    jsonfile.readFile(file, function(err, obj) {
      var fileObj = obj;

      fileObj.minimo=parseFloat(Quitarcoma(data.tar_min));
      fileObj.km=parseFloat(data.ran_km);
      fileObj.precio=parseFloat(Quitarcoma(data.pre_km));

      jsonfile.writeFile(file, fileObj, function(err,obj) {
        if (err) throw err;
      fileObj.msg = "Configuración actualizada con éxito";
      fileObj.r= true;
      AR.enviarDatos(fileObj, res);
      });
    });

});



module.exports = router;
