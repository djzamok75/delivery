import {
  formatear,
  processImageFile,
  REGEXP_VALIDAR_CORREO,
  dec2hex,
  dias,
} from './utilsPrimitives'



const utils = {
  esNumerico: v => +v === +v,

  sumarDinero: (...valores) =>
    parseFloat(valores.reduce((a,b) => (a * 100 + b * 100) / 100, 0).toFixed(2)),

  restarDinero: (...valores) =>
    parseFloat(valores.reduce((a,b) => (a * 100 - b * 100) / 100, 0).toFixed(2)),

  verificarCorreo: correo => {
    let validez = true;

    if(!correo) validez = 'Por favor escribe tu correo';
    else if(!REGEXP_VALIDAR_CORREO.test(correo)) validez = 'Por favor ingresa un correo válido';

    return validez;
  },

  verificarTelefono: numero => {
    let validez = true;

    if(!numero) validez = 'Por favor ingresa tu número';
    else if (numero.length < 10 || numero.length > 16) validez = 'Mínimo 11 caracteres';

    return validez;
  },

  normalizarRuta(r) {
    r = r.endsWith('/') ? r : r+'/'; // Agregar barra al final.
    r = r.startsWith('/') ? r.slice(1) : r; // Quitar barra al principio.
    return r;
  },

  procesarImagen(e, vue_context) {
    let files = e.target.files || e.dataTransfer.files;
    let name = e.target.name;
    if (!files.length) return;
    let file = files[0];
    if (file.size / 1024 > 10240) {
      console.log('La imagen es muy grande!')
    }
    else {
      processImageFile(file, name, vue_context);
    }
  },

  precio: function(num, simbol) {
    simbol = simbol || '';

    if (num == null || !num)
      num = 0;

    num = num.toString().replace(/,/g, ".");

    num = Math.round(num * 100) / 100;

    return formatear(num.toFixed(2), simbol).replace(/-/g, "");
  },

  generarId (len) {
    /* Genera id hexadecimal aleatoria */
    var arr = new Uint8Array((len || 40) / 2)
    window.crypto.getRandomValues(arr)
    return Array.from(arr, dec2hex).join('')
  },

  de24a12 (hora) {
    /*
    Convierte de formato 24h a 12h
    '16:00:00' -> '04:00PM'
    */

    // Verificar formato correcto de la hora y separar en componentes.
    hora = hora.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [hora];

    if (hora.length > 1) { // If hora format correct
      hora = hora.slice (1);  // Remove full string match value
      hora[5] = +hora[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
      hora[0] = +hora[0] % 12 || 12; // Adjust hours
    }
    hora = hora.filter(Boolean);
    if (hora.length === 5)
      delete hora[3]; // Quitar segundos si los tiene.

    return hora.join (''); // return adjusted time or original string
  },

  hoy(){
    /* Nombre del dia de hoy */
    return dias[new Date().getDay()];
  },

};



export default utils;
export const verificarCorreo = utils.verificarCorreo;
export const verificarTelefono = utils.verificarTelefono;
export const procesarImagen = utils.procesarImagen;
export const normalizarRuta = utils.normalizarRuta;
export const precio = utils.precio;
export const generarId = utils.generarId;
export const esNumerico = utils.esNumerico;
export const sumarDinero = utils.sumarDinero;
export const restarDinero = utils.restarDinero;
export const de24a12 = utils.de24a12;
export const hoy = utils.hoy;
