function clientesjs() {
    
    recuperar_clientes();

    function recuperar_clientes(){
        ajaxGET('clientes/', actualizar_tabla_cientes)
    };

    function actualizar_tabla_cientes(data) {
        const table = $('.table').DataTable({
            "language": { "url": "../static/<lib></lib>/JTables/Spanish.json" }
        });
        let count = 1;
        table.clear().draw();

        for (cliente of data) {
            options = `<a href="#md-verCliente" id="${cliente.id}" class="btn-ver-cliente btn btn-floating btn-primary modal-trigger tooltipped" data-position="bottom" data-delay="50" data-tooltip="Ver información del cliente"><i class="fa fa-eye"></i></a>`;

            table.row.add([count++, cliente.nombre+' '+cliente.apellido, options]).draw().node();
        }
    };

    $(document).on('click', '.btn-ver-cliente', evt => {
        const id_cliente = evt.target.id;

        ajaxGET(`clientes/${id_cliente}`, data => {
            let sexo;
            const cliente = data[0];

            if(cliente.sexo=='M') sexo = 'M';
            else if (cliente.sexo=='F') sexo=='F';
            $('#ver-cedula').html(cliente.identificacion+'-'+cliente.cedula);
            $('#ver-nombre').html(cliente.nombre);
            $('#ver-apellido').html(cliente.apellido);
            $('#ver-sexo').html(cliente.sexo=='M' ? 'Masculino' : 'Femenino');
            $('#ver-sexo').html();
            $('#ver-telefono').html(cliente.telefono);
            $('#ver-correo').html(cliente.correo);
            $('#ver-usuario').html(cliente.usuario);
            $('#ver-whatsapp').html(cliente.whatsapp);
        });
    });

};
