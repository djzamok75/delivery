"use strict";

const { Modelo } = require("./Modelo");


class CategoriaMenu extends Modelo {
    //Obligatorio. Indica la tabla que va a usar
    static get tableName() {
        return "categorias_menu";
    }
}
module.exports = CategoriaMenu;
