exports.up = knex => {
    return knex.schema.createTable("tipovehiculo", table => {
        table.increments("id").primary();
        table.string("nombre_veh");
        table.string("icono_veh");
    });
};

exports.down = knex => {
    return knex.schema.dropTableIfExists("tipovehiculo");
};