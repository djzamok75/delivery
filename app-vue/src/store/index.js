import Vue from 'vue'
import Vuex from 'vuex'

// import example from './module-example'
import productos from './modules/productos'
import cliente from './modules/cliente'
import doomis from './modules/doomis'
import carrito from './modules/carrito'
import ordenes from './modules/ordenes'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      productos,
      cliente,
      doomis,
      carrito,
      ordenes,
    },

    state: {
      titleCurrenView: 'Doomi',
      leftDrawerState: false,
      Estado: false,
      Fav:0,
      Pedido:0
    },


    getters: {
      leftDrawerState(state){
        return state.leftDrawerState;
      },
    },


    mutations: {
      toggleLeftDrawer(state){
        state.leftDrawerState = !state.leftDrawerState;
      },
      openLeftDrawer(state){
        state.leftDrawerState = true;
      },
      closeLeftDrawer(state){
        state.leftDrawerState = false;
      },
      setLeftDrawer(state, v){
        state.leftDrawerState = Boolean(v);
      },
    },


    actions: {
      actualizarTodo(context){
        context.dispatch('doomis/actualizarDoomialiados');
        context.dispatch('productos/actualizarProductos');
        context.dispatch('productos/actualizarCategorias');
        context.dispatch('doomis/actualizarDoomialiadosCercanos');
        context.dispatch('doomis/actualizarCategorias');
        context.dispatch('cliente/actualizarFavoritos');
      },
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
