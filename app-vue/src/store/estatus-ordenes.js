export default {
	ENVIADO:'Enviado', // Pedido enviado y guardado.
	ACEPTADO:'Aceptado', // Recibido y aceptado.
	RECHAZADO:'Rechazado', // Rechazado.
	ESPERANDO:'Esperando', // Esperando por el pago del cliente.
	VERIFICANDO:'Verificado', // Pago verificado y correcto.
	INHABILITADO:'Inhabilitado', // Pago incorrecto.
	TRANSITO:'En transito',
	ENTREGADO:'Entregado',
	FINALIZADO:'Finalizado',
};
