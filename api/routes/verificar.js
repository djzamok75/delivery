const moment = require("moment");
moment.locale("es");

//const Notificaciones = require("../models/Notificacion");


arr = []
class Verificar {
	/*static cinco_dias() {
        //console.log('-------Vencido-----------')
        var dias = moment().subtract(5, 'days')
        var fecha = moment(dias).format('YYYY-MM-DD')
        //console.log(fecha)
        Cuotas_facturas.query()
        .join("facturas","facturas.id","cuotas_facturas.id_factura")
        .joinRaw('LEFT JOIN notificacion ON notificacion.id_usable = cuotas_facturas.id_factura AND notificacion.id_receptor = facturas.id_estudiante AND notificacion.tipo_noti=12')
        .where('cuotas_facturas.sts_cuota',null)
        .whereRaw(' cuotas_facturas.fecha_registro < "'+fecha+'"')
        .where('notificacion.id',null)
        .select('cuotas_facturas.id','cuotas_facturas.id_factura','cuotas_facturas.fecha_registro')
        .then(async ret => {
      		//console.log(ret);
      		for (var i = 0; i < ret.length; i++) {
      			const fac = await Facturas.query().where('id',ret[i].id_factura).catch(err => {console.log(err)});
      			const not = await Notificaciones.query()
		          	.insert({
		              descripcion: "Tienes un pago vencido",
		              contenido: "El pago de la factura <b>"+fac[0].codigo_factura+"</b> se ha vencido",
		              estatus: 0,
		              id_receptor: fac[0].id_estudiante,
		              tabla_usuario: 'estudiantes',
		              tipo_noti: 12,
		              id_usable: ret[i].id_factura,
		              fecha: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
		          	}).catch(err => {console.log(err)});
		        await Censo.query().update({ estatus_estudiante:'Moroso', sts_multimedia: 0,sts_clase: 0}).where({id_grupo: fac[0].id_grupo, id_estudiante: fac[0].id_estudiante});
      		};
      		
  		});
    	
    }
    static undiaparagrupo_e() {
        //console.log('-------Vencido-----------')
        var dias = moment().add(1, 'days')
        var fecha = moment(dias).format('YYYY-MM-DD')
        //console.log(fecha)
        Estudiantes.query()
        .join("censo","censo.id_estudiante","estudiantes.id")
        .join("grupos","grupos.id","censo.id_grupo")
        .joinRaw('LEFT JOIN notificacion ON notificacion.id_usable = grupos.id AND notificacion.id_receptor = estudiantes.id AND notificacion.tipo_noti=13')
        .whereRaw(' grupos.fecha_inicio = "'+fecha+'"')
        .whereRaw(' censo.estatus_estudiante <> "Postulado" AND censo.estatus_estudiante <> "Censado"')
        .where('notificacion.id',null)
        .select('grupos.id','grupos.nombre_grupo','estudiantes.id as id_estudiante','grupos.fecha_inicio')
        .then(async ret => {
      		//console.log(ret);
      		for (var i = 0; i < ret.length; i++) {
      			var fec = moment(ret[i].fecha_inicio).format('DD-MM-YYYY');
      			const not2 = await Notificaciones.query()
		          	.insert({
		              descripcion: "Mañana <b>"+fec+"</b> comienza el grupo <b>"+ret[i].nombre_grupo+"</b>",
		              contenido: "Desde el <b>"+fec+"</b> se dará inicio al grupo <b>"+ret[i].nombre_grupo+"</b>",
		              estatus: 0,
		              id_receptor: ret[i].id_estudiante,
		              tabla_usuario: 'estudiantes',
		              tipo_noti: 13,
		              id_usable: ret[i].id,
		              fecha: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
		          	}).catch(err => {console.log(err)});
		    };
      		
  		});
    }
    */
}

module.exports = Verificar;
